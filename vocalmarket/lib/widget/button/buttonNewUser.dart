// import 'dart:math';

import 'package:http/http.dart' as http;
// import 'dart:convert';

import 'package:flutter/material.dart';
// import 'package:vocalmarket/screens/login.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';
// import 'package:vocalmarket/widget/password.dart';
// import 'package:vocalmarket/widget/inputEmail.dart';
// import 'package:vocalmarket/app/data/pojo/user.dart';
// import 'package:vocalmarket/app/pages/home.dart';

class ButtonNewUser extends StatefulWidget {
  final String name;
  final String email;
  final String pass;
  final String repass;

  ButtonNewUser(this.name, this.email, this.pass, this.repass);

  @override
  _ButtonNewUserState createState() => _ButtonNewUserState();
}

class _ButtonNewUserState extends Base<ButtonNewUser> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  _signup(String name, String email, String pass, String repass) {
    print(
        "name===============$name=============$email-----$pass-------$repass----------------");
    getOdooInstance().then((odoo) {
      if (isValid(email, pass, repass)) {
        isConnected().then((isInternet) {
          if (isInternet) {
            // showLoading();
            odoo.authenticate("vijay@wearme.me", "a", Strings.odoo_db).then(
              (http.Response auth) {
                if (auth.body != null) {
                  // hideLoadingSuccess("Logged in successfully");
                  odoo.create('res.users', {
                    'name': name,
                    'login': email,
                    'password': pass,
                    'sel_groups_1_9_10': 9,
                  }).then((OdooResponse res) {
                    if (res.getStatusCode() == 200) {
                      _scaffoldKey.currentState.showSnackBar(new SnackBar(
                          content: Text("Signup in successfully!")));
                    }
                  });
                  // saveUser(json.encode(user));
                  // saveOdooUrl("http://10.0.2.2:8069");
                  // pushReplacement(Home());
                } else {
                  _scaffoldKey.currentState.showSnackBar(new SnackBar(
                      content: Text("Please Enter Valid Email or Password!")));
                }
              },
            );
          }
        });
      }
    });
  }

  bool isValid(String email, String pass, String repass) {
    print("isValid------------------------$email------$pass-----$repass---");
    // Pattern pattern =
    //     r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    // RegExp regex = new RegExp(pattern);
    // if(pass != repass){
    //   // hideLoadingError("Password is not Matched!");
    // }
    // else if (!regex.hasMatch(email)){
    //   // hideLoadingError("Please Enter Valid Email!");

    // }
    // else{
    //   // hideLoadingError("working");
    //   return true
    // }
    // return false;
    return true;
  }

  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 40, right: 50, left: 50),
      child: Container(
        alignment: Alignment.bottomRight,
        height: 50,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(0),
        ),
        child: FlatButton(
          onPressed: () {
            print("data===============${widget.email}");
            _signup(widget.name, widget.email, widget.pass, widget.repass);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'SIGNUP',
                style: TextStyle(color: Colors.white, fontFamily: "Quicksand"),
              ),
              Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
