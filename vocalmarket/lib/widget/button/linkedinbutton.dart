import 'package:flutter/material.dart';
// import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class LinkedinButton extends StatefulWidget {
  @override
  _LinkedinButtonState createState() => _LinkedinButtonState();
}

class _LinkedinButtonState extends State<LinkedinButton> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, right: 50, left: 50),
      child: Container(
        alignment: Alignment.bottomRight,
        height: 50,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.black,
            width: 1,
          ),
          boxShadow: [
            // BoxShadow(
            //   color: Colors.blue[300],
            //   blurRadius: 10.0, // has the effect of softening the shadow
            //   spreadRadius: 1.0, // has the effect of extending the shadow
            //   offset: Offset(
            //     5.0, // horizontal, move right 10
            //     5.0, // vertical, move down 10
            //   ),
            // ),
          ],
          color: Colors.white,
          borderRadius: BorderRadius.circular(0),
        ),
        child: FlatButton(
          onPressed: () {},
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Login with Linkedin',
                style: TextStyle(
                  color: Colors.black,
                  // fontSize: 14,
                  // fontWeight: FontWeight.w700,
                  fontFamily: "Quicksand"
                ),
              ),
            ],
          ),
        ),
        
      ),
    );
  }
}
