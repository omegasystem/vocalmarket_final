// import 'dart:math';

import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:flutter/material.dart';
// import 'package:vocalmarket/screens/login.dart';
import 'package:vocalmarket/base.dart';
// import 'package:vocalmarket/widget/password.dart';
// import 'package:vocalmarket/widget/inputEmail.dart';
import 'package:vocalmarket/odoo/data/pojo/user.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';
// import 'package:vocalmarket/app/pages/home.dart';
import 'package:vocalmarket/screens/home.dart';

// class ButtonLogin extends StatefulWidget {

//   const ButtonLogin(String text, String text2);

//   @override
//   _ButtonLoginState createState() => _ButtonLoginState();
// }

// class _ButtonLoginState extends Base<ButtonLogin> {

// _login([emailCtrler]) {
//   print("login----------------------------");
//   if (isValid()) {
//     isConnected().then((isInternet) {
//       if (isInternet) {
//         showLoading();
//         odoo.authenticate(, _pass, _selectedDb).then(
//           (http.Response auth) {
//             if (auth.body != null) {
//               hideLoadingSuccess("Logged in successfully");
//               User user = User.fromJson(jsonDecode(auth.body));
//               saveUser(json.encode(user));
//               saveOdooUrl(odooURL);
//               pushReplacement(Home());
//             } else {
//               showMessage("Authentication Failed",
//                   "Please Enter Valid Email or Password");
//             }
//           },
//         );
//       }
//     });
//   }
// }

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(top: 40, right: 50, left: 50),
//       child: Container(
//         alignment: Alignment.bottomRight,
//         height: 50,
//         width: MediaQuery.of(context).size.width,
//         decoration: BoxDecoration(
//           color: Colors.black,
//           borderRadius: BorderRadius.circular(0),
//         ),
//         child: FlatButton(
//           onPressed: () {
//             _login();
//           },
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Text(
//                 'LOG IN',
//                 style: TextStyle(
//                   color: Colors.white,
//                   fontFamily: "Quicksand"
//                 ),
//               ),
//               Icon(
//                 Icons.arrow_forward,
//                 color: Colors.white,
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }

class ButtonLogin extends StatefulWidget {
  final String email;
  final String pass;

  ButtonLogin(this.email, this.pass);

  @override
  _ButtonLoginState createState() => _ButtonLoginState();
}

class _ButtonLoginState extends Base<ButtonLogin> {
  _login(String email, String pass) {
    print("login--------------$email");
    getOdooInstance().then((odoo) {
      // if (isValid(email, pass)) {
      // isConnected().then((isInternet) {
      //   if (isInternet) {
      //     // showLoading();
      //     odoo.authenticate(email, pass, Strings.odoo_db).then(
      //       (http.Response auth) {
      //         if (auth.body != null) {
      //           // hideLoadingSuccess("Logged in successfully");
      //           User user = User.fromJson(jsonDecode(auth.body));
      //           if (user.result == null) {
      //             hideLoadingError('Please Enter Valid Email or Password');
      //           } else {
      //             hideLoadingSuccess("Logged in successfully");
      //             saveUser(json.encode(user));
      //             saveOdooUrl(Strings.odoo_url);
      //             pushReplacement(Home());
      //           }
      //         } else {
      //           showMessage("Authentication Failed",
      //               "Please Enter Valid Email or Password");
      //         }
      //       },
      //     );
      //   }
      // });
      // }
    });
  }

  bool isValid(String email, String pass) {
    if (email.length > 0 && pass.length > 0) {
      return true;
    } else {
      // showSnackBar("Please enter valid email and password");
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 40, right: 50, left: 50),
      child: Container(
        alignment: Alignment.bottomRight,
        height: 50,
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(0),
        ),
        child: FlatButton(
          onPressed: () {
            print("widdfer0000000000000000000000${widget.email}");
            _login(widget.email, widget.pass);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'LOG IN',
                style: TextStyle(color: Colors.white, fontFamily: "Quicksand"),
              ),
              Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// class ButtonLogin extends StatelessWidget {

//   final String email;
//   final String pass;

//   ButtonLogin(this.email, this.pass);

//   _login(String email, String pass) {
//     print("click-------------------$email");
//   }

//   _login([emailCtrler]) {
//     print("login----------------------------");
//     if (isValid()) {
//       isConnected().then((isInternet) {
//         if (isInternet) {
//           showLoading();
//           odoo.authenticate(, _pass, _selectedDb).then(
//             (http.Response auth) {
//               if (auth.body != null) {
//                 hideLoadingSuccess("Logged in successfully");
//                 User user = User.fromJson(jsonDecode(auth.body));
//                 saveUser(json.encode(user));
//                 saveOdooUrl(odooURL);
//                 pushReplacement(Home());
//               } else {
//                 showMessage("Authentication Failed",
//                     "Please Enter Valid Email or Password");
//               }
//             },
//           );
//         }
//       });
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(top: 40, right: 50, left: 50),
//       child: Container(
//         alignment: Alignment.bottomRight,
//         height: 50,
//         width: MediaQuery.of(context).size.width,
//         decoration: BoxDecoration(
//           color: Colors.black,
//           borderRadius: BorderRadius.circular(0),
//         ),
//         child: FlatButton(
//           onPressed: () {
//             _login(email, pass);
//           },
//           child: Row(
//             mainAxisAlignment: MainAxisAlignment.center,
//             children: <Widget>[
//               Text(
//                 'LOG IN',
//                 style: TextStyle(
//                   color: Colors.white,
//                   fontFamily: "Quicksand"
//                 ),
//               ),
//               Icon(
//                 Icons.arrow_forward,
//                 color: Colors.white,
//               ),
//             ],
//           ),
//         ),
//       ),
//     );
//   }
// }
