import 'package:flutter/material.dart';

class NewName extends StatelessWidget {
  final TextEditingController controller;

  const NewName({Key key, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 50, left: 50, right: 50),
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width,
        child: 
        new TextField(
            controller: controller,
            decoration: new InputDecoration(
              labelText: "Enter Name",
              fillColor: Colors.black,
            ),
            keyboardType: TextInputType.emailAddress,
            style: new TextStyle(
              fontFamily: "Quicksand",
              color: Colors.black
            ),
        ),
      ),
    );
  }
}



// class NewNome extends StatefulWidget {
//   @override
//   _NewNomeState createState() => _NewNomeState();
// }

// class _NewNomeState extends State<NewNome> {
//   @override
//   Widget build(BuildContext context) {
//     return Padding(
//       padding: const EdgeInsets.only(top: 50, left: 50, right: 50),
//       child: Container(
//         height: 60,
//         width: MediaQuery.of(context).size.width,
//         child: 

//         // new TextFormField(
//         //     decoration: new InputDecoration(
//         //       labelText: "Name",
//         //       fillColor: Colors.lightBlueAccent,
//         //       // border: new OutlineInputBorder(
//         //       //   borderRadius: new BorderRadius.circular(15.0),
//         //       //   borderSide: new BorderSide(
//         //       //     color: Colors.white
//         //       //   ),
//         //       // ),
//         //       //fillColor: Colors.green
//         //     ),
//         //     validator: (val) {
//         //       if(val.length==0) {
//         //         return "Email cannot be empty";
//         //       }else{
//         //         return null;
//         //       }
//         //     },
//         //     keyboardType: TextInputType.emailAddress,
//         //     style: new TextStyle(
//         //       fontFamily: "Quicksand",
//         //       color: Colors.white
//         //     ),
//         // ),

//         TextField(
//           style: TextStyle(
//             color: Colors.black,
//           ),
//           decoration: InputDecoration(
//             fillColor: Colors.lightBlueAccent,
//             labelText: 'Name',
//             labelStyle: TextStyle(
//               color: Colors.black,
//             ),
//           ),
//         ),
//       ),
//     );
//   }
// }