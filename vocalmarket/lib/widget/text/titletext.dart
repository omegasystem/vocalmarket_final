import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:vocalmarket/src/theme/lightcolor.dart';

class TitleText extends StatelessWidget {
  final String text;
  final double fontSize;
  final Color color;
  final FontWeight fontWeight;
  final TextOverflow textOverflow;
  const TitleText(
      {Key key,
      this.text,
      this.fontSize = 18,
      this.color = LightColor.titleTextColor,
      this.fontWeight = FontWeight.w800,
      this.textOverflow = TextOverflow.ellipsis})
      : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Text(text,
        style: GoogleFonts.quicksand(
            fontSize: fontSize, fontWeight: fontWeight, color: color));
  }
}
