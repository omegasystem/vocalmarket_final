import 'package:flutter/material.dart';


class NewEmail extends StatelessWidget {
  final TextEditingController controller;

  const NewEmail({Key key, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width,
        child: 
        new TextField(
            controller: controller,
            decoration: new InputDecoration(
              labelText: "Enter Email",
              fillColor: Colors.black,
            ),
            keyboardType: TextInputType.emailAddress,
            style: new TextStyle(
              fontFamily: "Quicksand",
              color: Colors.black
            ),
        ),
      ),
    );
  }
}