import 'package:flutter/material.dart';


class RePasswordInput extends StatelessWidget {
  final TextEditingController controller;

  const RePasswordInput({Key key, this.controller}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 20, left: 50, right: 50),
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width,
        child: 
        new TextField(
            obscureText: true,
            controller: controller,
            decoration: new InputDecoration(
              labelText: "Enter Re-Password",
              fillColor: Colors.black,
            ),
            keyboardType: TextInputType.emailAddress,
            style: new TextStyle(
              fontFamily: "Quicksand",
              color: Colors.black
            ),
        ),
      ),
    );
  }
}