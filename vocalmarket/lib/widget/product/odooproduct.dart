import 'package:flutter/material.dart';
import 'package:vocalmarket/odoo/data/pojo/product.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/screens/allproducts.dart';

class OdooProducts extends StatefulWidget {
  final String categId;
  final String categName;

  const OdooProducts({Key key, this.categId, this.categName}) : super(key: key);

  @override
  _OdooProductsState createState() => _OdooProductsState();
}

class _OdooProductsState extends Base<OdooProducts> {
  List<Products> _products = [];

  getOdooProducts() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo
            .searchRead(
                "product.template",
                [
                  ["public_categ_ids", "child_of", int.parse(widget.categId)]
                ],
                ['id', 'name', 'image'],
                limit: 6)
            .then(
          (OdooResponse res) {
            if (!res.hasError()) {
              String session = getSession();
              session = session.split(",")[0].split(";")[0];
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    _products.add(new Products(
                      id: i["id"].toString(),
                      name: i["name"],
                      imageUrl: getURL() +
                          "/web/image?model=product.template&field=image&" +
                          session +
                          "&id=" +
                          i["id"].toString(),
                    ));
                    // hideLoading();
                  }
                });
              }
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getOdooProducts();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;

    return new Container(
        color: Colors.white,
        width: deviceSize.width,
        height: 150.0,
        child: new ListView.builder(
            scrollDirection: Axis.horizontal,
            itemCount: _products.length,
            itemBuilder: (BuildContext cont, int index) {
              String imgPath = _products[index].imageUrl;
              return new Container(
                // width: 100.0,
                child: new FlatButton(
                    onPressed: () {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AllProducts(
                              categId: widget.categId,
                              categName: widget.categName),
                        ),
                      );
                    },
                    padding:
                        new EdgeInsets.only(top: 5.0, left: 15.0, right: 15.0),
                    child: new GestureDetector(
                      onTap: () {
                        print('ca;;;;;;;;;;;;;;;;;;;;;;;');
                        // Navigator.push(context, MaterialPageRoute(builder: (context)=> ProductDetails(productId: productId, name: name, imgPath: imgPath)));
                      },
                      child: SizedBox(
                        width: 150.0,
                        height: 150.0,
                        child: Card(
                          semanticContainer: true,
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          child: FadeInImage(
                              placeholder:
                                  new AssetImage('images/placeholder.jpeg'),
                              image: new NetworkImage(
                                imgPath,
                              )),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(0.0),
                          ),
                          elevation: 5,
                          margin: EdgeInsets.all(10),
                        ),
                      ),
                    )),
              );
            }));
  }
}
