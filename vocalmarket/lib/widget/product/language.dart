// import 'package:vocalmarket/app_translations_delegate.dart';
import 'package:vocalmarket/application.dart';
// import 'package:vocalmarket/app_translations.dart';
import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
// import 'package:vocalmarket/odoo/data/services/odoo_response.dart';

class OdooLanguageSelector extends StatefulWidget {
  final void Function(String) onChangeLang;

  OdooLanguageSelector({this.onChangeLang});

  @override
  _OdooLanguageSelectorState createState() => _OdooLanguageSelectorState();
}

class _OdooLanguageSelectorState extends Base<OdooLanguageSelector> {
  String dropdownValue = 'English';

  //languagesList also moved to the Application class just like the languageCodesList
  static final List<String> languagesList = application.supportedLanguages;
  String firstLang = languagesList[0];
  static final List<String> languageCodesList =
      application.supportedLanguagesCodes;

  final Map<dynamic, dynamic> languagesMap = {
    languagesList[0]: languageCodesList[0],
    languagesList[1]: languageCodesList[1],
  };

  // getLanguage() async {
  //   isConnected().then((isInternet) {
  //     if (isInternet) {
  //       odoo.searchRead("res.currency", [], [
  //         'id',
  //         'name',
  //       ]).then(
  //         (OdooResponse res) {
  //           if (!res.hasError()) {
  //             setState(() {
  //               for (var i in res.getRecords()) {
  //                 _odoocurrencyList.add(new OdooCurrency(
  //                   name: i["name"],
  //                 ));
  //                 // widget.getCurrency();
  //               }
  //               hideLoading();
  //             });
  //           }
  //         },
  //       );
  //     }
  //   });
  // }

  @override
  void initState() {
    super.initState();
    // getOdooInstance().then((odoo) {
    //   getLanguage();
    // });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 0, right: 0),
      child: Container(
          alignment: Alignment.centerRight,
          height: 25, //gives the height of the dropdown button
          width: MediaQuery.of(context).size.width /
              4, //gives the width of the dropdown button
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(3)),
              color: Colors.white),
          child: Theme(
              data: Theme.of(context).copyWith(
                  canvasColor:
                      Colors.white, // background color for the dropdown items
                  buttonTheme: ButtonTheme.of(context).copyWith(
                    alignedDropdown:
                        true, //If false (the default), then the dropdown's menu will be wider than its button.
                  )),
              child: DropdownButtonHideUnderline(
                  child: DropdownButton<String>(
                items: languagesList.map((String data) {
                  return new DropdownMenuItem<String>(
                    value: data,
                    child: new Text(data),
                  );
                }).toList(),
                value: firstLang,
                onChanged: (value) {
                  setState(() {
                    firstLang = value;
                  });
                  // getOdooInstance().then((odoo) {
                  //     widget.onLocaleChange(value);
                  // });
                  widget.onChangeLang(value);
                  // application.onLocaleChanged(Locale(languagesMap[value]));
                },
              )))),
    );
  }
}
