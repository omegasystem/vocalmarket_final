import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/attribute.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
// import 'package:vocalmarket/odoo/data/pojo/filter.dart';
// import 'package:vocalmarket/widget/product/productattributevalue.dart';
// import 'package:vocalmarket/widget/text/titletext.dart';

class ProductAttribute extends StatefulWidget {
  final String productId;

  final void Function(String) onChangeAttribute;

  ProductAttribute({Key key, this.productId, this.onChangeAttribute})
      : super(key: key);

  @override
  _ProductAttributeState createState() => _ProductAttributeState();
}

class _ProductAttributeState extends Base<ProductAttribute> {
  Map<String, List> filterMap = {};
  Map<String, List> attributeListMap = {};

  List<Attribute> _attribute = [];
  Attribute _selectedAttribute;
  // List<Filters> _attributeList = [];

  // getAttribute() async {
  //   isConnected().then((isInternet) {
  //     if (isInternet) {
  //       odoo.searchRead("product.attribute", [
  //         [
  //           'attribute_line_ids.product_tmpl_id',
  //           "=",
  //           int.parse(widget.productId)
  //         ]
  //       ], [
  //         'id',
  //         'name'
  //       ]).then(
  //         (OdooResponse res) {
  //           if (!res.hasError()) {
  //             if (this.mounted) {
  //               setState(() {
  //                 for (var i in res.getRecords()) {
  //                   _attribute.add(i["id"].toString());
  //                   attributeListMap[i["name"].toString()] = [];
  //                   _attributeList.add(new Filters(
  //                     id: i["id"].toString(),
  //                     name: i["name"],
  //                   ));
  //                 }
  //               });
  //             }
  //           }
  //         },
  //       );
  //     }
  //   });
  // }

  getAttribute() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo.searchRead("product.product", [
          ['product_tmpl_id', "=", int.parse(widget.productId)]
        ], [
          'id',
          'name',
          'combine_attribute_name'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    print("i=====================$i");
                    _attribute.add(new Attribute(
                        id: i["id"].toString(),
                        name: i["name"],
                        attributename: i["combine_attribute_name"] != null
                            ? i["combine_attribute_name"]
                            : "data"));
                  }
                  // hideLoading();
                });
              }
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getAttribute();
      // _selectedAttribute = _attribute.isEmpty? hideLoading(): _attribute.first;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  // Widget build(BuildContext context) {
  //   return _attributeList.isEmpty? Container(): Column(
  //       crossAxisAlignment: CrossAxisAlignment.start,
  //       children: List<Widget>.generate(_attributeList.length, (int index) {
  //         return Column(
  //           children: <Widget>[
  //             SizedBox(height: 10),
  //             TitleText(
  //               text: _attributeList[index].name,
  //               fontSize: 14,
  //               color: Colors.white,
  //             ),
  //             SizedBox(height: 10),
  //             Row(
  //                 mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                 children: <Widget>[
  //                   ProductAttributeValue(
  //                       productId: widget.productId,
  //                       attributeId: _attributeList[index].id,
  //                       atttibuteList: _attribute),
  //                 ])
  //           ],
  //         );
  //       })
  //       );
  // }

  // OLD CODE - 1

  // return _attributeList.isEmpty
  //     ? Container()
  //     : Column(
  //         children: List<Widget>.generate(_attributeList.length, (int index) {
  //         return Row(children: <Widget>[
  //           Expanded(
  //             child: Column(
  //               mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //               children: <Widget>[
  //                 Padding(
  //                   padding:
  //                       const EdgeInsets.only(left: 45, right: 45, top: 10),
  //                   child: Container(
  //                       alignment: Alignment.centerLeft,
  //                       child: new Text(_attributeList[index].name,
  //                           textAlign: TextAlign.left,
  //                           style: new TextStyle(
  //                               color: Colors.black,
  //                               fontSize: 15.0,
  //                               fontWeight: FontWeight.bold))),
  //                 ),
  //                 Row(
  //                     mainAxisAlignment: MainAxisAlignment.spaceEvenly,
  //                     children: <Widget>[
  //                       ProductAttributeValue(
  //                           productId: widget.productId,
  //                           attributeId: _attributeList[index].id,
  //                           atttibuteList: _attribute),
  //                     ])

  // OLD CODE - 2

  // Padding(
  //   padding: const EdgeInsets.only(
  //       left: 40, right: 40, bottom: 24, top: 12),
  //   child: Container(
  //     height: 55, //gives the height of the dropdown button
  //     width: MediaQuery.of(context)
  //         .size
  //         .width, //gives the width of the dropdown button
  //     decoration: BoxDecoration(
  //         borderRadius: BorderRadius.all(Radius.circular(3)),
  //         color: Color(0xFFF2F2F2)),
  //     child: Theme(
  //         data: Theme.of(context).copyWith(
  //             canvasColor: Colors
  //                 .white, // background color for the dropdown items
  //             buttonTheme: ButtonTheme.of(context).copyWith(
  //               alignedDropdown:
  //                   true, //If false (the default), then the dropdown's menu will be wider than its button.
  //             )),
  //         child: DropdownButtonHideUnderline(
  //           // to hide the default underline of the dropdown button
  //           child: DropdownButton<String>(
  //             iconEnabledColor: Color(
  //                 0xFF595959), // icon color of the dropdown button
  //             items: _attribute.map((String value) {
  //               return DropdownMenuItem<String>(
  //                 value: value,
  //                 child: Text(
  //                   value,
  //                   style: TextStyle(fontSize: 15),
  //                 ),
  //               );
  //             }).toList(),
  //             hint: Text(
  //               "Frame Type",
  //               style: TextStyle(
  //                   color: Color(0xFF8B8B8B), fontSize: 15),
  //             ), // setting hint
  //             onChanged: (String value) {
  //               setState(() {
  //                 // frametype = value; // saving the selected value
  //               });
  //             },
  //             // value: frametype, // displaying the selected value
  //           ),
  //         )),
  //   ),
  // )

  // OLD CODE - 1

  //                 ],
  //               ),
  //             ),
  //           ]);
  //         }));

  // }
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30, bottom: 24, top: 12),
      child: Container(
          height: 55, //gives the height of the dropdown button
          width: MediaQuery.of(context).size.width /
              2, //gives the width of the dropdown button
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(3)),
              color: Color(0xFFF2F2F2)),
          child: Theme(
              data: Theme.of(context).copyWith(
                  canvasColor:
                      Colors.white, // background color for the dropdown items
                  buttonTheme: ButtonTheme.of(context).copyWith(
                    alignedDropdown:
                        true, //If false (the default), then the dropdown's menu will be wider than its button.
                  )),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<Attribute>(
                  iconEnabledColor:
                      Color(0xFF595959), // icon color of the dropdown button
                  items: _attribute.map((data) {
                    return DropdownMenuItem<Attribute>(
                      value: data,
                      child: Text(
                        data.attributename,
                        style: TextStyle(fontSize: 15),
                      ),
                    );
                  }).toList(),
                  hint: Text(
                    "Select",
                    style: TextStyle(color: Color(0xFF8B8B8B), fontSize: 15),
                  ), // setting hint
                  value: this._selectedAttribute,
                  onChanged: (Attribute data) {
                    setState(() {
                      this._selectedAttribute = data;
                    });
                    widget.onChangeAttribute(data.id.toString());
                  },
                ),
              ))),
    );
  }
}
