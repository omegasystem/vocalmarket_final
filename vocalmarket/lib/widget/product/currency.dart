import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/partners.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
// import 'package:vocalmarket/odoo/utility/strings.dart';
import 'package:vocalmarket/odoo/data/pojo/currency.dart';

class ProductCurrency extends StatefulWidget {
  // final VoidCallback getCurrency;
  final void Function(String) onChangeCurrency;

  ProductCurrency({this.onChangeCurrency});

  @override
  _ProductCurrencyState createState() => _ProductCurrencyState();
}

class _ProductCurrencyState extends Base<ProductCurrency> {
  String dropdownValue = 'USD';
  List<OdooCurrency> _odoocurrencyList = [];
  OdooCurrency _selectedCurrency;
  final Map<String, dynamic> someMap = {};

  getCurrency() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("res.currency", [], [
          'id',
          'name',
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    _odoocurrencyList.add(new OdooCurrency(
                      id: i["id"].toString(),
                      name: i["name"],
                    ));
                    // widget.getCurrency();
                  }
                  // hideLoading();
                });
              }
            }
          },
        );
      }
    });
  }

  setCurrency(String currencyId, String currencyName) async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo.write("res.partner", [user.result.partnerId],
            {"app_currency_id": currencyName}).then((OdooResponse res) {
          if (!res.hasError()) {
            someMap['result'] = {
              'id': user.result.partnerId,
              'app_currency_id': currencyName
            };
            Partner partner = Partner.fromJson(someMap);
            savePartner(json.encode(partner));
            widget.onChangeCurrency(currencyName);
          }
        });
      }
    });
  }

  // _updateProductList(){
  //   // widget.getCurrency();
  //   widget.callback("Hello");
  // }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getCurrency();
      // _selectedCurrency =
      //     _odoocurrencyList.isEmpty ? hideLoading() : _odoocurrencyList.first;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  // @override
  // Widget build(BuildContext context) {
  //   return Padding(
  //     padding: const EdgeInsets.only(left: 0, right: 0),

  //     child: Container(
  //       alignment: Alignment.centerRight,
  //         height: 25, //gives the height of the dropdown button
  //         width: MediaQuery.of(context).size.width /
  //             5, //gives the width of the dropdown button
  //         decoration: BoxDecoration(
  //             borderRadius: BorderRadius.all(Radius.circular(3)),
  //             color: Colors.white),
  //         child: Theme(
  //             data: Theme.of(context).copyWith(
  //                 canvasColor:
  //                     Colors.white, // background color for the dropdown items
  //                 buttonTheme: ButtonTheme.of(context).copyWith(
  //                   alignedDropdown:
  //                       true, //If false (the default), then the dropdown's menu will be wider than its button.
  //                 )),
  //             child: DropdownButtonHideUnderline(
  //               child: DropdownButton<OdooCurrency>(
  //                 iconEnabledColor:
  //                     Color(0xFF595959), // icon color of the dropdown button
  //                 items: _odoocurrencyList.map((data) {
  //                   return DropdownMenuItem<OdooCurrency>(
  //                     value: data,
  //                     child: Text(
  //                       data.name,
  //                       style: TextStyle(fontSize: 15),
  //                     ),
  //                   );
  //                 }).toList(),
  //                 hint: Text(
  //                   "USD",
  //                   style: TextStyle(color: Color(0xFF8B8B8B), fontSize: 15),
  //                 ), // setting hint
  //                 value: this._selectedCurrency,
  //                 onChanged: (OdooCurrency data) {
  //                   setState(() {
  //                     this._selectedCurrency = data; // saving the selected value
  //                   });
  //                   getOdooInstance().then((odoo) {
  //                     widget.onChangeCurrency(data.name);
  //                   });
  //                 },
  //               ),
  //             ))),
  //   );
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          title: Text(
            "Currency",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
        ),
        body: ListView.separated(
          itemCount: _odoocurrencyList.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(_odoocurrencyList[index].name),
              onTap: () {
                setCurrency(
                    _odoocurrencyList[index].id, _odoocurrencyList[index].name);
                Navigator.pop(context);
              },
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
        ));
  }
}
