import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/product_review.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class WidgetProductReview extends StatefulWidget {
  final String productId;
  WidgetProductReview(this.productId);

  @override
  _WidgetProductReviewState createState() => _WidgetProductReviewState();
}

class _WidgetProductReviewState extends Base<WidgetProductReview> {
  List<ProductReview> _productReview = [];

  getProductRating() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("reviews.ratings", [
          ['rating_product_id', '=', int.parse(widget.productId)]
        ], [
          'id',
          'message_rate',
          'review',
          'rating_product_id'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    _productReview.add(new ProductReview(
                        id: i["id"].toString(),
                        messagerate: i["message_rate"].toString(),
                        review: i["review"]));
                  }
                });
              }
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getProductRating();
      // _selectedAttribute = _attribute.isEmpty? hideLoading(): _attribute.first;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Flexible(
      child: ListView.builder(
        itemCount: _productReview.length,
        itemBuilder: (context, index) {
          return ListTile(
            // leading: FlutterLogo(size: 56.0),
            title: Text(_productReview[index].review),
            subtitle: RatingBarIndicator(
                rating: double.parse(_productReview[index].messagerate),
                itemBuilder: (context, index) => Icon(
                      Icons.star,
                      color: Colors.amber,
                    ),
                itemCount: 5,
                itemSize: 15.0,
                unratedColor: Colors.amber.withAlpha(50),
                direction: Axis.horizontal),
            // trailing: Icon(Icons.more_vert),
          );
        },
      ),
    );
  }
}
