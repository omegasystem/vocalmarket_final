import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/product.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';

class SortingSheet extends StatefulWidget {
  final String categId;
  final List<Products> productList;
  final void Function(List) updateList;
  SortingSheet({this.categId, this.productList, this.updateList});

  @override
  _SortingSheetState createState() => _SortingSheetState();
}

class _SortingSheetState extends Base<SortingSheet> {
  Map<String, List> filterMap = {};
  List<Products> _products = [];
  List findProduct = [];
  String selectedRadioTile;

  _searchProduct(loadoffset,
      [String toCurrency = "INR", String order = "id asc"]) async {
    print(
        "_searchProduct------------------$findProduct---------------${widget.categId}");
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        print("isInternet--------------------------$isInternet");
        odoo
            .searchRead(
                "product.template",
                [
                  ['id', 'in', findProduct],
                  ["public_categ_ids", "child_of", int.parse(widget.categId)]
                ],
                [
                  'id',
                  'name',
                  'image',
                  'lst_price',
                  'label_ept_id',
                  'description_sale',
                  'attribute_line_ids'
                ],
                offset: loadoffset,
                limit: 6,
                order: order,
                context: {'toCurrency': toCurrency})
            .then(
          (OdooResponse res) {
            if (!res.hasError()) {
              String session = getSession();
              session = session.split(",")[0].split(";")[0];
              // if (this.mounted) {
              // setState(() {
              // toCurrency = toCurrency;
              for (var i in res.getRecords()) {
                print("shorting-----------------------${i["id"]}");
                final data = i["attribute_line_ids"] as List;
                _products.add(new Products(
                    id: i["id"].toString(),
                    name: i["name"],
                    price: i["lst_price"],
                    imageUrl: getURL() +
                        "/web/image?model=product.template&field=image&" +
                        session +
                        "&id=" +
                        i["id"].toString(),
                    description: i["description_sale"] is! bool
                        ? i["description_sale"]
                        : i["name"],
                    productLabel: i["label_ept_id"] is! bool
                        ? i["label_ept_id"][1].toString()
                        : "false",
                    symbol: i["symbol"],
                    attributeCheck: data.isNotEmpty ? "yes" : "no"));
              }
              widget.updateList(_products);
              // hideLoading();
              // });
              // }
            }
          },
        );
      }
    });
  }

  _getProductList() {
    for (var i in widget.productList) {
      findProduct.add(int.parse(i.id));
    }
  }

  void setSelectedRadioTile(String val) {
    setState(() {
      selectedRadioTile = val;
    });
    getOdooInstance().then((odoo) {
      _searchProduct(0, "INR", val);
    });
    Navigator.pop(context);
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      _getProductList();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 500,
      child: Padding(
          padding: const EdgeInsets.all(30.0),
          child: new Column(
            children: <Widget>[
              RadioListTile(
                value: "asc",
                groupValue: selectedRadioTile,
                title: Text("Price"),
                subtitle: Text("Low to High"),
                onChanged: (val) {
                  setSelectedRadioTile("list_price $val");
                },
                activeColor: Colors.blue,
                selected: true,
              ),
              RadioListTile(
                value: "desc",
                groupValue: selectedRadioTile,
                title: Text("Price"),
                subtitle: Text("High to Low"),
                onChanged: (val) {
                  setSelectedRadioTile("list_price $val");
                },
                activeColor: Colors.blue,
                selected: false,
              ),
              RadioListTile(
                value: "desc",
                groupValue: selectedRadioTile,
                title: Text("Name"),
                subtitle: Text("A to Z"),
                onChanged: (val) {
                  setSelectedRadioTile("name $val");
                },
                activeColor: Colors.blue,
                selected: false,
              ),
              RadioListTile(
                value: "asc",
                groupValue: selectedRadioTile,
                title: Text("Name"),
                subtitle: Text("Z to A"),
                onChanged: (val) {
                  setSelectedRadioTile("name $val");
                },
                activeColor: Colors.blue,
                selected: false,
              ),
            ],
          )),
    );
  }
}
