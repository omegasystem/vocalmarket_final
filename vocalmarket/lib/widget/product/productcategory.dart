import 'package:flutter/material.dart';
import 'package:vocalmarket/app_translations.dart';
// import 'package:vocalmarket/app_translations.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/data/pojo/categories.dart';
// import 'package:vocalmarket/widget/product/odooproduct.dart';
import 'package:vocalmarket/screens/allproducts.dart';
import 'package:vocalmarket/widget/text/titletext.dart';

class ProductCategory extends StatefulWidget {
  @override
  _ProductCategoryState createState() => _ProductCategoryState();
}

class _ProductCategoryState extends Base<ProductCategory> {
  List<Categories> _categories = [];

  _getCategory() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead(
            "product.public.category", [], ['id', 'name', 'image']).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                String session = getSession();
                session = session.split(",")[0].split(";")[0];
                setState(() {
                  for (var i in res.getRecords()) {
                    _categories.add(new Categories(
                      id: i["id"].toString(),
                      name: i["name"],
                      imgurl: getURL() +
                          "/web/image?model=product.public.category&field=image&" +
                          session +
                          "&id=" +
                          i["id"].toString(),
                    ));
                  }
                });
              }
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      _getCategory();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              TitleText(
                text: AppTranslations.of(context).text("CATEGORIES"),
                color: Colors.black,
                fontSize: 20.0,
              ),
              // Icon(
              //   Icons.arrow_right
              // )
              // GestureDetector(
              //   onTap: () => {
              //     print('View All'),
              //   },
              //   child: Text("View All",
              //       style: TextStyle(
              //         color: Colors.grey,
              //         fontSize: 12.0,
              //         fontWeight: FontWeight.w600,
              //       )),
              // ),
            ],
          ),
        ),
        Column(
          children: <Widget>[
            Container(height: 20.0),
            Container(
              color: Colors.transparent,
              height: 150,
              child: ListView.builder(
                // gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                //   crossAxisCount: 4,
                //   childAspectRatio: MediaQuery.of(context).size.width /
                //       (MediaQuery.of(context).size.height),
                // ),
                shrinkWrap: true,
                scrollDirection: Axis.horizontal,
                itemCount: _categories.length,
                itemBuilder: (BuildContext context, int index) {
                  // Song data = widget.alubums[index];
                  return GestureDetector(
                    onTap: () => {
                      Navigator.push(
                        context,
                        MaterialPageRoute(
                          builder: (context) => AllProducts(
                              categId: _categories[index].id,
                              categName: _categories[index].name),
                        ),
                      )
                    },
                    child: Container(
                      width: 140,
                      child: Padding(
                        padding: const EdgeInsets.only(left: 20.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Center(
                              child: ClipRRect(
                                // borderRadius: BorderRadius.circular(20.0),
                                child: Image(
                                  height: 90.0,
                                  width: 90.0,
                                  image:
                                      NetworkImage(_categories[index].imgurl),
                                  fit: BoxFit.fitWidth,
                                ),
                              ),
                            ),
                            SizedBox(
                              height: 10,
                            ),
                            Center(
                              child: TitleText(
                                text: _categories[index].name.toUpperCase(),
                                fontSize: 10.0,
                                fontWeight: FontWeight.w600,
                                // textOverflow: TextOverflow.fade,
                              ),
                            ),
                            // Text(
                            //   _categories[index].name,
                            //   style: TextStyle(
                            //     fontSize: 12.0,
                            //     fontWeight: FontWeight.w600,
                            //   ),
                            //   maxLines: 1,
                            //   overflow: TextOverflow.ellipsis,
                            // ),
                            SizedBox(
                              height: 10,
                            ),
                          ],
                        ),
                      ),
                    ),
                  );
                },
              ),
            ),
          ],
        )
      ]),
    );
  }
}
