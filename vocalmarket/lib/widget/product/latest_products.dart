import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:vocalmarket/app_translations.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/product.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';
import 'package:vocalmarket/screens/product_details.dart';
// import 'package:vocalmarket/odoo/data/pojo/categories.dart';
// import 'package:vocalmarket/widget/product/product_card.dart';
import 'package:vocalmarket/widget/text/titletext.dart';

class LatestProducts extends StatefulWidget {
  @override
  _LatestProductsState createState() => _LatestProductsState();
}

class _LatestProductsState extends Base<LatestProducts> {
  List<Products> _products = [];
  String partnerId = "";
  String variantId;
  String toCurrency = "INR";

  getLatestProduct([String order = "id asc"]) async {
    isConnected().then((isInternet) {
      toCurrency = partner.partnerresult.appCurrencyName;
      if (isInternet) {
        // EasyLoading.show();
        odoo
            .searchRead(
                "product.template",
                [
                  ['active', '=', true]
                ],
                [
                  'id',
                  'name',
                  'image',
                  'lst_price',
                  'label_ept_id',
                  'product_brand_ept_id',
                  'description_sale',
                  'attribute_line_ids'
                ],
                order: "create_date asc",
                limit: 5,
                context: {'toCurrency': toCurrency})
            .then(
          (OdooResponse res) {
            if (!res.hasError()) {
              String session = getSession();
              session = session.split(",")[0].split(";")[0];
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    final data = i["attribute_line_ids"] as List;
                    print("i--------------------$i");
                    _products.add(new Products(
                        id: i["id"].toString(),
                        name: i["name"],
                        price: i["lst_price"],
                        imageUrl: getURL() +
                            "/web/image?model=product.template&field=image&" +
                            session +
                            "&id=" +
                            i["id"].toString(),
                        description: i["description_sale"] is! bool
                            ? i["description_sale"]
                            : i["name"],
                        productLabel: i["label_ept_id"] is! bool
                            ? i["label_ept_id"][1].toString()
                            : "false",
                        productbrand: i["product_brand_ept_id"] is! bool
                            ? i["product_brand_ept_id"][1].toString()
                            : "false",
                        symbol: "₹",
                        attributeCheck: data.isNotEmpty ? "yes" : "no"));
                  }
                  // EasyLoading.dismiss();
                });
              }
            }
          },
        );
      }
    });
    return _products;
  }

  _getUserData() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead(Strings.res_users, [
          ["id", "=", getUID()]
        ], [
          "partner_id"
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              final result = res.getResult()['records'][0];
              if (this.mounted) {
                setState(() {
                  partnerId = result['partner_id'][0].toString();
                });
              }
            } else {
              showMessage("Warning", res.getErrorMessage());

              // EasyLoading.showSuccess(res.getErrorMessage());
            }
          },
        );
      }
    });
  }

  addtocart(String variantId) async {
    print("addtocart---------------$variantId");
    final prefs = await SharedPreferences.getInstance();
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo
            .searchRead(
                'product.product',
                [
                  ['product_tmpl_id', "=", int.parse(variantId)]
                ],
                ['id'],
                limit: 1)
            .then((OdooResponse res) {
          if (!res.hasError()) {
            setState(() {
              for (var i in res.getRecords()) {
                variantId = i['id'].toString();
              }
            });
          }
        });
        odoo.searchRead('sale.order', [
          ['id', '=', prefs.getInt('order_id')],
          ['state', '=', 'draft']
        ], [
          'id'
        ]).then((OdooResponse so) {
          if (so.getResult()['length'] == 0) {
            odoo.create('sale.order', {
              'partner_id': int.parse(partnerId)
            }).then((OdooResponse newso) {
              if (!newso.hasError()) {
                prefs.setInt('order_id', newso.getResult());
                createSoLine(newso.getResult(), variantId);
              }
            });
          } else {
            setState(() {
              createSoLine(prefs.getInt('order_id'), variantId);
            });
          }
        });
      }
    });
  }

  createSoLine(result, String variantId) async {
    odoo.create("sale.order.line", {
      'order_id': result,
      'product_id': int.parse(variantId)
    }).then((OdooResponse soLine) {
      // EasyLoading.showSuccess("Product Added in Cart!");
    });
  }

  getProductProduct(String templateId) async {
    print("getProductProduct----------------$templateId");
    odoo
        .searchRead(
            'product.product',
            [
              ['product_tmpl_id', "=", int.parse(templateId)]
            ],
            ['id'],
            limit: 1)
        .then((OdooResponse res) {
      if (!res.hasError()) {
        setState(() {
          for (var i in res.getRecords()) {
            variantId = i['id'].toString();
          }
        });
        addtocart(variantId);
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getLatestProduct();
      _getUserData();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(children: <Widget>[
        Padding(
          padding: const EdgeInsets.only(left: 20.0, right: 20.0, bottom: 10.0),
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              TitleText(
                text: AppTranslations.of(context).text("LATEST_PRODCUTS"),
                color: Colors.black,
                fontSize: 20.0,
              ),
              Icon(Icons.arrow_right)
            ],
          ),
        ),
        _products.isNotEmpty
            ? Column(
                children: <Widget>[
                  Container(height: 20.0),
                  Container(
                    color: Colors.transparent,
                    height: 250,
                    child: ListView.builder(
                      shrinkWrap: true,
                      scrollDirection: Axis.horizontal,
                      itemCount: _products.length,
                      itemBuilder: (BuildContext context, int index) {
                        return Container(
                            height: 300,
                            width: 150,
                            decoration: BoxDecoration(
                              color: Colors.white,
                              borderRadius:
                                  BorderRadius.all(Radius.circular(20)),
                              boxShadow: <BoxShadow>[
                                BoxShadow(
                                    color: Color(0xfff8f8f8),
                                    blurRadius: 15,
                                    spreadRadius: 10),
                              ],
                            ),
                            margin: EdgeInsets.symmetric(vertical: 0),
                            padding: EdgeInsets.symmetric(
                                horizontal: 20, vertical: 10),
                            child: GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) => ProductDetails(
                                            productId: _products[index].id,
                                            name: _products[index].name,
                                            imgPath: _products[index].imageUrl,
                                            price: _products[index]
                                                .price
                                                .toString(),
                                            partnerId: partnerId,
                                            symbol: _products[index].symbol,
                                            description:
                                                _products[index].description,
                                            attributeCheck: _products[index]
                                                .attributeCheck)));
                              },
                              child: Stack(
                                alignment: Alignment.center,
                                children: <Widget>[
                                  // Positioned(
                                  //     left: 0,
                                  //     top: 0,
                                  //     child: IconButton(
                                  //         icon: Icon(Icons.favorite, color: Colors.red),
                                  //         onPressed: () {})),
                                  Column(
                                    crossAxisAlignment:
                                        CrossAxisAlignment.center,
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    children: <Widget>[
                                      SizedBox(height: 15),
                                      Stack(
                                        alignment: Alignment.center,
                                        children: <Widget>[
                                          Image.network(
                                            _products[index].imageUrl,
                                            fit: BoxFit.fill,
                                            height: 80.0,
                                          )
                                        ],
                                      ),
                                      TitleText(
                                        text: _products[index].symbol +
                                            _products[index]
                                                .price
                                                .toStringAsFixed(2),
                                        fontSize: 16,
                                      ),
                                      RaisedButton(
                                        onPressed: () {
                                          getOdooInstance().then((odoo) async {
                                            getProductProduct(
                                                _products[index].id);
                                          });
                                          final snackBar = SnackBar(
                                            content: Text(AppTranslations.of(
                                                    context)
                                                .text("Product_added_in_cart")),
                                          );
                                          Scaffold.of(context)
                                              .showSnackBar(snackBar);
                                        },
                                        child: Text(
                                            AppTranslations.of(context)
                                                .text("add_to_cart"),
                                            style: TextStyle(
                                              fontSize: 10.0,
                                              color: Colors.white,
                                            )),
                                        color: Colors.black,
                                        shape: RoundedRectangleBorder(
                                          borderRadius:
                                              BorderRadius.circular(15.0),
                                        ),
                                      )
                                      // Container(
                                      //   height: double.infinity,
                                      //   child: RaisedButton(
                                      //     child: Text(
                                      //       "ADD TO Cart",
                                      //       style: Theme.of(context).textTheme.button.copyWith(
                                      //             color: Colors.white,
                                      //           ),
                                      //     ),
                                      //     onPressed: () {
                                      //       // getOdooInstance().then((odoo) {
                                      //       //   addtocart();
                                      //       // });
                                      //     },
                                      //     color: Colors.black,
                                      //     shape: RoundedRectangleBorder(
                                      //       borderRadius: BorderRadius.circular(15.0),
                                      //     ),
                                      //   ),
                                      // )
                                    ],
                                  ),
                                ],
                              ),
                            ));
                      },
                    ),
                  ),
                  Container(height: 20.0),
                ],
              )
            : new Center(child: new CircularProgressIndicator())
      ]),
    );
  }
}
