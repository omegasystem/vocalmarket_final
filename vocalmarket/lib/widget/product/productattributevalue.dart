// import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/data/pojo/filter.dart';
// import 'package:vocalmarket/widget/filtertabs.dart';
// import 'package:vocalmarket/widget/product/productattributevalue.dart';

class ProductAttributeValue extends StatefulWidget {
  final String attributeId;
  final String productId;
  final String filterName;
  final List atttibuteList;

  ProductAttributeValue(
      {Key key,
      this.attributeId,
      this.filterName,
      this.productId,
      this.atttibuteList})
      : super(key: key);

  @override
  _ProductAttributeValueState createState() => _ProductAttributeValueState();
}

class _ProductAttributeValueState extends Base<ProductAttributeValue> {
  List<FiltersValue> _filtersvalueList = [];
  Map<String, List> filterMap = {};
  Map<String, List> attributeListMap = {};
  FiltersValue _selectedFilter;
  List<FiltersValue> _filterValueMap = [];
  List normalList = [];

  getAttributeValue() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo.searchRead("product.template.attribute.value", [
          ["product_tmpl_id", "=", int.parse(widget.productId)],
          [
            "product_attribute_value_id.attribute_id",
            "=",
            int.parse(widget.attributeId)
          ]
        ], [
          'id',
          'name',
          'attribute_id'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                String session = getSession();
                session = session.split(",")[0].split(";")[0];
                setState(() {
                  for (var i in res.getRecords()) {
                    if (widget.atttibuteList
                        .contains(i["attribute_id"][0].toString())) {
                      _filtersvalueList.add(new FiltersValue(
                        id: i["id"].toString(),
                        name: i["name"],
                      ));
                    }
                  }
                  // hideLoading();
                });
              }
            }
          },
        );
      }
    });
  }

  // getProductProduct() async {
  //   isConnected().then((isInternet) {
  //     if (isInternet) {
  //       odoo.searchRead("product.product", [['product_tmpl_id', "=", int.parse(widget.productId)]], ['id', 'name', 'attribute_value_ids']).then(
  //         (OdooResponse res) {
  //           if (!res.hasError()) {
  //             setState(() {
  //               for (var i in res.getRecords()) {
  //                print("getAttribute-------------------$i");
  //               }
  //             });
  //           }
  //         },
  //       );
  //     }
  //   });
  // }

  getProductProductLine() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("product.attribute.value", [], ['id', 'name']).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    _filterValueMap.add(new FiltersValue(
                      id: i["id"].toString(),
                      name: i["name"],
                    ));
                  }
                });
              }
            }
          },
        );
      }
    });
  }

  setAtttibute(valuesIds) {
    for (var data in valuesIds) {
      _filterValueMap.forEach((value) {
        // print("values==================${value.id}");
        // print("data-----------------$data");
        if (int.parse(value.id) == int.parse(data)) {
          print("matched---------------${_filterValueMap[data]}");
        }
      });
    }
    // _filterValueMap.forEach((data){

    // });
  }

  var items = [];
  getAttribute() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("product.product", [
          ['product_tmpl_id', "=", int.parse(widget.productId)]
        ], [
          'id',
          'name',
          'attribute_value_ids',
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    setAtttibute(i['attribute_value_ids']);
                    // items.add(List(i['attribute_value_ids']));
                  }
                });
              }
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getAttributeValue();
      getProductProductLine();
      getAttribute();

      // _selectedFilter =
      //     _filtersvalueList.isEmpty ? hideLoading() : _filtersvalueList.first;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  // Widget getDropDown() {
  //   var items = _filterValue.map((data) {
  //     return new DropdownMenuItem<FiltersValue>(
  //       value: data,
  //       child: new Text(data.name),
  //     );
  //   }).toList();

  //   return DropdownButton<FiltersValue>(
  //       value: this._filtersvalueList.first,
  //       onChanged: (FiltersValue data) {
  //         setState(() {
  //           this._selectedFilter = data;
  //         });
  //       },
  //       items: items);
  // }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 30, right: 30, bottom: 24, top: 12),
      child: Container(
          height: 55, //gives the height of the dropdown button
          width: MediaQuery.of(context).size.width /
              2, //gives the width of the dropdown button
          decoration: BoxDecoration(
              borderRadius: BorderRadius.all(Radius.circular(3)),
              color: Color(0xFFF2F2F2)),
          child: Theme(
              data: Theme.of(context).copyWith(
                  canvasColor:
                      Colors.white, // background color for the dropdown items
                  buttonTheme: ButtonTheme.of(context).copyWith(
                    alignedDropdown:
                        true, //If false (the default), then the dropdown's menu will be wider than its button.
                  )),
              child: DropdownButtonHideUnderline(
                child: DropdownButton<FiltersValue>(
                  iconEnabledColor:
                      Color(0xFF595959), // icon color of the dropdown button
                  items: _filtersvalueList.map((data) {
                    return DropdownMenuItem<FiltersValue>(
                      value: data,
                      child: Text(
                        data.name,
                        style: TextStyle(fontSize: 15),
                      ),
                    );
                  }).toList(),
                  hint: Text(
                    "Select",
                    style: TextStyle(color: Color(0xFF8B8B8B), fontSize: 15),
                  ), // setting hint
                  value: this._selectedFilter,
                  onChanged: (FiltersValue data) {
                    setState(() {
                      this._selectedFilter = data; // saving the selected value
                    });
                  },
                ),
              ))),
    );

    // return Row(
    //   mainAxisAlignment: MainAxisAlignment.spaceEvenly,
    //   children: <Widget>[
    //     ChoiceChip(
    //       label: Text(_filtersvalue[index].name),
    //       selected: _value == index,
    //       onSelected: (bool selected) {
    //         setState(() {
    //           _value = selected ? index : null;
    //         });
    //       },
    //     ),
    //     Container(width: 5.0)
    //   ],
    // );
  }
}
