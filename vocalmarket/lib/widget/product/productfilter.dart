import 'package:flutter/material.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/product.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/data/pojo/filter.dart';
import 'package:vocalmarket/widget/filtertabs.dart';

class ProductFilter extends StatefulWidget {
  final String categId;
  final List<Products> productList;
  final void Function(List) updateList;
  ProductFilter({this.categId, this.productList, this.updateList});

  @override
  _ProductFilterState createState() => _ProductFilterState();
}

class _ProductFilterState extends Base<ProductFilter> {
  List<Filters> _filters = [];
  List _filterIds = [];
  List findProduct = [];
  Map<String, List> filterMap = {};
  List<Products> _products = [];
  String toCurrency = "";

  _getFilters() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("category.attribute.line", [
          ['category_id', '=', int.parse(widget.categId)]
        ], [
          'id',
          'category_attribute_id'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    _filters.add(new Filters(
                      id: i["category_attribute_id"][0].toString(),
                      name: i["category_attribute_id"][1],
                    ));
                  }
                });
              }
            }
          },
        );
      }
    });
  }

  _getProductList() {
    for (var i in widget.productList) {
      findProduct.add(int.parse(i.id));
    }
  }

  _searchProduct(loadoffset, [String order = "id asc"]) async {
    print(
        "_searchProduct------------------------$findProduct------------$_filterIds-----${widget.categId}");
    isConnected().then((isInternet) {
      if (isInternet) {
        toCurrency = partner.partnerresult.appCurrencyName;
        odoo
            .searchRead(
                "product.template",
                [
                  ['id', 'in', findProduct],
                  ['attribute_line_ids.value_ids', 'in', _filterIds],
                  ["public_categ_ids", "child_of", int.parse(widget.categId)]
                ],
                [
                  'id',
                  'name',
                  'image',
                  'lst_price',
                  'label_ept_id',
                  'description_sale',
                  'attribute_line_ids'
                ],
                offset: loadoffset,
                limit: 6,
                order: order,
                context: {'toCurrency': toCurrency})
            .then(
          (OdooResponse res) {
            if (!res.hasError()) {
              print("res--------------${res.getResult()}");
              String session = getSession();
              session = session.split(",")[0].split(";")[0];
              // if (this.mounted) {
              // setState(() {
              print("setstate-------------------");
              toCurrency = toCurrency;
              for (var i in res.getRecords()) {
                print("_searchProduct------------------$i");
                final data = i["attribute_line_ids"] as List;
                _products.add(new Products(
                    id: i["id"].toString(),
                    name: i["name"],
                    price: i["lst_price"],
                    imageUrl: getURL() +
                        "/web/image?model=product.template&field=image&" +
                        session +
                        "&id=" +
                        i["id"].toString(),
                    description: i["description_sale"] is! bool
                        ? i["description_sale"]
                        : i["name"],
                    productLabel: i["label_ept_id"] is! bool
                        ? i["label_ept_id"][1].toString()
                        : "false",
                    symbol: i["symbol"],
                    attributeCheck: data.isNotEmpty ? "yes" : "no"));
              }
              widget.updateList(_products);
              // hideLoading();
              // });
              // }
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      _getProductList();
      _getFilters();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  void updateFilter(int val) {
    setState(() {
      _filterIds = List.from(_filterIds)..add(val);
    });
  }

  @override
  Widget build(BuildContext context) {
    var deviceSize = MediaQuery.of(context).size;
    return Scaffold(
        appBar: AppBar(
          title: Text("Filter"),
          backgroundColor: Colors.black,
        ),
        body: _filters.isNotEmpty
            ? Container(
                width: deviceSize.width,
                child: ListView(
                  children: <Widget>[
                    Column(
                        children: List.generate(_filters.length, (index) {
                      return Column(children: <Widget>[
                        new Padding(
                            padding:
                                new EdgeInsets.only(top: 10.0, bottom: 6.0),
                            child: new Row(children: [
                              new Expanded(
                                child: new Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    new Container(
                                        padding: const EdgeInsets.only(
                                            bottom: 8.0, left: 10.0),
                                        child: new Text(_filters[index].name,
                                            textAlign: TextAlign.start,
                                            style: new TextStyle(
                                                color: Colors.black,
                                                fontSize: 20.0,
                                                fontWeight: FontWeight.bold))),
                                  ],
                                ),
                              ),
                            ])),
                        FilterTabs(
                            filterId: _filters[index].id,
                            onAddFilter: (int val) {
                              updateFilter(val);
                            }),
                      ]);
                    })),
                    Column(
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(
                              left: 20.0, right: 20, bottom: 15),
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              ButtonTheme(
                                buttonColor: Colors.black,
                                minWidth: double.infinity,
                                height: 40.0,
                                child: RaisedButton(
                                  onPressed: () {
                                    getOdooInstance().then((odoo) {
                                      _searchProduct(0);
                                    });
                                    Navigator.pop(context);
                                  },
                                  child: Text(
                                    "Apply Filters",
                                    style: TextStyle(
                                        color: Colors.white, fontSize: 16),
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                      ],
                    ),
                  ],
                ))
            : Container(
                child: new Center(child: new CircularProgressIndicator())));
  }
}
