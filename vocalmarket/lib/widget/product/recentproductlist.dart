import 'package:flutter/material.dart';

// import 'featured_card.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/data/pojo/product.dart';

class RecentProductsList extends StatefulWidget {
  @override
  _RecentProductsListState createState() => _RecentProductsListState();
}

class _RecentProductsListState extends Base<RecentProductsList> {
  List<Products> _products = [];

  getOdooProducts() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("product.product", [], ['id', 'name', 'image']).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                String session = getSession();
                session = session.split(",")[0].split(";")[0];
                setState(() {
                  for (var i in res.getRecords()) {
                    _products.add(new Products(
                      id: i["id"].toString(),
                      name: i["name"],
                      imageUrl: getURL() +
                          "/web/image?model=product.product&field=image&" +
                          session +
                          "&id=" +
                          i["id"].toString(),
                    ));
                  }
                });
              }
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getOdooProducts();
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 150,
      margin: const EdgeInsets.symmetric(vertical: 15),
      child: ListView.builder(
        scrollDirection: Axis.horizontal,
        itemCount: _products.length,
        itemBuilder: (ctx, i) {
          String imgPath = _products[i].imageUrl;
          return GestureDetector(
            onTap: () {
              // Navigator.push(
              //   context,
              //   MaterialPageRoute(
              //     builder: (ctx) => DetailsScreen(id: i),
              //   ),
              // );
            },
            child: Padding(
              padding: const EdgeInsets.symmetric(horizontal: 9.0),
              child: Container(
                decoration: BoxDecoration(
                  borderRadius: BorderRadius.circular(9.0),
                  color: Colors.white,
                ),
                child: ClipRRect(
                    borderRadius: BorderRadius.circular(9.0),
                    child: Image.network(imgPath)),
              ),
            ),
          );
        },
      ),
    );
  }
}
