import 'package:flutter/material.dart';
import 'package:vocalmarket/screens/newuser.page.dart';
import 'package:vocalmarket/screens/resetpass.dart';
// import 'package:vocalmarket/widget/forgot.dart';

class FirstTime extends StatefulWidget {
  @override
  _FirstTimeState createState() => _FirstTimeState();
}

class _FirstTimeState extends State<FirstTime> {
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30, left: 50),
      child: Container(
        alignment: Alignment.topRight,
        height: 20,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton(
              padding: EdgeInsets.all(0),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => NewUser()));
              },
              child: Text(
                'Don\'t have an account?',
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.black,
                ),
                textAlign: TextAlign.right,
              ),
            ),
            Spacer(),
            FlatButton(
              padding: EdgeInsets.only(right: 50.0),
              onPressed: () {
                Navigator.push(context,
                    MaterialPageRoute(builder: (context) => ResetPass()));
              },
              child: Text(
                'Reset Password',
                style: TextStyle(
                  fontSize: 12,
                  color: Colors.black,
                ),
                textAlign: TextAlign.right,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
