import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';
import 'package:vocalmarket/screens/login.dart';

class Forgot extends StatefulWidget {
  final String email;

  Forgot(this.email);

  @override
  _ForgotState createState() => _ForgotState();
}

class _ForgotState extends Base<Forgot> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  _forgot(String email) {
    print("signup-----------------------$email--------------");
    getOdooInstance().then((odoo) {
      isConnected().then((isInternet) {
        if (isInternet) {}
      });
    });
  }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(top: 30, left: 50, right: 50),
      child: Container(
        alignment: Alignment.topRight,
        height: 50,
        decoration: BoxDecoration(
          color: Colors.black,
          borderRadius: BorderRadius.circular(0),
        ),
        child: FlatButton(
          onPressed: () {
            print("widget.email-----------------_${widget.email}");
            _forgot(widget.email);
          },
          child: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
                'Forgot Password',
                style: TextStyle(color: Colors.white, fontFamily: "Quicksand"),
              ),
              Icon(
                Icons.arrow_forward,
                color: Colors.white,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
