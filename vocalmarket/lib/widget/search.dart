import 'package:flutter/material.dart';
import 'package:vocalmarket/app_translations.dart';
import 'package:vocalmarket/base.dart';

import 'package:vocalmarket/odoo/data/pojo/product.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/screens/allproducts.dart';
import 'package:vocalmarket/screens/search_product.dart';

class Search extends StatefulWidget {
  @override
  _SearchState createState() => _SearchState();
}

class _SearchState extends Base<Search> {
  String toCurrency = "";

  // getLatestProduct([String name = "smart watch", String order = "id asc"]) async {
  //   isConnected().then((isInternet) {
  //     toCurrency = partner.partnerresult.appCurrencyName;
  //     print("toCurrency---------------$toCurrency");
  //     if (isInternet) {
  //       odoo
  //           .searchRead(
  //               "product.template",
  //               [
  //                 ["name", "ilike", name]
  //               ],
  //               [
  //                 'id',
  //                 'name',
  //                 'image',
  //                 'lst_price',
  //                 'label_ept_id',
  //                 'product_brand_ept_id',
  //                 'description_sale',
  //                 'attribute_line_ids'
  //               ],
  //               order: "create_date asc",
  //               limit: 5,
  //               context: {'toCurrency': toCurrency})
  //           .then(
  //         (OdooResponse res) {
  //           if (!res.hasError()) {
  //             print("res-------------------${res.getResult()}");
  //             String session = getSession();
  //             session = session.split(",")[0].split(";")[0];
  //             if (this.mounted) {
  //               setState(() {
  //                 for (var i in res.getRecords()) {
  //                   final data = i["attribute_line_ids"] as List;
  //                   _products.add(new Products(
  //                       id: i["id"].toString(),
  //                       name: i["name"],
  //                       price: i["lst_price"],
  //                       imageUrl: getURL() +
  //                           "/web/image?model=product.template&field=image&" +
  //                           session +
  //                           "&id=" +
  //                           i["id"].toString(),
  //                       description: i["description_sale"] is! bool
  //                           ? i["description_sale"]
  //                           : i["name"],
  //                       productLabel: i["label_ept_id"] is! bool
  //                           ? i["label_ept_id"][1].toString()
  //                           : "false",
  //                       productbrand: i["product_brand_ept_id"] is! bool
  //                           ? i["product_brand_ept_id"][1].toString()
  //                           : "false",
  //                       symbol: i["symbol"],
  //                       attributeCheck: data.isNotEmpty ? "yes" : "no"));
  //                 }
  //                 // EasyLoading.dismiss();
  //               });
  //               print("-----------$_products");
  //               Navigator.push(context, MaterialPageRoute(builder: (context) => AllProducts(products: _products)));
  //             }
  //           }
  //         },
  //       );
  //     }
  //   });
  //   return _products;
  // }

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(15),
      child: Container(
        width: MediaQuery.of(context).size.width,
        decoration: BoxDecoration(
          color: Colors.blueGrey[50],
          borderRadius: BorderRadius.all(
            Radius.circular(15.0),
          ),
        ),
        child: TextField(
          textInputAction: TextInputAction.search,
          onSubmitted: (searchValue) {
            Navigator.push(
                context,
                MaterialPageRoute(
                    builder: (context) =>
                        SearchProducts(searchValue: searchValue)));
            // getOdooInstance().then((odoo) {
            //   getLatestProduct();
            // });
            // print("onSubmitted-----------$_products");

            //
          },
          onChanged: (searchValue) {
            // print("onChanged--------------$newValue");
          },
          style: TextStyle(
            fontSize: 15.0,
            color: Colors.blueGrey[300],
          ),
          decoration: InputDecoration(
            contentPadding: EdgeInsets.all(10.0),
            border: OutlineInputBorder(
              borderRadius: BorderRadius.circular(15.0),
              borderSide: BorderSide(
                color: Colors.white,
              ),
            ),
            enabledBorder: OutlineInputBorder(
              borderSide: BorderSide(
                color: Colors.white,
              ),
              borderRadius: BorderRadius.circular(15.0),
            ),
            hintText: AppTranslations.of(context).text("search_bar"),
            prefixIcon: Icon(
              Icons.search,
              color: Colors.blueGrey[300],
            ),
            hintStyle: TextStyle(
              fontSize: 15.0,
              color: Colors.blueGrey[300],
            ),
          ),
          maxLines: 1,
        ),
      ),
    );
  }
}
