import 'package:flutter/material.dart';
import 'package:vocalmarket/app_translations.dart';

class BottomBar extends StatefulWidget {
  final void Function(int) onChangeMenu;

  const BottomBar({this.onChangeMenu});

  @override
  _BottomBarState createState() => _BottomBarState();
}

class _BottomBarState extends State<BottomBar> {
  int selectedPosition = 0;

  @override
  Widget build(BuildContext context) {
    return BottomNavigationBar(
      items: [
        BottomNavigationBarItem(
            icon: Icon(Icons.home),
            title: Text(AppTranslations.of(context).text("Home"))),
        BottomNavigationBarItem(
            icon: Icon(Icons.list),
            title: Text(AppTranslations.of(context).text("Orders"))),
        BottomNavigationBarItem(
            icon: Icon(Icons.shopping_cart),
            title: Text(AppTranslations.of(context).text("Cart"))),
        BottomNavigationBarItem(
            icon: Icon(Icons.person),
            title: Text(AppTranslations.of(context).text("Account"))),
      ],
      currentIndex: selectedPosition,
      type: BottomNavigationBarType.fixed,
      backgroundColor: Colors.grey.shade100,
      selectedItemColor: Colors.blue,
      unselectedItemColor: Colors.black,
      onTap: (position) {
        setState(() {
          selectedPosition = position;
        });
        widget.onChangeMenu(position);
      },
    );
  }
}
