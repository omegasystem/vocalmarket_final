import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/data/pojo/filter.dart';

class FilterTabs extends StatefulWidget {
  final String filterId;
  final void Function(int) onAddFilter;

  FilterTabs({this.filterId, this.onAddFilter});

  @override
  _FilterTabsState createState() => _FilterTabsState();
}

class _FilterTabsState extends Base<FilterTabs> {
  List filterIds = [];
  List<FiltersValue> _filtersValue = [];
  final PageStorageBucket bucket = PageStorageBucket();

  _getCategoryValue() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo.searchRead("product.attribute.value", [
          ['attribute_id', '=', int.parse(widget.filterId)]
        ], [
          'id',
          'name',
          'attribute_id'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    _filtersValue.add(new FiltersValue(
                      id: i["id"].toString(),
                      name: i["name"],
                    ));
                  }
                  // hideLoading();
                });
              }
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      _getCategoryValue();
    });
  }

  List<String> selectedChoices = List();
  @override
  Widget build(BuildContext context) {
    return PageStorage(
      child: Container(
          child: Wrap(
              children: List.generate(_filtersValue.length, (index) {
        return Container(
          padding: EdgeInsets.all(2.0),
          child: ChoiceChip(
            label: Text(_filtersValue[index].name),
            selected: selectedChoices.contains(_filtersValue[index].name),
            onSelected: (selectedFilter) {
              setState(() {
                filterIds.add(_filtersValue[index].id);
                selectedChoices.contains(_filtersValue[index].name)
                    ? selectedChoices.remove(_filtersValue[index].name)
                    : selectedChoices.add(_filtersValue[index].name);
              });
              widget.onAddFilter(int.parse(_filtersValue[index].id));
            },
          ),
        );
      }))),
      bucket: bucket,
    );
  }
}
