import 'dart:io';
import 'package:flutter/material.dart';
import 'package:vocalmarket/odoo/data/services/odoo_api.dart';
import 'package:vocalmarket/screens/home.dart';
import 'package:vocalmarket/screens/login.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';
import 'package:vocalmarket/base.dart';

class MyHttpOverrides extends HttpOverrides {
  @override
  HttpClient createHttpClient(SecurityContext context) {
    return super.createHttpClient(context)
      ..badCertificateCallback =
          (X509Certificate cert, String host, int port) => true;
  }
}

void main() {
  HttpOverrides.global = new MyHttpOverrides();
  WidgetsFlutterBinding.ensureInitialized();
  runApp(MyApp());
}

class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends Base<MyApp> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
        debugShowCheckedModeBanner: false,
        title: Strings.app_title,
        theme: ThemeData(
          fontFamily: "Montserrat",
          primaryColor: Colors.black,
        ),
        home: FutureBuilder<Odoo>(
          future: getOdooInstance(),
          builder: (BuildContext context, AsyncSnapshot<Odoo> snapshot) {
            switch (snapshot.connectionState) {
              case ConnectionState.done:
                return isLoggedIn() ? Home() : Login();
              default:
                return new Container(
                  decoration: new BoxDecoration(color: Colors.white),
                  child: new Center(child: CircularProgressIndicator()),
                );
            }
          },
        ));
  }
}
