class Strings {
  static const String app_title = "WEARME";

  static const String internetMessage = "No Internet Connection!";
  static const String loginAgainMessage = "Setting Saved! Please Login!";
  static const String invalidUrlMessage =
      "Can't connect to the server! Please enter valid URL";
  static const String no_orders = "No Orders";

  // Route Name
  static const String route_home = "/home";
  static const String route_login = "/login";

  // static const String odoo_url = "http://10.0.2.2:8069";

  static const String odoo_url = "http://192.168.0.107:8069";
  // static const String odoo_url = "https://139.59.65.253";
  // static const String odoo_db = "stagingdev";
  static const String odoo_db = "vocalmarket_mobile";

  // Model name
  static const String res_partner = "res.partner";
  static const String res_users = "res.users";
  static const String sale_order = "sale.order";
  static const String sale_order_line = "sale.order.line";
  static const String product_template = "product.template";
  static const String product_product = "product.product";
  static const String product_pricelist = "product.pricelist";
  static const String app_banner = "app.banner";

  // static const String admin_email = "vijay@wearme.me";
  // static const String admin_pass = "1";

  static const String odoo_email = "admin";
  static const String odoo_pass = "a";
  static const String admin_email = "admin";
  static const String admin_pass = "a";
}
