class AppBanner {
  final String id;
  final String name;
  final String imgUrl;

  AppBanner({this.id, this.name, this.imgUrl});

  AppBanner.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        imgUrl = json['imgUrl']
      ;

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
      'imgUrl': imgUrl
    };
}