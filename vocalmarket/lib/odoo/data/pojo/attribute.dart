class Attribute {
  final String id;
  final String name;
  final String attributename;

  Attribute({this.id, this.name, this.attributename});

  Attribute.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        attributename = json['attributename']
      ;

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
      'attributename': attributename
    };
}