class OdooLanguage {
  final String id;
  final String name;

  OdooLanguage({this.id, this.name});

  OdooLanguage.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'];

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
    };
}