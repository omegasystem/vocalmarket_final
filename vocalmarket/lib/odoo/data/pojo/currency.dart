class OdooCurrency {
  final String id;
  final String name;

  OdooCurrency({this.id, this.name});

  OdooCurrency.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'];

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
    };
}