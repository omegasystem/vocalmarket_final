class ProductReview {
  final String id;
  final String messagerate;
  final String review;

  ProductReview({this.id, this.messagerate, this.review});

  ProductReview.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        messagerate = json['messagerate'],
        review = json['review']
      ;

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'messagerate': messagerate,
      'review': review
    };
}