class OdooCountry {
  final String id;
  final String name;

  OdooCountry({this.id, this.name});

  OdooCountry.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name']
      ;

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
    };
}