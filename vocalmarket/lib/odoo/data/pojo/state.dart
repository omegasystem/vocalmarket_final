class OdooState {
  final String id;
  final String name;

  OdooState({this.id, this.name});

  OdooState.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name']
      ;

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
    };
}