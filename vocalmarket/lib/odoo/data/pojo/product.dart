class Products {
  final String id;
  final String name;
  final String imageUrl;
  final List<int> productCateg;
  final double price;
  final String symbol;
  final String productLabel;
  final String productbrand;
  final String description;
  final String attributeCheck;

  Products({
    this.id, this.name, this.imageUrl, this.productCateg, this.price, this.symbol, this.productLabel, this.productbrand,
    this.description, this.attributeCheck
    });

  Products.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        imageUrl = json['imageUrl'],
        productCateg = json['productCateg'],
        price = json["price"],
        symbol = json["symbol"],
        productLabel = json["productLabel"],
        productbrand = json["productbrand"],
        description = json["description"],
        attributeCheck = json["attributeCheck"]
      ;

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
      'imageUrl': imageUrl,
      'productCateg': productCateg,
      'price': price,
      'symbol': symbol,
      'productLabel': productLabel,
      'productbrand': productbrand,
      'description': description,
      'attributeCheck': attributeCheck
    };
}

class ProductsProduct {
  final String id;
  final String name;
  final String imageUrl;
  final String price;
  final String uomId;

  ProductsProduct({this.id, this.name, this.imageUrl, this.price, this.uomId});

  ProductsProduct.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        imageUrl = json['imageUrl'],
        price = json["price"],
        uomId = json["uomId"]
      ;

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
      'imageUrl': imageUrl,
      'price': price,
      'uomId': uomId
    };
}

class ProductsImage {
  final String id;
  final String name;
  final String imageUrl;

  ProductsImage({this.id, this.name, this.imageUrl});

  ProductsImage.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        imageUrl = json['imageUrl']
      ;

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
      'imageUrl': imageUrl,
    };
}