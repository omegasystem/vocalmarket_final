class Filters {
  final String id;
  final String name;

  Filters({this.id, this.name});

  Filters.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'];

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
    };
}

class FiltersValue {
  final String id;
  final String name;

  FiltersValue({this.id, this.name});

  FiltersValue.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name']
      ;

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
    };
}
