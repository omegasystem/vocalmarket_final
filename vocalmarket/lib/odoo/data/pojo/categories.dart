class Categories {
  final String id;
  final String name;
  final String imgurl;

  Categories({this.id, this.name, this.imgurl});

  Categories.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        imgurl = json['imgurl']
      ;

  Map<String, dynamic> toJson() =>
    {
      'id': id,
      'name': name,
      'imgurl': imgurl
    };
}