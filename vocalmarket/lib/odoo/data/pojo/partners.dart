// import 'dart:convert';

// class Partner {
//   int id;
//   String name;
//   String imageUrl;
//   String email;
//   String phone;
//   String address;

//   Partner(
//       {this.id,
//       this.name,
//       this.imageUrl,
//       this.email,
//       this.phone,
//       this.address});

//   Partner.fromJson(Map<String, dynamic> json) {
//     name = json['name'];
//   }

// }

class PartnerAddressValues {
  final String id;
  final String street;
  bool selected = false;

  PartnerAddressValues({this.id, this.street});

  PartnerAddressValues.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        street = json['street'];

  Map<String, dynamic> toJson() => {
        'id': id,
        'street': street,
      };
}

class Partner {
  PartnerResult partnerresult;

  Partner({this.partnerresult});

  Partner.fromJson(Map<String, dynamic> json) {
    partnerresult = json['result'] != null
        ? new PartnerResult.fromJson(json['result'])
        : null;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    if (this.partnerresult != null) {
      data['result'] = this.partnerresult.toJson();
    }
    return data;
  }
}

class PartnerResult {
  int id;
  String appCurrencyName;
  // String name;
  // String imageUrl;
  // String email;
  // String phone;
  // String address;

  PartnerResult({this.id, this.appCurrencyName
      // this.name,
      // this.imageUrl,
      // this.email,
      // this.phone,
      // this.address
      });

  PartnerResult.fromJson(Map<String, dynamic> json) {
    id = json['id'];
    appCurrencyName =
        json['app_currency_id'] is! bool ? json['app_currency_id'] : "INR";
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['id'] = this.id;
    data['app_currency_id'] = this.appCurrencyName;

    return data;
  }
}
