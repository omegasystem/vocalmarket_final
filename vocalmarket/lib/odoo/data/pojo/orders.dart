class Orders {
  final String id;
  final String name;
  final String confirmationDate;
  final String amountTotal;

  Orders({this.id, this.name, this.confirmationDate, this.amountTotal});

  Orders.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        confirmationDate = json["confirmationDate"],
        amountTotal = json["amountTotal"];

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'confirmationDate': confirmationDate,
        'amountTotal': amountTotal
      };
}

class OrdersLine {
  final String id;
  final String name;
  final String priceTotal;
  final String priceUnit;
  final String productUomQty;
  final String orderId;

  OrdersLine(
      {this.id,
      this.name,
      this.priceTotal,
      this.priceUnit,
      this.productUomQty,
      this.orderId});

  OrdersLine.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        name = json['name'],
        priceTotal = json["priceTotal"],
        priceUnit = json["price_unit"],
        productUomQty = json["productUomQty"],
        orderId = json["order_id"];

  Map<String, dynamic> toJson() => {
        'id': id,
        'name': name,
        'priceTotal': priceTotal,
        'price_unit': priceUnit,
        'productUomQty': productUomQty,
        'order_id': orderId
      };
}
