import 'package:flutter/material.dart';
import 'package:carousel_slider/carousel_slider.dart';
import 'package:vocalmarket/odoo/data/pojo/banner.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';
import 'package:vocalmarket/widget/product/featured_products.dart';
import 'package:vocalmarket/widget/product/latest_products.dart';
// import 'package:vocalmarket/widget/custom_app_bar.dart';
// import 'package:vocalmarket/widget/product/currency.dart';
// import 'package:vocalmarket/widget/product/featured_products.dart';
import 'package:vocalmarket/widget/search.dart';
import 'package:vocalmarket/widget/product/productcategory.dart';
import 'package:vocalmarket/base.dart';
// import 'package:vocalmarket/odoo/utility/constant.dart';
// import 'package:vocalmarket/screens/login.dart';
import 'package:vocalmarket/screens/orderhistory.dart';

final List<String> imgList = [
  'https://images.unsplash.com/photo-1520342868574-5fa3804e551c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=6ff92caffcdd63681a35134a6770ed3b&auto=format&fit=crop&w=1951&q=80',
  'https://images.unsplash.com/photo-1522205408450-add114ad53fe?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=368f45b0888aeb0b7b08e3a1084d3ede&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1519125323398-675f0ddb6308?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=94a1e718d89ca60a6337a6008341ca50&auto=format&fit=crop&w=1950&q=80',
  'https://images.unsplash.com/photo-1523205771623-e0faa4d2813d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89719a0d55dd05e2deae4120227e6efc&auto=format&fit=crop&w=1953&q=80',
  'https://images.unsplash.com/photo-1508704019882-f9cf40e475b4?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=8c6e5e3aba713b17aa1fe71ab4f0ae5b&auto=format&fit=crop&w=1352&q=80',
  'https://images.unsplash.com/photo-1519985176271-adb1088fa94c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=a0c8d632e977f94e5d312d9893258f59&auto=format&fit=crop&w=1355&q=80'
];

final Map<String, dynamic> routeMap = {'1': OrderHistory()};

class HomePage extends StatefulWidget {
  @override
  _HomePageState createState() => _HomePageState();
}

class _HomePageState extends Base<HomePage> {
  int selectedPosition = 0;
  List<Widget> listBottomWidget = new List();
  final Map<String, dynamic> someMap = {};
  List<String> _appbanner = [];

  final List<Widget> imageSliders = imgList
      .map((item) => Container(
            child: Container(
              margin: EdgeInsets.all(5.0),
              child: ClipRRect(
                  borderRadius: BorderRadius.all(Radius.circular(5.0)),
                  child: Stack(
                    children: <Widget>[
                      Image.network(item, fit: BoxFit.cover, width: 1000.0),
                      Positioned(
                        bottom: 0.0,
                        left: 0.0,
                        right: 0.0,
                        child: Container(
                            // decoration: BoxDecoration(
                            //   gradient: LinearGradient(
                            // colors: [
                            //   Color.fromARGB(200, 0, 0, 0),
                            //   Color.fromARGB(0, 0, 0, 0)
                            // ],
                            //     begin: Alignment.bottomCenter,
                            //     end: Alignment.topCenter,
                            //   ),
                            // ),
                            // padding: EdgeInsets.symmetric(
                            //     vertical: 10.0, horizontal: 20.0),
                            ),
                      ),
                    ],
                  )),
            ),
          ))
      .toList();

  getAppBanner() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead(Strings.app_banner, [], ['id', 'image']).then(
            (OdooResponse res) {
          if (!res.hasError()) {
            String session = getSession();
            session = session.split(",")[0].split(";")[0];
            if (this.mounted) {
              setState(() {
                for (var i in res.getRecords()) {
                  print("i-------------------$i");
                  _appbanner.add(
                    getURL() +
                        "/web/image?model=app.banner&field=image&" +
                        session +
                        "&id=" +
                        i["id"].toString(),
                  );
                }
              });
            }
          }
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getAppBanner();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: new SafeArea(
            child: ListView(
      children: <Widget>[
        Container(height: 10.0),
        Search(),
        // Container(
        //     width: MediaQuery.of(context).size.width,
        //     child: Column(
        //       children: <Widget>[
        // CarouselSlider(
        //   options: CarouselOptions(
        //     autoPlay: true,
        //     aspectRatio: 2.0,
        //     enlargeCenterPage: true,
        //   ),
        //   items: imageSliders,
        // ),
        //   _appbanner.isNotEmpty
        //       ? CarouselSlider.builder(
        //           itemCount: _appbanner.length,
        //           itemBuilder: (BuildContext context, int itemIndex) =>
        //               Container(
        //             child: Image.network(_appbanner[itemIndex]),
        //           ),
        //           options: CarouselOptions(
        //             autoPlay: true,
        //             aspectRatio: 2.0,
        //             enlargeCenterPage: true,
        //           ),
        //         )
        //       : new Center(child: new CircularProgressIndicator())
        // ],
        // )),
        Container(
          width: MediaQuery.of(context).size.width,
          height: 20.0,
        ),
        ProductCategory(),
        FeaturedProducts(),
        LatestProducts()
      ],
    )));
  }
}
