import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/partners.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/screens/createpartneraddress.dart';
import 'package:vocalmarket/screens/paypal_view.dart';
import 'package:vocalmarket/widget/text/titletext.dart';

class MyAddress extends StatefulWidget {
  // final String partnerId;
  // final double totalAmount;
  // final String toCurrency;

  MyAddress();

  @override
  _MyAddressState createState() => _MyAddressState();
}

class _MyAddressState extends Base<MyAddress> {
  List<PartnerAddressValues> _partnerAddressValue = [];

  TextEditingController street = new TextEditingController();
  TextEditingController street2 = new TextEditingController();
  TextEditingController city = new TextEditingController();
  TextEditingController state = new TextEditingController();
  TextEditingController pincode = new TextEditingController();
  TextEditingController country = new TextEditingController();

  _getAddress() async {
    // print("widget.partnerId----------${widget.partnerId}");
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("res.partner", [
          ['parent_id', '=', user.result.partnerId],
          ['type', '=', 'delivery']
        ], [
          'id',
          'street'
        ]).then((OdooResponse res) {
          if (!res.hasError()) {
            if (this.mounted) {
              setState(() {
                for (var i in res.getRecords()) {
                  _partnerAddressValue.add(new PartnerAddressValues(
                      id: i["id"].toString(), street: i["street"]));
                }
              });
            }
          }
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      _getAddress();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  void changeArrayState(int index) {
    for (int i = 0; i < _partnerAddressValue.length; i++) {
      _partnerAddressValue[i].selected = false;
    }
    _partnerAddressValue[index].selected = true;
  }

  Widget _getListItemTile(BuildContext context, int index) {
    return ListTile(
      onTap: () {
        setState(() {
          changeArrayState(index);
        });
      },
      selected: _partnerAddressValue[index].selected,
      leading: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {},
        child: Container(
          width: 48,
          height: 48,
          padding: EdgeInsets.symmetric(vertical: 4.0),
          alignment: Alignment.center,
          // child: CircleAvatar(
          //     backgroundColor: _partnerAddressValue[index].colorpicture,
          // ),
        ),
      ),
      // title: Text('ID: ' + _partnerAddressValue[index].id.toString()),
      subtitle: Text(_partnerAddressValue[index].street),
      trailing: (_partnerAddressValue[index].selected)
          ? Icon(Icons.check_box)
          : Icon(Icons.check_box_outline_blank),
    );
  }

  // void updateAddList() {
  //   print("updateAddList-----------------------");
  //   getOdooInstance().then((odoo) {
  //     _getAddress();
  //   });
  //   if (this.mounted) {
  //     setState(() {
  //       _partnerAddressValue = List.from(_partnerAddressValue);
  //     });
  //   }
  // }

  createCartList() {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemBuilder: (context, position) {
        return _getListItemTile(context, position);
      },
      itemCount: _partnerAddressValue.length,
    );
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        appBar: new AppBar(
          backgroundColor: Colors.black,
          title: new TitleText(
            text: "My Addresses",
            fontSize: 18.0,
            color: Colors.white,
          ),
          actions: <Widget>[
            new IconButton(
                icon: const Icon(Icons.add),
                onPressed: () {
                  Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) =>
                              CreatePartnerAddress(user.result.partnerId)));
                })
          ],
        ),
        body: Builder(
          builder: (context) {
            return ListView(
              children: <Widget>[
                createCartList(),
              ],
            );
          },
        ));
  }
}
