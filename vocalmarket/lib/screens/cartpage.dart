import 'package:flutter/material.dart';
import 'package:vocalmarket/app_translations.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/data/pojo/orders.dart';
// import 'package:vocalmarket/odoo/utility/strings.dart';
// import 'package:vocalmarket/odoo/utility/strings.dart';
import 'package:vocalmarket/screens/partneraddress.dart';
import 'package:vocalmarket/widget/text/titletext.dart';
import 'package:flutter/services.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';

class CartPage extends StatefulWidget {
  @override
  _CartPageState createState() => _CartPageState();
}

class _CartPageState extends Base<CartPage> {
  List list = ['12', '11'];
  bool checkboxValueA = true;
  bool checkboxValueB = false;
  bool checkboxValueC = false;
  String toolbarname;
  List<OrdersLine> _ordersLine = [];
  List orderIds = [];
  // List<OrdersLine> _updateordersLine = [];
  String message = "";
  bool isLoading = false;
  // static int page = 0;
  var partnerId = "";
  int countOrder = 0;
  double totalAmount = 0.0;
  int updatedCount = 0;
  double updatedtotalAmount = 0.0;
  double oldTotal = 0.0;
  String toCurrency = "";
  String street = "";
  static const platform = const MethodChannel("razorpay_flutter");
  Razorpay _razorpay;

  getOrderData() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        // EasyLoading.showSuccess('Use in initState');
        odoo.searchRead("sale.order.line", [
          ['state', '=', 'draft'],
          ['product_id', '!=', false]
        ], [
          'id',
          'product_id',
          'price_total',
          'price_unit',
          'product_uom_qty',
          'order_id'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    countOrder++;
                    totalAmount += i["price_total"];
                    _ordersLine.add(new OrdersLine(
                        orderId: i['order_id'][0].toString(),
                        id: i["id"].toString(),
                        name: i["product_id"][1],
                        priceUnit: i["price_unit"].toString(),
                        productUomQty: i["product_uom_qty"].toString(),
                        priceTotal: i["price_total"].toString()));
                    orderIds.add(i['order_id'][0].toString());
                  }
                  // hideLoading();
                });
              }
            }
          },
        );
      }
    });
  }

  getAddress() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("res.partner", [
          ['parent_id', '=', user.result.partnerId],
          ['type', '=', 'delivery']
        ], [
          'id',
          'street'
        ]).then((OdooResponse res) {
          if (!res.hasError()) {
            if (this.mounted) {
              setState(() {
                for (var i in res.getRecords()) {
                  street = i["street"];
                  // _partnerAddressValue.add(new PartnerAddressValues(
                  //     id: i["id"].toString(), street: i["street"]));
                }
              });
            }
          }
        });
      }
    });
  }

  paymentProcess(uniqueorderIds) async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.callKW("sale.order", 'test', ['1'],
            context: {'context': '1'}).then((OdooResponse res) {
          if (!res.hasError()) {
            if (this.mounted) {
              setState(() {
                print(
                    "res.getRecords()------===================================");
                print(res.getRecords());
                // for (var i in res.getRecords()) {
                // }
              });
            }
          }
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      // getUserData();
      getOrderData();
      getAddress();
      toCurrency = partner.partnerresult.appCurrencyName;
    });
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override
  void dispose() {
    super.dispose();
  }

  void openCheckout() async {
    var options = {
      'key': 'rzp_test_dXFv4RoB1PQoEG',
      'amount': 4700,
      'name': 'VocalMarket.',
      'description': 'Shopping',
      'prefill': {'contact': '8888888888', 'email': 'test@razorpay.com'},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint(e);
    }
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    print("orderIds----------------------$orderIds-------");
    // var uniqueorderIds = orderIds.toSet().toList();
    // getOdooInstance().then((odoo) {
    //   paymentProcess(uniqueorderIds);
    // });
    Fluttertoast.showToast(
        msg: "SUCCESS: " + response.paymentId, timeInSecForIos: 4);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message,
        timeInSecForIos: 4);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName, timeInSecForIos: 4);
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // resizeToAvoidBottomPadding: false,
      resizeToAvoidBottomInset: false,
      backgroundColor: Colors.grey.shade100,
      body: _ordersLine.isNotEmpty
          ? Builder(
              builder: (context) {
                return ListView(
                  children: <Widget>[
                    // createHeader(),
                    displayAddress(),
                    // createSubTitle(),
                    createCartList(),
                    // footer(context)
                  ],
                );
              },
            )
          : Container(
              alignment: AlignmentDirectional.center,
              child: TitleText(
                text: "Your cart is empty!",
                fontSize: 16.0,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
      bottomNavigationBar: new Container(
        height: 40.0,
        color: Colors.red,
        child: RaisedButton(
          onPressed: () {
            openCheckout();
            // getOdooInstance().then((odoo) {
            //   var uniqueorderIds = orderIds.toSet().toList();
            //   paymentProcess(uniqueorderIds);
            // });
            // Navigator.push(
            //     context,
            //     new MaterialPageRoute(
            //         builder: (context) =>
            //             PartnerAddress(partnerId, totalAmount, toCurrency)));
          },
          color: Colors.black,
          padding: EdgeInsets.only(top: 12, left: 60, right: 60, bottom: 12),
          child: Text(
            AppTranslations.of(context).text("check_out"),
            style: TextStyle(color: Colors.white),
          ),
        ),
        //   child: Column(
        //     crossAxisAlignment: CrossAxisAlignment.center,
        //     mainAxisAlignment: MainAxisAlignment.end,
        //     children: <Widget>[
        //       Row(
        //         mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //         children: <Widget>[
        //           Container(
        //             margin: EdgeInsets.only(left: 30),
        //             child: Text(
        //               AppTranslations.of(context).text("total"),
        //             ),
        //           ),
        //           Container(
        //             margin: EdgeInsets.only(right: 30),
        //             child: Text(
        //               "$totalAmount",
        //             ),
        //           ),
        //         ],
        //       ),
        //       RaisedButton(
        //         onPressed: () {
        //           Navigator.push(
        //               context,
        //               new MaterialPageRoute(
        //                   builder: (context) => PartnerAddress(
        //                       partnerId, totalAmount, toCurrency)));
        //         },
        //         color: Colors.black,
        //         padding:
        //             EdgeInsets.only(top: 12, left: 60, right: 60, bottom: 12),
        //         child: Text(
        //           AppTranslations.of(context).text("check_out"),
        //           style: TextStyle(color: Colors.white),
        //         ),
        //       ),
        //     ],
        //   ),
        //   margin: EdgeInsets.only(top: 16),
      ),
    );
  }

  footer(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.end,
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(left: 30),
                child: Text(
                  AppTranslations.of(context).text("total"),
                ),
              ),
              Container(
                margin: EdgeInsets.only(right: 30),
                child: Text(
                  "$totalAmount",
                ),
              ),
            ],
          ),
          RaisedButton(
            onPressed: () {
              // Navigator.push(
              //     context,
              //     new MaterialPageRoute(
              //         builder: (context) =>
              //             PartnerAddress(partnerId, totalAmount, toCurrency)));
            },
            color: Colors.black,
            padding: EdgeInsets.only(top: 12, left: 60, right: 60, bottom: 12),
            child: Text(
              AppTranslations.of(context).text("check_out"),
              style: TextStyle(color: Colors.white),
            ),
          ),
        ],
      ),
      margin: EdgeInsets.only(top: 16),
    );
  }

  createHeader() {
    return Container(
      alignment: Alignment.topLeft,
      child: Text(AppTranslations.of(context).text("shopping_cart")),
      margin: EdgeInsets.only(left: 12, top: 12),
    );
  }

  displayAddress() {
    return Container(
        margin: EdgeInsets.only(left: 16, right: 16, top: 16),
        decoration: BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.all(Radius.circular(16))),
        child: Row(children: <Widget>[
          Container(
            margin: EdgeInsets.only(right: 8, left: 8, top: 8, bottom: 8),
            child: Text(
              "Deliver to " + street,
              maxLines: 2,
              softWrap: true,
            ),
          ),
          Expanded(
              child: Container(
            padding: const EdgeInsets.all(8.0),
            alignment: Alignment.centerRight,
            margin: EdgeInsets.only(right: 18, left: 8, top: 8, bottom: 8),
            child: RaisedButton(
              onPressed: () {
                // Navigator.push(
                //     context,
                //     new MaterialPageRoute(
                //         builder: (context) => PartnerAddress(
                //             partnerId, totalAmount, toCurrency)));
              },
              color: Colors.black,
              child: Text(
                "Change",
                style: TextStyle(color: Colors.white),
              ),
            ),
          ))
        ]));
  }

  createSubTitle() {
    return Container(
      alignment: Alignment.topLeft,
      child: Text(
        "Total($countOrder) Items",
      ),
      margin: EdgeInsets.only(left: 12, top: 4),
    );
  }

  createCartList() {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemBuilder: (context, position) {
        return CreateListItems(_ordersLine, position,
            onDelete: () => removeItem(position),
            onUpdate: (counter) => updateItem(position, counter));
      },
      itemCount: _ordersLine.length,
    );
  }

  void removeItem(int position) {
    print("totalAmount---------------$totalAmount");
    if (this.mounted) {
      setState(() {
        _ordersLine = List.from(_ordersLine)..removeAt(position);
        // for(var order in _ordersLine){
        //   updatedCount++;
        //   updatedtotalAmount += double.parse(order.priceTotal);
        // }
        // print("updatedtotalAmount--------------$updatedtotalAmount");
        // countOrder = updatedCount;
        // totalAmount = updatedtotalAmount;
      });
      odoo.searchRead("sale.order.line", [
        ['state', '=', 'draft'],
        ['product_id', '!=', false]
      ], [
        'id',
        'price_total',
      ]).then(
        (OdooResponse res) {
          if (!res.hasError()) {
            if (this.mounted) {
              setState(() {
                updatedtotalAmount = 0.0;
                for (var i in res.getRecords()) {
                  updatedtotalAmount += i["price_total"];
                  updatedCount++;
                }
                totalAmount = updatedtotalAmount;
                countOrder = updatedCount;
                // hideLoading();
              });
            }
          }
        },
      );
    }
  }

  void updateItem(int position, int counter) {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo.searchRead("sale.order.line", [
          ['state', '=', 'draft'],
          ['product_id', '!=', false]
        ], [
          'id',
          'price_total',
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  updatedtotalAmount = 0.0;
                  for (var i in res.getRecords()) {
                    updatedtotalAmount += i["price_total"];
                  }
                  totalAmount = updatedtotalAmount;
                  // hideLoading();
                });
              }
            }
          },
        );
      }
    });
  }
}

class CreateListItems extends StatefulWidget {
  final int position;
  final List _ordersLine;
  final VoidCallback onDelete;
  final void Function(int counter) onUpdate;
  CreateListItems(this._ordersLine, this.position,
      {this.onDelete, this.onUpdate});

  @override
  _CreateListItemsState createState() => _CreateListItemsState();
}

class _CreateListItemsState extends Base<CreateListItems> {
  int counter = 1;

  _updateOrderData(int updatecounter) async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo.write(
            "sale.order.line",
            [int.parse(widget._ordersLine[widget.position].id)],
            {"product_uom_qty": updatecounter}).then((OdooResponse res) {
          if (!res.hasError()) {
            // hideLoading();
            widget.onUpdate(updatecounter);
            setState(() {
              counter = updatecounter;
            });
          }
        });
      }
    });
  }

  _deleteOrderData() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo.unlink("sale.order.line", [
          int.parse(widget._ordersLine[widget.position].id)
        ]).then((OdooResponse res) {
          if (!res.hasError()) {
            // hideLoading();
            widget.onDelete();
          }
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    counter = (double.parse(widget._ordersLine[widget.position].productUomQty))
        .toInt();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(left: 16, right: 16, top: 16),
          decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.all(Radius.circular(16))),
          child: Row(
            children: <Widget>[
              Container(
                margin: EdgeInsets.only(right: 8, left: 8, top: 8, bottom: 8),
                width: 80,
                height: 80,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.all(Radius.circular(14)),
                    color: Colors.blue.shade200,
                    image:
                        DecorationImage(image: AssetImage("images/logo.png"))),
              ),
              Expanded(
                child: Container(
                  padding: const EdgeInsets.all(8.0),
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        padding: EdgeInsets.only(right: 8, top: 4),
                        child: Text(
                          widget._ordersLine[widget.position].name,
                          maxLines: 2,
                          softWrap: true,
                        ),
                      ),
                      Container(
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                            Text(
                              widget._ordersLine[widget.position].priceUnit
                                  .toString(),
                            ),
                            // Text(widget._ordersLine[widget.position].orderId
                            //     .toString()),
                            // Text(
                            //   widget._ordersLine[widget.position].priceTotal
                            //       .toString(),
                            // ),
                            Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                  children: <Widget>[
                                    counter != 0
                                        ? GestureDetector(
                                            child: Container(
                                              padding:
                                                  const EdgeInsets.all(5.0),
                                              decoration: BoxDecoration(
                                                shape: BoxShape.circle,
                                                color: Colors.black,
                                              ),
                                              child: Icon(
                                                Icons.remove,
                                                color: Colors.white,
                                              ),
                                            ),
                                            onTap: () {
                                              setState(() {
                                                counter--;
                                              });
                                              getOdooInstance().then((odoo) {
                                                _updateOrderData(counter);
                                              });
                                            },
                                          )
                                        : Container(),
                                    SizedBox(width: 15),
                                    Text(
                                      "$counter",
                                      style: Theme.of(context).textTheme.title,
                                    ),
                                    SizedBox(width: 15),
                                    GestureDetector(
                                      child: Container(
                                        padding: const EdgeInsets.all(5.0),
                                        decoration: BoxDecoration(
                                          shape: BoxShape.circle,
                                          color: Colors.black,
                                        ),
                                        child: Icon(
                                          Icons.add,
                                          color: Colors.white,
                                        ),
                                      ),
                                      onTap: () {
                                        setState(() {
                                          counter++;
                                        });
                                        getOdooInstance().then((odoo) {
                                          _updateOrderData(counter);
                                        });
                                      },
                                    ),
                                  ],
                                ))
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                flex: 100,
              )
            ],
          ),
        ),
        Align(
          alignment: Alignment.topRight,
          child: Container(
            width: 24,
            height: 24,
            alignment: Alignment.center,
            margin: EdgeInsets.only(right: 10, top: 10),
            child: IconButton(
              padding: EdgeInsets.only(right: 0, top: 0),
              icon: Icon(Icons.close),
              color: Colors.white,
              iconSize: 20,
              onPressed: () {
                getOdooInstance().then((odoo) {
                  _deleteOrderData();
                });
              },
            ),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.all(Radius.circular(4)),
                color: Colors.black),
          ),
        )
      ],
    );
  }
}
