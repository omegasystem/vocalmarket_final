import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/data/pojo/orders.dart';
import 'package:vocalmarket/screens/orderdetails.dart';

class OrderHistory extends StatefulWidget {
  @override
  _OrderHistoryState createState() => _OrderHistoryState();
}

class _OrderHistoryState extends Base<OrderHistory> {
  List list = ['12', '11'];
  bool checkboxValueA = true;
  bool checkboxValueB = false;
  bool checkboxValueC = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  String toolbarname;
  ScrollController _controller;
  List<Orders> _orders = [];
  String message = "";
  bool isLoading = false;
  static int page = 0;

  _getOrderData() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo.searchRead("sale.order", [
          ["state", "=", "sale"]
        ], [
          'id',
          'name',
          'confirmation_date',
          'amount_total'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    _orders.add(
                      new Orders(
                          id: i["id"].toString(),
                          name: i["name"],
                          confirmationDate: i["confirmation_date"],
                          amountTotal: i["amount_total"].toString()),
                    );
                  }
                  // hideLoading();
                });
              }
            }
          },
        );
      }
    });
  }

  _scrollListener() {
    if (_controller.offset >= _controller.position.maxScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        message = "reach the bottom";
      });
    }
    if (_controller.offset <= _controller.position.minScrollExtent &&
        !_controller.position.outOfRange) {
      setState(() {
        message = "reach the top";
      });
    }
  }

  @override
  void initState() {
    super.initState();

    getOdooInstance().then((odoo) {
      _getOrderData();
    });
    _controller = new ScrollController()..addListener(_scrollListener);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    // IconData _backIcon() {
    //   switch (Theme.of(context).platform) {
    //     case TargetPlatform.android:
    //     case TargetPlatform.fuchsia:
    //       return Icons.arrow_back;
    //     case TargetPlatform.iOS:
    //       return Icons.arrow_back_ios;
    //   }
    //   assert(false);
    //   return null;
    // }

    return new Scaffold(
      key: _scaffoldKey,
      body: ListView.builder(
          controller: _controller,
          itemCount: _orders.length,
          itemBuilder: (BuildContext cont, int ind) {
            return _orders.length == 0
                ? Center(
                    child: Container(
                    child: new Text(
                      "No data",
                      style: TextStyle(color: Colors.black),
                    ),
                  ))
                : SafeArea(
                    child: Column(children: <Widget>[
                    Container(
                        margin:
                            EdgeInsets.only(left: 5.0, right: 5.0, bottom: 5.0),
                        color: Colors.black12,
                        child: InkWell(
                          // onTap: () => Navigator.push(
                          //   context,
                          //   MaterialPageRoute(
                          //     builder: (context) =>
                          //         OrderDetails(orderId: _orders[ind].id),
                          //   ),
                          // ),
                          onTap: () {
                            print("cal-----------------");
                          },
                          child: Card(
                              elevation: 4.0,
                              child: Container(
                                  padding: const EdgeInsets.fromLTRB(
                                      10.0, 10.0, 10.0, 10.0),
                                  child: GestureDetector(
                                      child: Column(
                                    mainAxisSize: MainAxisSize.max,
                                    mainAxisAlignment: MainAxisAlignment.start,
                                    children: <Widget>[
                                      // three line description
                                      Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          _orders[ind].name,
                                          style: TextStyle(
                                            fontSize: 16.0,
                                            fontStyle: FontStyle.normal,
                                            color: Colors.black87,
                                          ),
                                        ),
                                      ),

                                      Container(
                                        margin: EdgeInsets.only(top: 3.0),
                                      ),
                                      Container(
                                        alignment: Alignment.topLeft,
                                        child: Text(
                                          'To Deliver On :' +
                                              _orders[ind].confirmationDate,
                                          style: TextStyle(
                                              fontSize: 13.0,
                                              color: Colors.black54),
                                        ),
                                      ),
                                      Divider(
                                        height: 10.0,
                                        color: Colors.blue,
                                      ),

                                      Row(
                                        mainAxisSize: MainAxisSize.max,
                                        mainAxisAlignment:
                                            MainAxisAlignment.spaceBetween,
                                        children: <Widget>[
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Order Id',
                                                    style: TextStyle(
                                                        fontSize: 13.0,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 3.0),
                                                    child: Text(
                                                      _orders[ind].name,
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  )
                                                ],
                                              )),
                                          Container(
                                              padding: EdgeInsets.all(3.0),
                                              child: Column(
                                                mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                children: <Widget>[
                                                  Text(
                                                    'Order Amount',
                                                    style: TextStyle(
                                                        fontSize: 13.0,
                                                        color: Colors.black54),
                                                  ),
                                                  Container(
                                                    margin: EdgeInsets.only(
                                                        top: 3.0),
                                                    child: Text(
                                                      _orders[ind]
                                                          .amountTotal
                                                          .toString(),
                                                      style: TextStyle(
                                                          fontSize: 15.0,
                                                          color:
                                                              Colors.black87),
                                                    ),
                                                  ),
                                                ],
                                              )),
                                          // Container(
                                          //     padding: EdgeInsets.all(3.0),
                                          //     child: Column(
                                          //       mainAxisAlignment:
                                          //           MainAxisAlignment.center,
                                          //       children: <Widget>[
                                          //         Text(
                                          //           'Payment Type',
                                          //           style: TextStyle(
                                          //               fontSize: 13.0,
                                          //               color: Colors.black54),
                                          //         ),
                                          //         Container(
                                          //           margin: EdgeInsets.only(top: 3.0),
                                          //           child: Text(
                                          //             itemList[ind].paymentType,
                                          //             style: TextStyle(
                                          //                 fontSize: 15.0,
                                          //                 color: Colors.black87),
                                          //           ),
                                          //         )
                                          //       ],
                                          //     )),
                                        ],
                                      ),
                                      Divider(
                                        height: 10.0,
                                        color: Colors.black,
                                      ),

                                      // Row(
                                      //   mainAxisAlignment: MainAxisAlignment.start,
                                      //   children: <Widget>[
                                      //     Icon(
                                      //       Icons.location_on,
                                      //       size: 20.0,
                                      //         color: Colors.amber.shade500,
                                      //     ),
                                      //     Text(itemList[ind].address,
                                      //         style: TextStyle(
                                      //             fontSize: 13.0,
                                      //             color: Colors.black54)),
                                      //   ],
                                      // ),
                                      // Divider(
                                      //   height: 10.0,
                                      //   color: Colors.amber.shade500,
                                      // ),
                                      // Container(
                                      //  child:_status(itemList[ind].cancelOder)
                                      // )
                                    ],
                                  )))),
                        )),
                  ]));
          }),
      // resizeToAvoidBottomPadding: false,
    );
  }

  // Widget _buildProgressIndicator() {
  //   return new Padding(
  //     padding: const EdgeInsets.all(8.0),
  //     child: new Center(
  //       child: new Opacity(
  //         opacity: isLoading ? 1.0 : 00,
  //         child: new CircularProgressIndicator(),
  //       ),
  //     ),
  //   );
  // }
}
