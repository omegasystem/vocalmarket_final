import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/country.dart';
import 'package:vocalmarket/odoo/data/pojo/state.dart';
import "package:vocalmarket/odoo/data/services/odoo_response.dart";

class CreatePartnerAddress extends StatefulWidget {
  final int partnerId;

  CreatePartnerAddress(this.partnerId);

  @override
  _CreatePartnerAddressState createState() => _CreatePartnerAddressState();
}

class _CreatePartnerAddressState extends Base<CreatePartnerAddress> {
  TextEditingController street = new TextEditingController();
  TextEditingController street2 = new TextEditingController();
  TextEditingController city = new TextEditingController();
  TextEditingController state = new TextEditingController();
  TextEditingController pincode = new TextEditingController();
  TextEditingController country = new TextEditingController();
  bool shippingBool = true;
  bool invoiceBool = false;
  String type = "delivery";
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<OdooCountry> _country = [];
  List<OdooState> _state = [];
  OdooCountry _selectedCountry;
  OdooState _selectedState;

  _createAddress() async {
    if (_selectedCountry == null || _selectedState == null) {
      _scaffoldKey.currentState.showSnackBar(
          new SnackBar(content: Text("Please select country and state!")));
    } else {
      isConnected().then((isInternet) {
        if (isInternet) {
          odoo.create("res.partner", {
            'type': type,
            'parent_id': widget.partnerId,
            'street': street.text,
            'street2': street2.text,
            'city': city.text,
            'state_id': _selectedState.id,
            'zip': pincode.text,
            'country_id': _selectedCountry.id
          }).then((OdooResponse res) {
            if (!res.hasError()) {
              _scaffoldKey.currentState
                  .showSnackBar(new SnackBar(content: Text("Address added!")));
            }
          });
        }
      });
    }
  }

  _getCountry() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead('res.country', [], ['id', 'name']).then(
            (OdooResponse res) {
          if (this.mounted) {
            setState(() {
              for (var i in res.getRecords()) {
                _country.add(
                    new OdooCountry(id: i["id"].toString(), name: i["name"]));
              }
            });
          }
        });
      }
    });
  }

  _getState() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead('res.country.state', [], ['id', 'name']).then(
            (OdooResponse res) {
          if (this.mounted) {
            setState(() {
              for (var i in res.getRecords()) {
                _state.add(
                    new OdooState(id: i["id"].toString(), name: i["name"]));
              }
            });
          }
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      _getCountry();
      _getState();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  void _setshippingBool(bool newValue) {
    if (newValue == true) {
      setState(() {
        type = "delivery";
        shippingBool = true;
        invoiceBool = false;
      });
    } else {
      shippingBool = false;
    }
  }

  void _setinvoiceBool(bool newValue) {
    if (newValue == true) {
      setState(() {
        type = "invoice";
        invoiceBool = true;
        shippingBool = false;
      });
    } else {
      invoiceBool = false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return new Scaffold(
        key: _scaffoldKey,
        appBar: new AppBar(
          backgroundColor: Colors.black,
          title: new Text("New Address"),
          actions: <Widget>[
            new IconButton(
                icon: const Icon(Icons.save),
                onPressed: () {
                  getOdooInstance().then((odoo) {
                    _createAddress();
                  });
                  // Navigator.pop(context);
                })
          ],
        ),
        body: new SingleChildScrollView(
          child: new Column(
            children: <Widget>[
              new ListTile(
                title: new TextFormField(
                  controller: street,
                  decoration: new InputDecoration(
                    hintText: "Street",
                  ),
                ),
              ),
              new ListTile(
                title: new TextFormField(
                  controller: street2,
                  decoration: new InputDecoration(
                    hintText: "Street2",
                  ),
                ),
              ),
              new ListTile(
                title: new TextFormField(
                  controller: city,
                  decoration: new InputDecoration(
                    hintText: "City",
                  ),
                ),
              ),
              const Divider(
                height: 1.0,
              ),
              Container(
                padding: EdgeInsets.only(left: 20.0),
                alignment: Alignment.topLeft,
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<OdooState>(
                    items: _state.map((data) {
                      return DropdownMenuItem<OdooState>(
                        value: data,
                        child: Text(
                          data.name,
                          style: TextStyle(fontSize: 13),
                        ),
                      );
                    }).toList(),
                    hint: Text(
                      "State",
                      style: TextStyle(color: Colors.black, fontSize: 13),
                    ), // setting hint
                    value: _selectedState,
                    onChanged: (OdooState data) {
                      setState(() {
                        _selectedState = data;
                      });
                    },
                  ),
                ),
              ),
              const Divider(
                height: 1.0,
              ),
              Container(
                padding: EdgeInsets.only(left: 20.0),
                alignment: Alignment.topLeft,
                child: DropdownButtonHideUnderline(
                  child: DropdownButton<OdooCountry>(
                    iconEnabledColor:
                        Color(0xFF595959), // icon color of the dropdown button
                    items: _country.map((data) {
                      return DropdownMenuItem<OdooCountry>(
                        value: data,
                        child: Text(
                          data.name,
                          style: TextStyle(fontSize: 13),
                        ),
                      );
                    }).toList(),
                    hint: Text(
                      "Country",
                      style: TextStyle(color: Colors.black, fontSize: 13),
                    ), // setting hint
                    value: _selectedCountry,
                    onChanged: (OdooCountry data) {
                      setState(() {
                        _selectedCountry = data;
                      });
                    },
                  ),
                ),
              ),
              const Divider(
                height: 1.0,
              ),
              new ListTile(
                title: new TextFormField(
                  controller: pincode,
                  decoration: new InputDecoration(
                    hintText: "Pincode",
                  ),
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 5.0),
                child: Row(
                  children: <Widget>[
                    Checkbox(
                      value: shippingBool,
                      onChanged: _setshippingBool,
                    ),
                    GestureDetector(
                      onTap: () => _setshippingBool(!shippingBool),
                      child: const Text(
                        'Shipping Address',
                      ),
                    ),
                  ],
                ),
              ),
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 1.0),
                child: Row(
                  children: <Widget>[
                    Checkbox(
                      value: invoiceBool,
                      onChanged: _setinvoiceBool,
                    ),
                    GestureDetector(
                      onTap: () => _setinvoiceBool(!invoiceBool),
                      child: const Text(
                        'Invoice Address',
                      ),
                    ),
                  ],
                ),
              ),
            ],
          ),
        ));
  }
}
