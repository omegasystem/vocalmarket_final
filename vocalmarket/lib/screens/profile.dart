// import 'dart:convert';
// import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';
// import 'package:vocalmarket/app/data/services/odoo_api.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
// import 'package:vocalmarket/app/data/services/utils.dart';
// import 'package:vocalmarket/odoo/utility/constant.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';
import 'package:vocalmarket/base.dart';
// import 'package:vocalmarket/widget/product/currency.dart';
// import 'package:shared_preferences/shared_preferences.dart';
// import 'package:vocalmarket/app/data/services/globals.dart';

// import 'login.dart';

class ProfilePage extends StatefulWidget {
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends Base<ProfilePage>
    with SingleTickerProviderStateMixin {
  String name = "";
  String image_URL = "";
  String email = "";
  TextEditingController mobileControl = new TextEditingController();
  var phone = "";
  var mobile = "";
  var street = "";
  var street2 = "";
  var city = "";
  var state_id = "";
  var zip = "";
  var title = "";
  var website = "";
  var jobposition = "";
  // TextEditingController _nameCtrler = new TextEditingController();

  bool _status = true;
  final FocusNode myFocusNode = FocusNode();

  _getUserData() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo.searchRead(Strings.res_users, [
          ["id", "=", getUID()]
        ], [
          'name',
          'image',
          'login'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  String session = getSession();
                  session = session.split(",")[0].split(";")[0];
                  final result = res.getResult()['records'][0];
                  name = result['name'] is! bool ? result['name'] : "";
                  image_URL = getURL() +
                      "/web/image?model=res.users&field=image&" +
                      session +
                      "&id=" +
                      getUID().toString();
                  email = result['login'] is! bool ? result['login'] : "";
                });
              }
            } else {
              // showMessage("Warning", res.getErrorMessage());
              // EasyLoading.showSuccess(res.getErrorMessage());
            }
            // hideLoading();
          },
        );
      }
    });
  }

  _getProfileData() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("res.partner", [
          ["id", "=", user.result.partnerId]
        ], [
          'phone',
          'mobile',
          'street',
          'street2',
          'city',
          'state_id',
          'zip',
          'title',
          'website',
          'function'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  final result = res.getResult()['records'][0];
                  print("result-------------$result");
                  phone = result['phone'] is! bool ? result['phone'] : "N/A";
                  // mobile = result['mobile'] is! bool ? result['mobile'] : "N/A";
                  mobileControl = TextEditingController(text: phone);
                  street = result['street'] is! bool ? result['street'] : "";
                  street2 = result['street2'] is! bool ? result['street2'] : "";
                  city = result['city'] is! bool ? result['city'] : "";
                  state_id =
                      result['state_id'] is! bool ? result['state_id'][1] : "";
                  zip = result['zip'] is! bool ? result['zip'] : "";
                  title = result['title'] is! bool ? result['title'][1] : "N/A";
                  website =
                      result['website'] is! bool ? result['website'] : "N/A";
                  jobposition =
                      result['function'] is! bool ? result['function'] : "N/A";
                });
              }
            }
          },
        );
      }
    });
  }

  _saveprofile() {
    print("mobile-----------${mobileControl.text}");
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.write(Strings.res_partner, [user.result.partnerId],
            {'mobile': mobileControl.text});
      }
    });
  }

  @override
  void initState() {
    super.initState();

    getOdooInstance().then((odoo) {
      _getUserData();
      _getProfileData();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        title: Text("Profile"),
        backgroundColor: Colors.black,
        actions: <Widget>[],
      ),
      body:
          // <Widget>[upper_header, Expanded(child: lower)],
          new Container(
        color: Colors.white,
        child: new ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                new Container(
                  height: 250.0,
                  color: Colors.white,
                  child: new Column(
                    children: <Widget>[
                      // Padding(
                      //     padding: EdgeInsets.only(left: 20.0, top: 20.0),
                      //     child: new Row(
                      //       crossAxisAlignment: CrossAxisAlignment.start,
                      //       children: <Widget>[
                      //         new Icon(
                      //           Icons.arrow_back_ios,
                      //           color: Colors.black,
                      //           size: 22.0,
                      //         ),
                      //         Padding(
                      //           padding: EdgeInsets.only(left: 25.0),
                      //           child: new Text('PROFILE',
                      //               style: TextStyle(
                      //                   fontWeight: FontWeight.bold,
                      //                   fontSize: 20.0,
                      //                   fontFamily: 'sans-serif-light',
                      //                   color: Colors.black)),
                      //         )
                      //       ],
                      //     )),
                      Padding(
                        padding: EdgeInsets.only(top: 20.0),
                        child:
                            new Stack(fit: StackFit.loose, children: <Widget>[
                          new Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              new Container(
                                  width: 140.0,
                                  height: 140.0,
                                  decoration: new BoxDecoration(
                                    shape: BoxShape.circle,
                                    image: new DecorationImage(
                                      image: new ExactAssetImage(
                                          'assets/logo.png'),
                                      fit: BoxFit.cover,
                                    ),
                                  )),
                            ],
                          ),
                          Padding(
                              padding: EdgeInsets.only(top: 90.0, right: 100.0),
                              child: new Row(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  new CircleAvatar(
                                    backgroundColor: Colors.red,
                                    radius: 25.0,
                                    child: new Icon(
                                      Icons.camera_alt,
                                      color: Colors.white,
                                    ),
                                  )
                                ],
                              )),
                        ]),
                      )
                    ],
                  ),
                ),
                new Container(
                  color: Color(0xffFFFFFF),
                  child: Padding(
                    padding: EdgeInsets.only(bottom: 25.0),
                    child: new Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 25.0),
                            child: new Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Text(
                                      'Parsonal Information',
                                      style: TextStyle(
                                          fontSize: 18.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.end,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    _status ? _getEditIcon() : new Container(),
                                  ],
                                )
                              ],
                            )),
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 25.0),
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Text(
                                      'Name',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 2.0),
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Flexible(
                                  child: new TextField(
                                    controller:
                                        TextEditingController(text: name),
                                    decoration: const InputDecoration(
                                      hintText: "Enter Your Name",
                                    ),
                                    enabled: !_status,
                                    autofocus: !_status,
                                  ),
                                ),
                              ],
                            )),
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 25.0),
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Text(
                                      'Email ID',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 2.0),
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Flexible(
                                  child: new TextField(
                                    controller:
                                        TextEditingController(text: email),
                                    decoration: const InputDecoration(
                                        hintText: "Enter Email ID"),
                                    enabled: !_status,
                                  ),
                                ),
                              ],
                            )),
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 25.0),
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Column(
                                  mainAxisAlignment: MainAxisAlignment.start,
                                  mainAxisSize: MainAxisSize.min,
                                  children: <Widget>[
                                    new Text(
                                      'Mobile',
                                      style: TextStyle(
                                          fontSize: 16.0,
                                          fontWeight: FontWeight.bold),
                                    ),
                                  ],
                                ),
                              ],
                            )),
                        Padding(
                            padding: EdgeInsets.only(
                                left: 25.0, right: 25.0, top: 2.0),
                            child: new Row(
                              mainAxisSize: MainAxisSize.max,
                              children: <Widget>[
                                new Flexible(
                                  child: new TextField(
                                    controller: mobileControl,
                                    decoration: const InputDecoration(
                                        hintText: "Enter Mobile Number"),
                                    enabled: !_status,
                                  ),
                                ),
                              ],
                            )),
                        // Padding(
                        //     padding: EdgeInsets.only(
                        //         left: 25.0, right: 25.0, top: 25.0),
                        //     child: new Row(
                        //       mainAxisSize: MainAxisSize.max,
                        //       mainAxisAlignment: MainAxisAlignment.start,
                        //       children: <Widget>[
                        //         Expanded(
                        //           child: Container(
                        //             child: new Text(
                        //               'Pin Code',
                        //               style: TextStyle(
                        //                   fontSize: 16.0,
                        //                   fontWeight: FontWeight.bold),
                        //             ),
                        //           ),
                        //           flex: 2,
                        //         ),
                        //         Expanded(
                        //           child: Container(
                        //             child: new Text(
                        //               'State',
                        //               style: TextStyle(
                        //                   fontSize: 16.0,
                        //                   fontWeight: FontWeight.bold),
                        //             ),
                        //           ),
                        //           flex: 2,
                        //         ),
                        //       ],
                        //     )),
                        // Padding(
                        //     padding: EdgeInsets.only(
                        //         left: 25.0, right: 25.0, top: 2.0),
                        //     child: new Row(
                        //       mainAxisSize: MainAxisSize.max,
                        //       mainAxisAlignment: MainAxisAlignment.start,
                        //       children: <Widget>[
                        //         Flexible(
                        //           child: Padding(
                        //             padding: EdgeInsets.only(right: 10.0),
                        //             child: new TextField(
                        //               controller:
                        //                   TextEditingController(text: zip),
                        //               decoration: const InputDecoration(
                        //                   hintText: "Enter Pin Code"),
                        //               enabled: !_status,
                        //             ),
                        //           ),
                        //           flex: 2,
                        //         ),
                        //         Flexible(
                        //           child: new TextField(
                        //             controller:
                        //                 TextEditingController(text: state_id),
                        //             decoration: const InputDecoration(
                        //                 hintText: "Enter State"),
                        //             enabled: !_status,
                        //           ),
                        //           flex: 2,
                        //         ),
                        //       ],
                        //     )),
                        // Padding(
                        //   padding: EdgeInsets.only(
                        //     left: 25.0, right: 25.0, top: 2.0),
                        //     child: new Row(
                        //       children: <Widget>[
                        //         ProductCurrency()
                        //       ],
                        //     ),
                        // ),
                        !_status ? _getActionButtons() : new Container(),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ],
        ),
      ),
    );
  }

  Widget _getActionButtons() {
    return Padding(
      padding: EdgeInsets.only(left: 25.0, right: 25.0, top: 45.0),
      child: new Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(right: 10.0),
              child: Container(
                  child: new RaisedButton(
                child: new Text("Save"),
                textColor: Colors.white,
                color: Colors.green,
                onPressed: () {
                  setState(() {
                    _status = true;
                    FocusScope.of(context).requestFocus(new FocusNode());
                  });
                  _saveprofile();
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          ),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(left: 10.0),
              child: Container(
                  child: new RaisedButton(
                child: new Text("Cancel"),
                textColor: Colors.white,
                color: Colors.red,
                onPressed: () {
                  setState(() {
                    _status = true;
                    FocusScope.of(context).requestFocus(new FocusNode());
                  });
                },
                shape: new RoundedRectangleBorder(
                    borderRadius: new BorderRadius.circular(20.0)),
              )),
            ),
            flex: 2,
          ),
        ],
      ),
    );
  }

  Widget _getEditIcon() {
    return new GestureDetector(
      child: new CircleAvatar(
        backgroundColor: Colors.red,
        radius: 14.0,
        child: new Icon(
          Icons.edit,
          color: Colors.white,
          size: 16.0,
        ),
      ),
      onTap: () {
        setState(() {
          _status = false;
        });
      },
    );
  }
}
