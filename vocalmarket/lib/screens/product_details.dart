import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';
import 'package:vocalmarket/app_translations.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/attribute.dart';
import 'package:vocalmarket/odoo/data/pojo/product_review.dart';
// import 'package:vocalmarket/odoo/data/pojo/orders.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';
// import 'package:vocalmarket/widget/product/product_review.dart';
// import 'package:vocalmarket/widget/product/product_review.dart';
// import 'package:vocalmarket/widget/text/titletext.dart';
// import 'package:vocalmarket/widget/product/productattribute.dart';
import 'package:vocalmarket/odoo/data/pojo/product.dart';
import 'package:vocalmarket/widget/text/titletext.dart';
import 'package:shared_preferences/shared_preferences.dart';
// import 'package:percent_indicator/percent_indicator.dart';

class ProductDetails extends StatefulWidget {
  final String partnerId;
  final String productId;
  final String name;
  final String imgPath;
  final String price;
  final String description;
  final String symbol;
  final String productLabel;
  final String attributeCheck;

  ProductDetails(
      {Key key,
      @required this.partnerId,
      this.productId,
      this.imgPath,
      this.name,
      this.description,
      this.price,
      this.symbol,
      this.productLabel,
      this.attributeCheck})
      : super(key: key);

  @override
  _ProductDetailsState createState() => _ProductDetailsState();
}

class _ProductDetailsState extends Base<ProductDetails> {
  // Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  List<Attribute> _attribute = [];
  static List<ProductsImage> _productImage = [];
  List<ProductReview> _productReview = [];
  Map<String, List> attributeListMap = {};
  // Attribute _selectedAttribute;
  TextEditingController productfelling = new TextEditingController();
  TextEditingController message = new TextEditingController();
  var partnerId = "";
  String variantId;
  List<String> responseValue;
  var saleOrderId;
  var presentSo;
  var productName = "";
  var uomId = "";
  var productPrice = "";
  var soPresent = false;
  double star1 = 0.0;
  double star2 = 0.0;
  double star3 = 0.0;
  double star4 = 0.0;
  double star5 = 0.0;
  double total = 0.0;
  double setreview = 0.0;
  double averagetotal = 0.0;
  String frametype = "";
  String color = 'Black';

  // final CarouselController _controller = CarouselController();

  getProductImage() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("product.image", [
          ['product_tmpl_id', '=', int.parse(widget.productId)]
        ], [
          'id',
          'name',
          'image'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  String session = getSession();
                  session = session.split(",")[0].split(";")[0];
                  for (var i in res.getRecords()) {
                    _productImage.add(new ProductsImage(
                        imageUrl: getURL() +
                            "/web/image?model=product.image&field=image&" +
                            session +
                            "&id=" +
                            i["id"].toString()));
                  }
                });
              }
            }
          },
        );
      }
    });
  }

  getProductRating() async {
    print("getProductRating==================");
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("reviews.ratings", [
          ['rating_product_id', '=', int.parse(widget.productId)]
        ], [
          'id',
          'message_rate',
          'review',
          'rating_product_id'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    _productReview.add(new ProductReview(
                        id: i["id"].toString(),
                        messagerate: i["message_rate"].toString(),
                        review: i["review"]));
                    total++;
                    averagetotal += i['message_rate'];
                    if (i['message_rate'] == 1) {
                      star1++;
                    } else if (i['message_rate'] == 2) {
                      star2++;
                    } else if (i['message_rate'] == 3) {
                      star3++;
                    } else if (i['message_rate'] == 4) {
                      star4++;
                    } else if (i['message_rate'] == 5) {
                      star5++;
                    }
                  }
                });
              }
            }
          },
        );
      }
    });
  }

  getUserData() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead(Strings.res_users, [
          ["id", "=", getUID()]
        ], []).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  final result = res.getResult()['records'][0];
                  partnerId = result['partner_id'][0].toString();
                });
              }
            } else {
              // showMessage("Warning", res.getErrorMessage());
            }
          },
        );
      }
    });
  }

  getAttribute() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        odoo.searchRead("product.product", [
          ['product_tmpl_id', "=", int.parse(widget.productId)]
        ], [
          'id',
          'name',
          'combine_attribute_name'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    _attribute.add(new Attribute(
                        id: i["id"].toString(),
                        name: i["name"],
                        attributename: i["combine_attribute_name"] != null
                            ? i["combine_attribute_name"]
                            : "data"));
                  }
                  // hideLoading();
                });
              }
            }
          },
        );
      }
    });
  }

  createSoLine(result) async {
    odoo.create("sale.order.line", {
      'order_id': result,
      'product_id': int.parse(variantId)
    }).then((OdooResponse soLine) {
      // hideLoadingSuccess("Product Added in Cart!");
    });
  }

  getProductProduct() async {
    odoo
        .searchRead(
            'product.product',
            [
              ['product_tmpl_id', "=", int.parse(widget.productId)]
            ],
            ['id'],
            limit: 1)
        .then((OdooResponse res) {
      if (!res.hasError()) {
        setState(() {
          for (var i in res.getRecords()) {
            variantId = i['id'].toString();
          }
        });
      }
    });
  }

  addtocart() async {
    final prefs = await SharedPreferences.getInstance();
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead('sale.order', [
          ['id', '=', prefs.getInt('order_id')],
          ['state', '=', 'draft']
        ], [
          'id'
        ]).then((OdooResponse so) {
          if (so.getResult()['length'] == 0) {
            odoo.create('sale.order', {
              'partner_id': int.parse(widget.partnerId)
            }).then((OdooResponse newso) {
              if (!newso.hasError()) {
                prefs.setInt('order_id', newso.getResult());
                createSoLine(newso.getResult());
              }
            });
          } else {
            setState(() {
              createSoLine(prefs.getInt('order_id'));
            });
          }
        });
      }
    });
  }

  postreview() async {
    if (message.text.isEmpty) {
      // hideLoadingError("Please write review!");
    } else {
      odoo.create("reviews.ratings", {
        'rating_product_id': int.parse(widget.productId),
        'customer_id': user.result.uid,
        'short_desc': productfelling.text,
        'review': message.text,
        'message_rate': setreview
      }).then((OdooResponse resreview) {
        productfelling.clear();
        message.clear();
        // hideLoadingSuccess("Review Posted!");
      });
    }
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getAttribute();
      getProductImage();
      getProductProduct();
      // getProductRating();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  Widget _widgetProductImage() {
    return Container(
        child: CarouselSlider(
      options: CarouselOptions(
        autoPlay: false,
        enlargeCenterPage: true,
        viewportFraction: 0.9,
        aspectRatio: 2.0,
        initialPage: 2,
      ),
      items: _productImage
          .map((item) => Container(
                child: Center(
                    child: Image.network(item.imageUrl, fit: BoxFit.fitHeight)),
              ))
          .toList(),
    ));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black,
        ),
        title: TitleText(
          text: widget.name,
          fontSize: 16.0,
          color: Colors.black,
        ),
        backgroundColor: Colors.white,
      ),
      body: SingleChildScrollView(
        scrollDirection: Axis.horizontal,
        child: Column(
          children: <Widget>[
            new Container(
              width: MediaQuery.of(context).size.width,
              height: 300.0,
              child: GridTile(
                  child:
                      // _widgetProductImage(),
                      Stack(
                    alignment: Alignment.center,
                    children: <Widget>[
                      Image.network(
                        widget.imgPath,
                        fit: BoxFit.fill,
                        height: 80.0,
                      )
                    ],
                  ),
                  footer: Container(
                      child: Row(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          mainAxisAlignment: MainAxisAlignment.spaceBetween,
                          children: <Widget>[
                        new Flexible(
                          child: new Container(
                            padding: new EdgeInsets.all(5.0),
                            child: new Text(
                              widget.name,
                              overflow: TextOverflow.ellipsis,
                              style: new TextStyle(
                                fontSize: 25.0,
                                fontFamily: 'Roboto',
                                color: new Color(0xFF212121),
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                          ),
                        ),
                        Column(
                            crossAxisAlignment: CrossAxisAlignment.end,
                            children: <Widget>[
                              Row(
                                crossAxisAlignment: CrossAxisAlignment.center,
                                children: <Widget>[
                                  TitleText(
                                    text: widget.symbol,
                                    fontSize: 18,
                                    color: Colors.red,
                                  ),
                                  TitleText(
                                    text: widget.price.toString(),
                                    fontSize: 25,
                                  ),
                                ],
                              ),
                            ])
                      ]))),
            ),
            new Container(
              child: Column(
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                    child: new TitleText(
                      text: AppTranslations.of(context)
                          .text("Product_Description"),
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new TitleText(
                      text: widget.description,
                      fontSize: 14.0,
                      color: Colors.grey,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.fromLTRB(12.0, 5.0, 5.0, 5.0),
                    child: new TitleText(
                      text: AppTranslations.of(context).text("Product_Brand"),
                      fontSize: 14.0,
                      color: Colors.black,
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: new TitleText(
                      text: widget.description,
                      fontSize: 14.0,
                      color: Colors.grey,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),

      // Column(
      //   children: [
      //     new Container(
      //       width: MediaQuery.of(context).size.width,
      //       height: 300.0,
      //       child: GridTile(
      //           child: _widgetProductImage(),
      //           footer: Container(
      //               child: Row(
      //                   crossAxisAlignment: CrossAxisAlignment.start,
      //                   mainAxisAlignment: MainAxisAlignment.spaceBetween,
      //                   children: <Widget>[
      //                 new Flexible(
      //                   child: new Container(
      //                     padding: new EdgeInsets.all(5.0),
      //                     child: new Text(
      //                       widget.name,
      //                       overflow: TextOverflow.ellipsis,
      //                       style: new TextStyle(
      //                         fontSize: 25.0,
      //                         fontFamily: 'Roboto',
      //                         color: new Color(0xFF212121),
      //                         fontWeight: FontWeight.bold,
      //                       ),
      //                     ),
      //                   ),
      //                 ),
      //                 Column(
      //                     crossAxisAlignment: CrossAxisAlignment.end,
      //                     children: <Widget>[
      //                       Row(
      //                         crossAxisAlignment: CrossAxisAlignment.center,
      //                         children: <Widget>[
      //                           TitleText(
      //                             text: widget.symbol,
      //                             fontSize: 18,
      //                             color: Colors.red,
      //                           ),
      //                           TitleText(
      //                             text: widget.price.toString(),
      //                             fontSize: 25,
      //                           ),
      //                         ],
      //                       ),
      //                     ])
      //               ]))),
      //     ),
      //     new Container()
      //   ],
      // ),

      // SizedBox(
      //   height: 20,
      // ),
      // widget.attributeCheck == "yes"
      //     ? ProductAttribute(
      //         productId: widget.productId,
      //         onChangeAttribute: (String vals) {
      //           setState(() {
      //             variantId = vals;
      //           });
      //         })
      //     : Container(),
      // Container(
      //   width: MediaQuery.of(context).size.width,
      //   height: 3000,
      // )

      bottomNavigationBar: Padding(
        padding: const EdgeInsets.all(8.0),
        child: Container(
          height: 50,
          child: Row(
            children: <Widget>[
              Expanded(
                child: Container(
                  margin: const EdgeInsets.only(right: 15),
                  height: double.infinity,
                  child: RaisedButton(
                    child: Text(
                      AppTranslations.of(context).text("add_to_cart"),
                      style: Theme.of(context).textTheme.button.copyWith(
                            color: Colors.white,
                          ),
                    ),
                    onPressed: () {
                      getOdooInstance().then((odoo) {
                        addtocart();
                      });
                      _displaySnackBar(context);
                    },
                    color: Colors.black,
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(15.0),
                    ),
                  ),
                ),
              ),
              // Container(
              //   margin: const EdgeInsets.only(right: 15),
              //   height: double.infinity,
              //   decoration: BoxDecoration(
              //     color: Colors.black,
              //     borderRadius: BorderRadius.circular(
              //       15.0,
              //     ),
              //   ),
              //   child: IconButton(
              //     icon: Icon(
              //       Icons.favorite_border,
              //       color: Colors.white,
              //     ),
              //     onPressed: () {},
              //   ),
              // ),
              // Container(
              //   height: double.infinity,
              //   decoration: BoxDecoration(
              //     color: Colors.black,
              //     borderRadius: BorderRadius.circular(
              //       15.0,
              //     ),
              //   ),
              //   child: IconButton(
              //     icon: Icon(
              //       Icons.share,
              //       color: Colors.white,
              //     ),
              //     onPressed: () {},
              //   ),
              // ),
            ],
          ),
        ),
      ),
    );
  }

  _displaySnackBar(BuildContext context) {
    final snackBar = SnackBar(
        content:
            Text(AppTranslations.of(context).text("Product_added_in_cart")));
    _scaffoldKey.currentState.showSnackBar(snackBar);
  }
}
