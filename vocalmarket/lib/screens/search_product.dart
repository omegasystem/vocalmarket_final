import 'dart:io';

import 'package:flutter/material.dart';
// import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:vocalmarket/app_translations.dart';
import 'package:vocalmarket/application.dart';
import 'package:flutter/cupertino.dart';
// import 'package:vocalmarket/odoo/data/pojo/partners.dart';
// import 'package:vocalmarket/odoo/data/pojo/currency.dart';
import 'package:vocalmarket/odoo/data/pojo/product.dart';

import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/screens/product_details.dart';
import 'package:vocalmarket/widget/product/productfilter.dart';
import 'package:vocalmarket/widget/product/sortingsheet.dart';
import 'package:vocalmarket/widget/text/titletext.dart';
import 'package:progress_dialog/progress_dialog.dart';

ProgressDialog pr;

class SearchProducts extends StatefulWidget {
  final String searchValue;

  const SearchProducts({Key key, this.searchValue}) : super(key: key);

  @override
  _SearchProductsState createState() => _SearchProductsState();
}

class _SearchProductsState extends Base<SearchProducts> {
  List<Products> _products = [];
  Map<String, String> mapdata;
  String toCurrency = "INR";
  int loadoffset = 0;
  String currencyId = "";
  String variantId;
  int selectedRadioTile;
  ScrollController _scrollController = ScrollController();
  bool isLoading = false;

  static final List<String> languagesList = application.supportedLanguages;
  static final List<String> languageCodesList =
      application.supportedLanguagesCodes;

  final Map<dynamic, dynamic> languagesMap = {
    languagesList[0]: languageCodesList[0],
    languagesList[1]: languageCodesList[1],
  };

  // THIS FUNCTION MANUPULATED WITH ODOO SEARCH METHOD, PASSED CURRENCY AND GET CONVERTED CURRENCY WITH SYMBOL
  getOdooProducts(loadoffset,
      [String searchValue = "", String order = "id asc"]) async {
    isConnected().then((isInternet) {
      toCurrency = partner.partnerresult.appCurrencyName;
      if (isInternet) {
        odoo
            .searchRead(
                "product.template",
                [
                  ["name", "ilike", searchValue]
                ],
                [
                  'id',
                  'name',
                  'image',
                  'lst_price',
                  'label_ept_id',
                  'description_sale',
                  'attribute_line_ids'
                ],
                offset: loadoffset,
                limit: 6,
                order: order,
                context: {'toCurrency': toCurrency})
            .then(
          (OdooResponse res) {
            if (!res.hasError()) {
              String session = getSession();
              session = session.split(",")[0].split(";")[0];
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    final data = i["attribute_line_ids"] as List;

                    _products.add(new Products(
                        id: i["id"].toString(),
                        name: i["name"],
                        price: i["lst_price"],
                        imageUrl: getURL() +
                            "/web/image?model=product.template&field=image&" +
                            session +
                            "&id=" +
                            i["id"].toString(),
                        description: i["description_sale"] is! bool
                            ? i["description_sale"]
                            : i["name"],
                        productLabel: i["label_ept_id"] is! bool
                            ? i["label_ept_id"][1].toString()
                            : "false",
                        symbol: i["symbol"],
                        attributeCheck: data.isNotEmpty ? "yes" : "no"));
                  }
                });
              }
            }
          },
        );
      }
    });
    return _products;
  }

  @override
  void initState() {
    _scrollController = ScrollController();
    _scrollController.addListener(_scrollListener);
    super.initState();
    getOdooInstance().then((odoo) {
      getOdooProducts(loadoffset, widget.searchValue);
    });
  }

  _scrollListener() {
    _loadMore();
  }

  @override
  void dispose() {
    super.dispose();
  }

  void updateProductList(List listVal) {
    if (this.mounted) {
      setState(() {
        _products.clear();
        _products = listVal;
      });
    }
  }

  _loadMore() async {
    toCurrency = partner.partnerresult.appCurrencyName;
    setState(() {
      isLoading = true;
    });
    await new Future.delayed(const Duration(seconds: 2));
    getOdooInstance().then((odoo) {
      getOdooProducts(loadoffset);
    });
    if (this.mounted) {
      setState(() {
        isLoading = false;
        loadoffset = loadoffset + 6;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(
          color: Colors.black, //change your color here
        ),
        title: Text(
          widget.searchValue,
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        // actions: <Widget>[
        //   IconButton(
        //     color: Colors.black,
        //     icon: Icon(Icons.sort),
        //     onPressed: () {
        //       showModalBottomSheet<void>(
        //           context: context,
        //           builder: (BuildContext context) {
        //             return SortingSheet(
        //               categId: "widget.categId",
        //               productList: _products,
        //               updateList: (List listval) {
        //                 updateProductList(listval);
        //               },
        //             );
        //           });
        //     },
        //   ),
        //   IconButton(
        //     color: Colors.black,
        //     icon: Icon(Icons.filter_list),
        //     onPressed: () {
        //       Navigator.push(
        //           context,
        //           MaterialPageRoute(
        //               builder: (context) => ProductFilter(
        //                     categId: "widget.categId",
        //                     productList: _products,
        //                     updateList: (List listval) {
        //                       updateProductList(listval);
        //                     },
        //                   )));
        //     },
        //   ),
        // ],
      ),
      body:
          // _products.isNotEmpty ?
          // LazyLoadScrollView(
          //   isLoading: isLoading,
          //   onEndOfPage: () => _loadMore(),
          //   child: GridView.builder(
          //     itemCount: _products.length,
          //     gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
          //       crossAxisCount: 2,
          //       childAspectRatio: MediaQuery.of(context).size.width /
          //           (MediaQuery.of(context).size.height),
          //     ),
          //     itemBuilder: (BuildContext context, int index) {
          //       return ProductItem(
          //           productId: _products[index].id,
          //           name: _products[index].name,
          //           imgPath: _products[index].imageUrl,
          //           price: _products[index].price,
          //           partnerId: user.result.partnerId.toString(),
          //           symbol: _products[index].symbol,
          //           description: _products[index].description,
          //           productLabel: _products[index].productLabel,
          //           attributeCheck: _products[index].attributeCheck);
          //     },
          //   ),
          // ): Center(child: CircularProgressIndicator())

          new Stack(
        children: <Widget>[
          _buildProductList(),
          _loader(),
        ],
      ),
    );
  }

  Widget _loader() {
    return isLoading
        ? new Align(
            child: new Container(
              width: 70.0,
              height: 70.0,
              child: new Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Platform.isIOS
                      ? new CupertinoActivityIndicator()
                      : new CircularProgressIndicator()),
            ),
            alignment: FractionalOffset.bottomCenter,
          )
        : new SizedBox(
            width: 0.0,
            height: 0.0,
          );

    // return isLoading
    //     ? new Center(child: new CircularProgressIndicator())
    //     : new SizedBox(
    //         width: 0.0,
    //         height: 0.0,
    //       );
  }

  Widget _buildProductList() {
    return _products.isNotEmpty
        ? GridView.builder(
            controller: _scrollController,
            itemCount: _products.length,
            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
              crossAxisCount: 2,
              childAspectRatio: MediaQuery.of(context).size.width /
                  (MediaQuery.of(context).size.height),
            ),
            itemBuilder: (BuildContext context, int index) {
              return ProductItem(
                  productId: _products[index].id,
                  name: _products[index].name,
                  imgPath: _products[index].imageUrl,
                  price: _products[index].price,
                  partnerId: user.result.partnerId.toString(),
                  symbol: _products[index].symbol,
                  description: _products[index].description,
                  productLabel: _products[index].productLabel,
                  attributeCheck: _products[index].attributeCheck);
            },
          )
        : Container(
            child: new Center(
                child: Platform.isIOS
                    ? new CupertinoActivityIndicator()
                    : new CircularProgressIndicator()));
  }
}

class ProductItem extends StatefulWidget {
  final String productId;
  final String name;
  final String imgPath;
  final double price;
  final String partnerId;
  final String symbol;
  final String description;
  final String productLabel;
  final String attributeCheck;

  ProductItem(
      {Key key,
      @required this.productId,
      this.name,
      this.imgPath,
      this.price,
      this.partnerId,
      this.symbol,
      this.description,
      this.productLabel,
      this.attributeCheck})
      : super(key: key);

  @override
  _ProductItemState createState() => _ProductItemState();
}

class _ProductItemState extends Base<ProductItem> {
  final String partnerId = "";
  var productid = "";
  var productName = "";
  var uomId = "";
  var productPrice = "";
  String variantId = "";
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getProductProduct();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  createSoLine(result) async {
    odoo.create("sale.order.line", {
      'order_id': result,
      'product_id': int.parse(variantId)
    }).then((OdooResponse soLine) {
      // _scaffoldKey.currentState.showSnackBar(new SnackBar(
      //     content: Text("Please insert correct Email & Password!")));
      // EasyLoading.showSuccess("Product Added in Cart!");
      // hideLoadingSuccess("");
    });
  }

  getProductProduct() async {
    odoo
        .searchRead(
            'product.product',
            [
              ['product_tmpl_id', "=", int.parse(widget.productId)]
            ],
            ['id'],
            limit: 1)
        .then((OdooResponse res) {
      if (!res.hasError()) {
        if (this.mounted) {
          setState(() {
            for (var i in res.getRecords()) {
              variantId = i['id'].toString();
            }
          });
        }
      }
    });
  }

  addtocart() async {
    final prefs = await SharedPreferences.getInstance();
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead('sale.order', [
          ['id', '=', prefs.getInt('order_id')],
          ['state', '=', 'draft']
        ], [
          'id'
        ]).then((OdooResponse so) {
          if (so.getResult()['length'] == 0) {
            odoo.create('sale.order', {
              'partner_id': int.parse(widget.partnerId)
            }).then((OdooResponse newso) {
              if (!newso.hasError()) {
                prefs.setInt('order_id', newso.getResult());
                createSoLine(newso.getResult());
              }
            });
          } else {
            if (this.mounted) {
              setState(() {
                createSoLine(prefs.getInt('order_id'));
              });
            }
          }
        });
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    final ThemeData theme = Theme.of(context);
    final TextStyle descriptionStyle = theme.textTheme.subhead;

    return Scaffold(
        key: _scaffoldKey,
        body: SafeArea(
            top: false,
            bottom: false,
            child: Container(
                padding: const EdgeInsets.all(4.0),
                height: 1200,
                // color: Colors.red,
                child: GestureDetector(
                  onTap: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) => ProductDetails(
                                productId: widget.productId,
                                name: widget.name,
                                imgPath: widget.imgPath,
                                price: widget.price.toString(),
                                partnerId: widget.partnerId,
                                symbol: widget.symbol,
                                description: widget.description,
                                attributeCheck: widget.attributeCheck)));
                  },
                  child: Card(
                    // shape: widget.shape,
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        // LABEL
                        widget.productLabel != "false"
                            ? Container(
                                child: Chip(
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(1.0),
                                ),
                                label: Text(
                                  widget.productLabel,
                                  style: TextStyle(color: Colors.white),
                                ),
                                backgroundColor: Colors.black,
                              ))
                            : Container(),
                        // PRODUCT IMAGE
                        SizedBox(
                          height: 170.0,
                          child: Stack(
                            children: <Widget>[
                              Positioned.fill(
                                  child: FadeInImage(
                                height: 50.0,
                                placeholder:
                                    new AssetImage('images/placeholder.jpeg'),
                                image: new NetworkImage(widget.imgPath),
                                fit: BoxFit.fitHeight,
                              )),
                              // Container(
                              //   alignment: Alignment.topLeft,
                              //  // padding: EdgeInsets.all(5.0),
                              //   child: IconButton(icon: const Icon(Icons.favorite_border), onPressed: (){

                              //   }),
                              // ),
                            ],
                          ),
                        ),
                        Divider(),
                        // PRODUCT NAME
                        Expanded(
                          child: Container(
                            // padding:
                            //     const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
                            padding: EdgeInsets.only(left: 10.0, right: 10.0),
                            child: DefaultTextStyle(
                              style: descriptionStyle,
                              child: Row(
                                mainAxisSize: MainAxisSize.max,
                                mainAxisAlignment:
                                    MainAxisAlignment.spaceBetween,
                                children: <Widget>[
                                  new Flexible(
                                    child: Container(
                                      alignment: Alignment.topCenter,
                                      child: Text(
                                        widget.name,
                                        overflow: TextOverflow.ellipsis,
                                        style: descriptionStyle.copyWith(
                                            color: Colors.black87,
                                            fontSize: 15.0,
                                            fontFamily: "Quicksand"),
                                      ),
                                    ),
                                  ),
                                ],
                              ),
                            ),
                          ),
                        ),
                        // PRODUCT PRICE
                        Container(
                          child: Padding(
                              padding: const EdgeInsets.only(
                                  right: 8.0,
                                  left: 15.0,
                                  top: 5.0,
                                  bottom: 5.0),
                              child: new TitleText(
                                text: widget.symbol +
                                    widget.price.toStringAsFixed(2),
                                fontSize: 15.0,
                                color: Colors.black,
                              )),
                        ),
                        //CART BUTTON
                        Container(
                            padding: EdgeInsets.only(left: 10.0, right: 10.0),
                            alignment: Alignment.centerLeft,
                            child: new RaisedButton(
                                color: Colors.black,
                                onPressed: () {
                                  // Navigator.push(context,
                                  //     new MaterialPageRoute(builder: (context) => CartPage()));
                                  getOdooInstance().then((odoo) {
                                    addtocart();
                                  });
                                  final snackBar = SnackBar(
                                    content: Text(AppTranslations.of(context)
                                        .text("Product_added_in_cart")),
                                  );
                                  Scaffold.of(context).showSnackBar(snackBar);
                                },
                                shape: new RoundedRectangleBorder(
                                    borderRadius:
                                        new BorderRadius.circular(0.0)),
                                textColor: Colors.white,
                                child: new TitleText(
                                  text: AppTranslations.of(context)
                                      .text("add_to_cart"),
                                  fontSize: 15.0,
                                  color: Colors.white,
                                ))),
                      ],
                    ),
                  ),
                ))));
  }
}
