// import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:vocalmarket/odoo/data/pojo/partners.dart';
import 'package:vocalmarket/odoo/data/pojo/user.dart';
import 'package:vocalmarket/odoo/data/services/odoo_api.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/screens/home.dart';
// import 'package:vocalmarket/screens/settings/settings_list.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/widget/text/textLogin.dart';
import 'package:vocalmarket/widget/text/inputEmail.dart';
import 'package:vocalmarket/widget/text/password.dart';
import 'package:vocalmarket/widget/first.dart';
// import 'package:vocalmarket/widget/button/button.dart';
import 'package:vocalmarket/widget/button/facebookbutton.dart';
import 'package:vocalmarket/widget/button/linkedinbutton.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';

class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends Base<Login> {
  String odooURL;
  String _selectedDb;
  final Map<String, dynamic> someMap = {};
  String _email;
  String _pass;
  List<String> _dbList = [];
  List dynamicList = [];
  bool isCorrectURL = false;
  bool isDBFilter = false;
  TextEditingController _emailCtrler = new TextEditingController();
  TextEditingController _passCtrler = new TextEditingController();
  final myController = TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  _checkFirstTime() {
    if (getURL() != null) {
      odooURL = getURL();
      _checkURL();
    }
  }

  _checkURL() {
    isConnected().then((isInternet) {
      if (isInternet) {
        // showLoading();
        // Init Odoo URL when URL is not saved
        print("odooURL========----------------------------" + odooURL);
        odoo = new Odoo(url: odooURL);
        odoo.getDatabases().then((http.Response res) {
          if (this.mounted) {
            setState(
              () {
                // hideLoadingSuccess("");
                isCorrectURL = true;
                dynamicList = json.decode(res.body)['result'] as List;
                saveOdooUrl(odooURL);
                dynamicList.forEach((db) => _dbList.add(db));
                _selectedDb = _dbList[0];
                if (_dbList.length == 1) {
                  isDBFilter = true;
                } else {
                  isDBFilter = false;
                }
              },
            );
          }
        }).catchError(
          (e) {
            showMessage("Warning", "Invalid URL");
          },
        );
      }
    });
  }

  _login(String email, String pass) {
    print("login------------$email");
    getOdooInstance().then((odoo) {
      if (isValid(email, pass)) {
        isConnected().then((isInternet) {
          if (isInternet) {
            odoo.authenticate(email, pass, Strings.odoo_db).then(
              (http.Response auth) {
                if (auth.body != null) {
                  User user = User.fromJson(jsonDecode(auth.body));
                  if (user.result == null) {
                    _scaffoldKey.currentState.showSnackBar(new SnackBar(
                        content:
                            Text("Please Enter Valid Email or Password!")));
                  } else {
                    _scaffoldKey.currentState.showSnackBar(
                        new SnackBar(content: Text("Logged in successfully!")));
                    saveUser(json.encode(user));
                    odoo.searchRead(Strings.res_partner, [
                      ["id", "=", user.result.partnerId]
                    ], [
                      "id",
                      "app_currency_id"
                    ]).then(
                      (OdooResponse res) {
                        if (!res.hasError()) {
                          someMap['result'] = res.getResult()['records'][0];
                          print("someMap-----------------_$someMap");
                          Partner partner = Partner.fromJson(someMap);
                          savePartner(json.encode(partner));
                          saveOdooUrl(Strings.odoo_url);
                          pushReplacement(Home());
                        } else {
                          _scaffoldKey.currentState.showSnackBar(new SnackBar(
                              content: Text(res.getErrorMessage())));
                        }
                      },
                    );
                  }
                } else {
                  _scaffoldKey.currentState.showSnackBar(new SnackBar(
                      content: Text("Please Enter Valid Email or Password")));
                }
              },
            );
          }
        });
      }
    });
  }

  @override
  void initState() {
    super.initState();

    getOdooInstance().then((odoo) {
      _checkFirstTime();
    });
  }

  @override
  void dispose() {
    myController.dispose();
    super.dispose();
  }

  bool isValid(String email, String pass) {
    if (email.length > 0 && pass.length > 0) {
      return true;
    } else {
      _scaffoldKey.currentState.showSnackBar(new SnackBar(
          content: Text("Please insert correct Email & Password!")));
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        key: _scaffoldKey,
        body: Container(
          decoration: BoxDecoration(
            gradient: LinearGradient(
                begin: Alignment.topRight,
                end: Alignment.bottomLeft,
                colors: [Colors.white, Colors.white]),
          ),
          child: ListView(
            children: <Widget>[
              Column(
                children: <Widget>[
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 40, right: 50, left: 50),
                    child: Container(
                        height: 100,
                        width: MediaQuery.of(context).size.width,
                        child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.center,
                            children: <Widget>[
                              SizedBox(
                                height: 100.0,
                                child: Image.asset(
                                  "assets/logo.png",
                                  fit: BoxFit.contain,
                                ),
                              ),
                            ])),
                  ),

                  InputEmail(
                    controller: _emailCtrler,
                  ),
                  PasswordInput(
                    controller: _passCtrler,
                  ),
                  Padding(
                    padding:
                        const EdgeInsets.only(top: 40, right: 50, left: 50),
                    child: Container(
                      alignment: Alignment.bottomRight,
                      height: 50,
                      width: MediaQuery.of(context).size.width,
                      decoration: BoxDecoration(
                        color: Colors.black,
                        borderRadius: BorderRadius.circular(0),
                      ),
                      child: FlatButton(
                        onPressed: () {
                          _login(_emailCtrler.text.trim(),
                              _passCtrler.text.trim());
                        },
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              'LOG IN',
                              style: TextStyle(
                                  color: Colors.white, fontFamily: "Quicksand"),
                            ),
                            Icon(
                              Icons.arrow_forward,
                              color: Colors.white,
                            ),
                          ],
                        ),
                      ),
                    ),
                  ),
                  FirstTime(),
                  // FacebookButton(),
                  // LinkedinButton(),
                ],
              )
            ],
          ),
        )
        // floatingActionButton: isLoggedIn()
        //     ? FloatingActionButton(
        //         child: Icon(Icons.settings),
        //         onPressed: () {
        //           // pushReplacement(Settings());
        //         },
        //       )
        //     : SizedBox(height: 0.0),
        );
  }
}
