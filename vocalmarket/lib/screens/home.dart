import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
// import 'package:vocalmarket/app_translations.dart';
import 'package:vocalmarket/app_translations_delegate.dart';
import 'package:vocalmarket/application.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/screens/login.dart';
// import 'package:vocalmarket/odoo/utility/constant.dart';
// import 'package:vocalmarket/screens/login.dart';
import 'package:vocalmarket/widget/bottombar.dart';
// import 'package:vocalmarket/widget/product/currency.dart';
// import 'package:vocalmarket/widget/product/productfilter.dart';
import 'package:vocalmarket/screens/orderhistory.dart';
import 'package:vocalmarket/screens/homepage.dart';
import 'package:vocalmarket/screens/cartpage.dart';
// import 'package:vocalmarket/widget/product/language.dart';
import 'package:vocalmarket/screens/settings/settings_list.dart';

class Home extends StatefulWidget {
  @override
  _HomeState createState() => _HomeState();
}

class _HomeState extends Base<Home> {
  int selectedPosition = 0;
  List<Widget> listBottomWidget = new List();
  AppTranslationsDelegate _newLocaleDelegate;

  static final List<String> languagesList = application.supportedLanguages;
  static final List<String> languageCodesList =
      application.supportedLanguagesCodes;

  final Map<dynamic, dynamic> languagesMap = {
    languagesList[0]: languageCodesList[0],
    languagesList[1]: languageCodesList[1],
  };

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      isLoggedIn() ? Home() : Login();
    });
    addHomePage();
    _newLocaleDelegate = AppTranslationsDelegate(newLocale: null);
    application.onLocaleChanged = onLocaleChange;
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
        // drawer: MenuDrawer(),
        bottomNavigationBar: BottomBar(onChangeMenu: (int val) {
          setState(() {
            selectedPosition = val;
          });
        }),
        appBar: AppBar(
          iconTheme: new IconThemeData(color: Colors.black),
          backgroundColor: Colors.white,
          title: Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Image(
                image: AssetImage('assets/logo.png'),
                height: 50.0,
              )
            ],
          ),
          actions: <Widget>[
            // OdooLanguageSelector(
            //    onChangeLang: (String val){
            //     application.onLocaleChanged(Locale(languagesMap[val]));
            //   }
            // ),
            // ProductCurrency(
            //   onChangeCurrency: (String val){
            //   }
            // ),
            // IconButton(
            //   color: Colors.black,
            //   icon: Icon(Icons.shopping_cart),
            //   onPressed: () {
            //   },
            // ),
          ],
        ),
        body: Builder(builder: (context) {
          return listBottomWidget[selectedPosition];
        }),
      ),
      localizationsDelegates: [
        _newLocaleDelegate,
        //provides localised strings
        GlobalMaterialLocalizations.delegate,
        //provides RTL support
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale("en", ""),
        const Locale("cn", ""),
      ],
    );
  }

  void addHomePage() {
    listBottomWidget.add(HomePage());
    listBottomWidget.add(OrderHistory());
    listBottomWidget.add(CartPage());
    listBottomWidget.add(SettingsScreen());
  }

  void onLocaleChange(Locale locale) {
    setState(() {
      _newLocaleDelegate = AppTranslationsDelegate(newLocale: locale);
    });
  }
}
