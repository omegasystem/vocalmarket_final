import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/data/pojo/orders.dart';
import 'package:flutter/material.dart';

class OrderDetails extends StatefulWidget {
  final String orderId;

  OrderDetails({Key key, this.orderId}) : super(key: key);

  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends Base<OrderDetails> {
  String toolbarname = 'Fruiys & Vegetables';
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  List list = ['12', '11'];
  List<OrdersLine> _ordersLine = [];

  int item = 0;

  getOrderData([String orderId]) async {
    print("orderId------------------" + orderId);
    isConnected().then((isInternet) {
      if (isInternet) {
        print("isinter-----------------");
        odoo.searchRead("sale.order.line", [
          ['order_id', '=', int.parse(orderId)],
        ], [
          'id',
          'product_id',
          'price_total',
          'price_unit',
          'product_uom_qty'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                setState(() {
                  for (var i in res.getRecords()) {
                    // countOrder++;
                    // totalAmount += i["price_total"];
                    _ordersLine.add(new OrdersLine(
                        id: i["id"].toString(),
                        name: i["product_id"][1],
                        priceUnit: i["price_unit"].toString(),
                        productUomQty: i["product_uom_qty"].toString(),
                        priceTotal: i["price_total"].toString()));
                  }
                  // hideLoading();
                });
              }
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    print("initState-------------------" + widget.orderId);
    super.initState();
    // getOdooInstance().then((odoo) {
    //   getOrderData(widget.orderId);
    // });
  }

  @override
  void dispose() {
    super.dispose();
  }

  viewList() {
    return ListView.builder(
        shrinkWrap: true,
        primary: false,
        itemBuilder: (context, position) {
          return Stack(children: <Widget>[Container()]);
        });
  }

  @override
  Widget build(BuildContext context) {
    // final ThemeData theme = Theme.of(context);
    // final TextStyle titleStyle =
    //     theme.textTheme.headline.copyWith(color: Colors.white);

    // final TextStyle descriptionStyle = theme.textTheme.subhead;
    // IconData _backIcon() {
    //   switch (Theme.of(context).platform) {
    //     case TargetPlatform.android:
    //     case TargetPlatform.fuchsia:
    //       return Icons.arrow_back;
    //     case TargetPlatform.iOS:
    //       return Icons.arrow_back_ios;
    //   }
    //   assert(false);
    //   return null;
    // }

    // IconData _add_icon() {
    //   switch (Theme.of(context).platform) {
    //     case TargetPlatform.android:
    //     case TargetPlatform.fuchsia:
    //       return Icons.add_circle;
    //     case TargetPlatform.iOS:
    //       return Icons.add_circle;
    //   }
    //   assert(false);
    //   return null;
    // }

    // IconData _sub_icon() {
    //   switch (Theme.of(context).platform) {
    //     case TargetPlatform.android:
    //     case TargetPlatform.fuchsia:
    //       return Icons.remove_circle;
    //     case TargetPlatform.iOS:
    //       return Icons.remove_circle;
    //   }
    //   assert(false);
    //   return null;
    // }

    return new Scaffold(
        key: _scaffoldKey,
        appBar: AppBar(
          // leading: IconButton(
          //   icon: Icon(_backIcon()),
          //   alignment: Alignment.centerLeft,
          //   tooltip: 'Back',
          //   onPressed: () {
          //     Navigator.pop(context);
          //   },
          // ),
          title: Text(toolbarname),
          backgroundColor: Colors.white,
          actions: <Widget>[
            new Padding(
              padding: const EdgeInsets.all(10.0),
              child: new Container(
                height: 150.0,
                width: 30.0,
                child: new GestureDetector(
                  onTap: () {
                    /*Navigator.of(context).push(
                  new MaterialPageRoute(
                      builder:(BuildContext context) =>
                      new CartItemsScreen()
                  )
              );*/
                  },
                  child: Stack(
                    children: <Widget>[
                      new IconButton(
                          icon: new Icon(
                            Icons.shopping_cart,
                            color: Colors.black,
                          ),
                          onPressed: () {
                            // Navigator.push(context, MaterialPageRoute(builder: (context) =>Checkout()));
                          }),
                      list.length == 0
                          ? new Container()
                          : new Positioned(
                              child: new Stack(
                              children: <Widget>[
                                new Icon(Icons.brightness_1,
                                    size: 20.0,
                                    color: Theme.of(context).primaryColor),
                                new Positioned(
                                    top: 4.0,
                                    right: 5.5,
                                    child: new Center(
                                      child: new Text(
                                        list.length.toString(),
                                        style: new TextStyle(
                                            color: Colors.white,
                                            fontSize: 11.0,
                                            fontWeight: FontWeight.w500),
                                      ),
                                    )),
                              ],
                            )),
                    ],
                  ),
                ),
              ),
            )
          ],
        ),
        body: Builder(
          builder: (context) {
            return ListView(
              children: <Widget>[
                viewList(),
              ],
            );
          },
        )

        // Container(
        //     padding: const EdgeInsets.all(8.0),
        //     child: SingleChildScrollView(
        //         child: Column(children: <Widget>[
        //       Container(
        //           padding: const EdgeInsets.fromLTRB(16.0, 16.0, 16.0, 0.0),
        //           child: DefaultTextStyle(
        //               style: descriptionStyle,
        //               child: Row(
        //                 mainAxisSize: MainAxisSize.max,
        //                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
        //                 children: <Widget>[
        //                   // three line description
        //                   Padding(
        //                     padding: const EdgeInsets.only(bottom: 8.0),
        //                     child: Text(
        //                       "Total Amount",
        //                       style: descriptionStyle.copyWith(
        //                           fontSize: 20.0,
        //                           fontWeight: FontWeight.bold,
        //                           color: Colors.black87),
        //                     ),
        //                   ),
        //                   Padding(
        //                     padding: const EdgeInsets.only(bottom: 8.0),
        //                     child: Text(
        //                       "Price here",
        //                       style: descriptionStyle.copyWith(
        //                           fontSize: 20.0, color: Colors.black54),
        //                     ),
        //                   ),
        //                 ],
        //               ))),
        //       ListView(children: <Widget>[
        //         ListView.builder(
        //           shrinkWrap: true,
        //           primary: false,
        //           itemBuilder: (context, position) {
        //             return Stack(children: <Widget>[Container()]);
        //             // return CreateListItems(_ordersLine, position,
        //             //     onDelete: () => removeItem(position),
        //             //     onUpdate: (counter) => updateItem(position, counter));
        //           },
        //           itemCount: _ordersLine.length,
        //         )
        //       ]),
        //       Container(
        //           margin: EdgeInsets.all(10.0),
        //           child: Card(
        //               child: Container(
        //                   padding:
        //                       const EdgeInsets.fromLTRB(10.0, 20.0, 10.0, 20.0),
        //                   child: DefaultTextStyle(
        //                       style: descriptionStyle,
        //                       child: Row(
        //                         mainAxisSize: MainAxisSize.max,
        //                         mainAxisAlignment:
        //                             MainAxisAlignment.spaceBetween,
        //                         children: <Widget>[
        //                           // three line description
        //                           Row(
        //                             children: <Widget>[
        //                               Text(
        //                                 item.toString(),
        //                                 style: descriptionStyle.copyWith(
        //                                     fontSize: 20.0,
        //                                     color: Colors.black87),
        //                               ),
        //                             ],
        //                           ),

        //                           Padding(
        //                             padding: const EdgeInsets.only(bottom: 8.0),
        //                             child: Container(
        //                                 alignment: Alignment.center,
        //                                 child: Text('Price here')),
        //                           ),
        //                         ],
        //                       ))))),
        //     ]))
        // )

        );
  }
}
