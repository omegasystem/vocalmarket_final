// import 'dart:async';
// import 'package:flutter/material.dart';
// import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

// const kAndroidUserAgent =
//     'Mozilla/5.0 (Linux; Android 6.0; Nexus 5 Build/MRA58N) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/62.0.3202.94 Mobile Safari/537.36';

// // String postUrl="https://www.sandbox.paypal.com/cgi-bin/webscr";
// String postUrl = "https://www.paypal.com/cgi-bin/webscr";
// String paypalid = "dhorajiya.v@gmail.com";
// final Set<JavascriptChannel> jsChannels = [
//   JavascriptChannel(
//       name: 'Print',
//       onMessageReceived: (JavascriptMessage message) {
//         print(message.message);
//       }),
// ].toSet();

// class WebViewExample extends StatefulWidget {

//   final String partnerId;
//   final double totalAmount;
//   final String toCurrency;

//   WebViewExample(this.partnerId, this.totalAmount, this.toCurrency);

//   @override
//   _WebViewExampleState createState() => _WebViewExampleState();
// }

// class _WebViewExampleState extends State<WebViewExample> {

// // Instance of WebView plugin
//   final flutterWebViewPlugin = FlutterWebviewPlugin();

//   // On destroy stream
//   StreamSubscription _onDestroy;
//   // On urlChanged stream
//   StreamSubscription<String> _onUrlChanged;
//   // On urlChanged stream
//   StreamSubscription<WebViewStateChanged> _onStateChanged;
//   StreamSubscription<WebViewHttpError> _onHttpError;
//   StreamSubscription<double> _onProgressChanged;
//   StreamSubscription<double> _onScrollYChanged;
//   StreamSubscription<double> _onScrollXChanged;

//   final _urlCtrl = TextEditingController(text: postUrl);
//   final _codeCtrl = TextEditingController(text: 'window.navigator.userAgent');
//   final _scaffoldKey = GlobalKey<ScaffoldState>();
//   final _history = [];
//   String postData  = "";

//   @override
//   void initState() {
//     super.initState();
//     setState(() {
//       postData =
//       "$postUrl?business=$paypalid&cmd=_xclick&item_name=xyz&item_number=123&amount=${widget.totalAmount}&currency_code=${widget.toCurrency}&cancel_return=http://escortapp.co.in/payment-cancel&return=http://escortapp.co.in/payment-status";
//     });

//     // Add a listener to on destroy WebView, so you can make came actions.
//     _onDestroy = flutterWebViewPlugin.onDestroy.listen((_) {
//       if (mounted) {
//         // Actions like show a info toast.
//         _scaffoldKey.currentState.showSnackBar(
//             const SnackBar(content: const Text('Webview Destroyed')));
//       }
//     });

//     // Add a listener to on url changed
//     _onUrlChanged = flutterWebViewPlugin.onUrlChanged.listen((String url) {
//       if (mounted) {
//         setState(() {
//           _history.add('onUrlChanged: $url');
//         });
//       }
//     });

//     _onProgressChanged =
//         flutterWebViewPlugin.onProgressChanged.listen((double progress) {
//           if (mounted) {
//             setState(() {
//               _history.add('onProgressChanged: $progress');
//             });
//           }
//         });

//     _onScrollYChanged =
//         flutterWebViewPlugin.onScrollYChanged.listen((double y) {
//           if (mounted) {
//             setState(() {
//               _history.add('Scroll in Y Direction: $y');
//             });
//           }
//         });

//     _onScrollXChanged =
//         flutterWebViewPlugin.onScrollXChanged.listen((double x) {
//           if (mounted) {
//             setState(() {
//               _history.add('Scroll in X Direction: $x');
//             });
//           }
//         });

//     _onStateChanged =
//         flutterWebViewPlugin.onStateChanged.listen((WebViewStateChanged state) {
//           if (mounted) {
//             setState(() {
//               print("url_res  ${state.type} ${state.url}");
//               _history.add('onStateChanged: ${state.type} ${state.url}');
//             });
//           }
//         });

//     _onHttpError =
//         flutterWebViewPlugin.onHttpError.listen((WebViewHttpError error) {
//           if (mounted) {
//             setState(() {
//               _history.add('onHttpError: ${error.code} ${error.url}');
//             });
//           }
//         });
//   }

//   @override
//   void dispose() {
//     // Every listener should be canceled, the same should be done with this stream.
//     _onDestroy.cancel();
//     _onUrlChanged.cancel();
//     _onStateChanged.cancel();
//     _onHttpError.cancel();
//     _onProgressChanged.cancel();
//     _onScrollXChanged.cancel();
//     _onScrollYChanged.cancel();

//     flutterWebViewPlugin.dispose();

//     super.dispose();
//   }

//   Future<bool> _onBackPressed() {
//     return showDialog(
//       context: context,
//       builder: (context) => new AlertDialog(
//         title: new Text('Are you sure?'),
//         content: new Text('Do you want to exit an App'),
//         actions: <Widget>[
//           new GestureDetector(
//             onTap: () => Navigator.of(context).pop(false),
//             child: Text("NO"),
//           ),
//           SizedBox(height: 16),
//           new GestureDetector(
//             onTap: () => Navigator.of(context).pop(true),
//             child: Text("YES"),
//           ),
//         ],
//       ),
//     ) ??
//         false;
//   }

//   @override
//   Widget build(BuildContext context) {
//     return WillPopScope(
//       // onWillPop: () {
//       //   print('Backbutton pressed (device or appbar button), do whatever you want.');
//       //   final future = flutterWebViewPlugin.evalJavascript('alert("Hello World");');
//       //   future.then((String result) {
//       //     setState(() {
//       //       _history.add('eval: $result');
//       //     });
//       //   });
//       //   //trigger leaving and use own data
//       //   Navigator.pop(context, false);

//       //   //we need to return a future
//       //   return Future.value(false);
//       // },
//       onWillPop: _onBackPressed,
//       child: MaterialApp(
//         debugShowCheckedModeBanner: false,
//           home: new WebviewScaffold(
//             url: postData,
//             appBar: new AppBar(
//               title: new Text(
//                 "Payment",
//               ),
//               leading: new IconButton(
//                 icon: new Icon(Icons.arrow_back),
//                 onPressed: () {
//                   Navigator.of(context).pop(true);
//                 },
//               ),
//             ),

//           )),
//     );
//   }
// }
