import 'package:flutter/material.dart';
// import 'package:vocalmarket/app_translations.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/partners.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/screens/createpartneraddress.dart';
// import 'package:vocalmarket/screens/paypal_view.dart';
import 'package:vocalmarket/widget/text/titletext.dart';
import 'package:flutter/services.dart';
import 'package:razorpay_flutter/razorpay_flutter.dart';
import 'package:fluttertoast/fluttertoast.dart';

class PartnerAddress extends StatefulWidget {
  final String partnerId;
  final double totalAmount;
  final String toCurrency;

  PartnerAddress(this.partnerId, this.totalAmount, this.toCurrency);

  @override
  _PartnerAddressState createState() => _PartnerAddressState();
}

class _PartnerAddressState extends Base<PartnerAddress> {
  List<PartnerAddressValues> _partnerAddressValue = [];

  TextEditingController street = new TextEditingController();
  TextEditingController street2 = new TextEditingController();
  TextEditingController city = new TextEditingController();
  TextEditingController state = new TextEditingController();
  TextEditingController pincode = new TextEditingController();
  TextEditingController country = new TextEditingController();

  static const platform = const MethodChannel("razorpay_flutter");
  Razorpay _razorpay;

  Widget _getListItemTile(BuildContext context, int index) {
    return ListTile(
      onTap: () {
        setState(() {
          changeArrayState(index);
        });
      },
      selected: _partnerAddressValue[index].selected,
      leading: GestureDetector(
        behavior: HitTestBehavior.opaque,
        onTap: () {},
        child: Container(
          width: 48,
          height: 48,
          padding: EdgeInsets.symmetric(vertical: 4.0),
          alignment: Alignment.center,
          // child: CircleAvatar(
          //     backgroundColor: _partnerAddressValue[index].colorpicture,
          // ),
        ),
      ),
      // title: Text('ID: ' + _partnerAddressValue[index].id.toString()),
      subtitle: Text(_partnerAddressValue[index].street),
      trailing: (_partnerAddressValue[index].selected)
          ? Icon(Icons.check_box)
          : Icon(Icons.check_box_outline_blank),
    );
  }

  // void updateAddList() {
  //   print("updateAddList-----------------------");
  //   getOdooInstance().then((odoo) {
  //     _getAddress();
  //   });
  //   if (this.mounted) {
  //     setState(() {
  //       _partnerAddressValue = List.from(_partnerAddressValue);
  //     });
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      home: Scaffold(
          appBar: new AppBar(
            backgroundColor: Colors.black,
            title: new TitleText(
              text: "CHECKOUT",
              fontSize: 18.0,
              color: Colors.white,
            ),
            actions: <Widget>[
              new IconButton(
                  icon: const Icon(Icons.add),
                  onPressed: () {
                    Navigator.push(
                        context,
                        MaterialPageRoute(
                            builder: (context) =>
                                CreatePartnerAddress(user.result.partnerId)));
                  })
            ],
          ),
          // body: Center(
          //     child: Row(
          //         mainAxisAlignment: MainAxisAlignment.center,
          //         children: <Widget>[
          //       RaisedButton(onPressed: openCheckout, child: Text('Open'))
          //     ])),

          body: Builder(
            builder: (context) {
              return ListView(
                children: <Widget>[
                  createCartList(),
                  _partnerAddressValue.isNotEmpty
                      ? Container(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.end,
                            children: <Widget>[
                              RaisedButton(
                                onPressed: () {
                                  openCheckout();
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) =>
                                  //             WebViewExample(widget.partnerId, widget.totalAmount, widget.toCurrency)));
                                },
                                color: Colors.black,
                                padding: EdgeInsets.only(
                                    top: 12, left: 60, right: 60, bottom: 12),
                                child: Text(
                                  "PROCESS CHECKOUT",
                                  style: TextStyle(color: Colors.white),
                                ),
                              ),
                            ],
                          ),
                          margin: EdgeInsets.only(top: 16),
                        )
                      : Padding(
                          padding: EdgeInsets.all(10.0),
                          child: new Column(
                            children: <Widget>[
                              new TextField(
                                controller: street,
                                decoration: new InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: "Street",
                                ),
                              ),
                              const Divider(
                                height: 5.0,
                              ),
                              new TextField(
                                controller: street2,
                                decoration: new InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: "Street2",
                                ),
                              ),
                              const Divider(
                                height: 5.0,
                              ),
                              new TextField(
                                controller: city,
                                decoration: new InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: "City",
                                ),
                              ),
                              const Divider(
                                height: 5.0,
                              ),
                              new TextField(
                                controller: state,
                                decoration: new InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: "State",
                                ),
                              ),
                              const Divider(
                                height: 5.0,
                              ),
                              new TextField(
                                controller: pincode,
                                decoration: new InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: "Pincode",
                                ),
                              ),
                              const Divider(
                                height: 5.0,
                              ),
                              new TextField(
                                controller: country,
                                decoration: new InputDecoration(
                                  border: OutlineInputBorder(),
                                  hintText: "Country",
                                ),
                              ),
                              const Divider(
                                height: 5.0,
                              ),
                              new RaisedButton(
                                onPressed: () {
                                  // _createAddress();
                                  // Navigator.push(
                                  //     context,
                                  //     MaterialPageRoute(
                                  //         builder: (context) => WebViewExample(
                                  //             widget.partnerId,
                                  //             widget.totalAmount,
                                  //             widget.toCurrency)));
                                },
                                color: Colors.black,
                                padding: EdgeInsets.only(
                                    top: 12, left: 60, right: 60, bottom: 12),
                                child: Text(
                                  "DELIVER TO THIS ADDRESS",
                                  style: TextStyle(color: Colors.white),
                                ),
                              )
                            ],
                          ))
                ],
              );
            },
          )),
    );
  }

  _getAddress() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("res.partner", [
          ['parent_id', '=', user.result.partnerId],
          ['type', '=', 'delivery']
        ], [
          'id',
          'street'
        ]).then((OdooResponse res) {
          if (!res.hasError()) {
            if (this.mounted) {
              setState(() {
                for (var i in res.getRecords()) {
                  _partnerAddressValue.add(new PartnerAddressValues(
                      id: i["id"].toString(), street: i["street"]));
                }
              });
            }
          }
        });
      }
    });
  }

  void changeArrayState(int index) {
    for (int i = 0; i < _partnerAddressValue.length; i++) {
      _partnerAddressValue[i].selected = false;
    }
    _partnerAddressValue[index].selected = true;
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      _getAddress();
    });
    _razorpay = Razorpay();
    _razorpay.on(Razorpay.EVENT_PAYMENT_SUCCESS, _handlePaymentSuccess);
    _razorpay.on(Razorpay.EVENT_PAYMENT_ERROR, _handlePaymentError);
    _razorpay.on(Razorpay.EVENT_EXTERNAL_WALLET, _handleExternalWallet);
  }

  @override
  void dispose() {
    super.dispose();
    _razorpay.clear();
  }

  void openCheckout() async {
    var options = {
      'key': 'rzp_test_dXFv4RoB1PQoEG',
      'amount': 2000,
      'name': 'Acme Corp.',
      'description': 'Fine T-Shirt',
      'prefill': {'contact': '8888888888', 'email': 'test@razorpay.com'},
      'external': {
        'wallets': ['paytm']
      }
    };

    try {
      _razorpay.open(options);
    } catch (e) {
      debugPrint(e);
    }
  }

  createCartList() {
    return ListView.builder(
      shrinkWrap: true,
      primary: false,
      itemBuilder: (context, position) {
        return _getListItemTile(context, position);
      },
      itemCount: _partnerAddressValue.length,
    );
  }

  void _handlePaymentSuccess(PaymentSuccessResponse response) {
    Fluttertoast.showToast(
        msg: "SUCCESS: " + response.paymentId, timeInSecForIos: 4);
  }

  void _handlePaymentError(PaymentFailureResponse response) {
    Fluttertoast.showToast(
        msg: "ERROR: " + response.code.toString() + " - " + response.message,
        timeInSecForIos: 4);
  }

  void _handleExternalWallet(ExternalWalletResponse response) {
    Fluttertoast.showToast(
        msg: "EXTERNAL_WALLET: " + response.walletName, timeInSecForIos: 4);
  }
}
