import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';
import 'package:vocalmarket/screens/login.dart';
import 'package:vocalmarket/widget/text/newEmail.dart';
import 'package:vocalmarket/widget/text/newName.dart';
import 'package:vocalmarket/widget/text/password.dart';
import 'package:vocalmarket/widget/text/repassword.dart';
import 'package:vocalmarket/widget/text/textLogin.dart';

class NewUser extends StatefulWidget {
  @override
  _NewUserState createState() => _NewUserState();
}

class _NewUserState extends Base<NewUser> {
  TextEditingController _nameCtrler = new TextEditingController();
  TextEditingController _emailCtrler = new TextEditingController();
  TextEditingController _passCtrler = new TextEditingController();
  TextEditingController _repassCtrler = new TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  bool check1;
  bool check2;
  String passwordtype = "";
  double passwordpercent = 0.0;

  _signup(String name, String email, String pass, String repass) {
    getOdooInstance().then((odoo) {
      if (isValid(email, pass, repass)) {
        isConnected().then((isInternet) {
          if (isInternet) {
            odoo
                .authenticate(
                    Strings.admin_email, Strings.admin_pass, Strings.odoo_db)
                .then(
              (http.Response auth) {
                if (auth.body != null) {
                  _scaffoldKey.currentState.showSnackBar(new SnackBar(
                      content: Text(
                          "Please Wait! Check your email for varification!")));
                  odoo.searchRead('res.users', [
                    ['login', '=', 'admin1']
                  ], [
                    'id'
                  ]).then((OdooResponse res) {
                    print(res.getRecords());
                    // if (res.getRecords() is ) {
                    //   print("Cal----------------");
                    // } else {
                    //   print("no---------------------");
                    // }
                    // if (res.getStatusCode() == 200) {
                    //   _scaffoldKey.currentState.showSnackBar(new SnackBar(
                    //       content: Text("Signup in successfully!")));
                    //   // pushReplacement(Login());
                    // }
                  });

                  // odoo.create('res.users', {
                  //   'name': name,
                  //   'login': email,
                  //   'password': pass,
                  //   'sel_groups_1_9_10': 9,
                  // }).then((OdooResponse res) {
                  //   if (res.getStatusCode() == 200) {
                  //     _scaffoldKey.currentState.showSnackBar(new SnackBar(
                  //         content: Text("Signup in successfully!")));
                  // pushReplacement(Login());
                  //   }
                  // });
                  // saveUser(json.encode(user));
                  // saveOdooUrl("http://10.0.2.2:8069");
                  // pushReplacement(Home());
                } else {
                  _scaffoldKey.currentState.showSnackBar(new SnackBar(
                      content: Text("Please Enter Valid Email or Password!")));
                }
              },
            );
          }
        });
      }
    });
  }

  bool isValid(String email, String pass, String repass) {
    Pattern pattern =
        r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
    RegExp regex = new RegExp(pattern);
    if (pass != repass) {
      print("if-----------------");
      // hideLoadingError("Password is not Matched!");
    }
    // else if (!regex.hasMatch(email)){
    //   print("else--id---------------");
    // hideLoadingError("Please Enter Valid Email!");
    // }
    else {
      print("else-----------------");
      return true;
    }
    return false;
  }

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    _nameCtrler.dispose();
    _emailCtrler.dispose();
    _passCtrler.dispose();
    _repassCtrler.dispose();
    super.dispose();
  }

  void checkPassword(text) {
    String pattern1 =
        r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
    String pattern2 = r'^(?=.*?[A-Z])(?=.*?[!@#\$&*~]).{8,}$';
    RegExp regExp = new RegExp(pattern1);
    RegExp regExp2 = new RegExp(pattern2);
    check1 = regExp.hasMatch(text);
    check2 = regExp2.hasMatch(text);
    setState(() {
      if (text == "") {
        passwordtype = "";
        passwordpercent = 0.0;
      } else {
        if (check1 == false && check2 == false) {
          passwordtype = "weak";
          passwordpercent = 0.2;
        } else if (check1 == true && check2 == false) {
          passwordtype = "strong";
          passwordpercent = 1.0;
        } else if (check1 == false && check2 == true) {
          passwordtype = "medium";
          passwordpercent = 0.5;
        } else if (check1 == true && check2 == true) {
          passwordtype = "strong";
          passwordpercent = 1.0;
        }
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.white, Colors.white]),
        ),
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                // Row(
                //   children: <Widget>[
                //     // SingUp(),
                //     TextLogin(),
                //   ],
                // ),
                Padding(
                  padding: const EdgeInsets.only(top: 40, right: 50, left: 50),
                  child: Container(
                      height: 100,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: 100.0,
                              child: Image.asset(
                                "assets/logo.png",
                                fit: BoxFit.contain,
                              ),
                            ),
                          ])),
                ),
                NewName(
                  controller: _nameCtrler,
                ),
                NewEmail(
                  controller: _emailCtrler,
                ),
                // PasswordInput(
                //   controller: _passCtrler,
                // ),

                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 50, right: 50),
                  child: Container(
                    height: 60,
                    width: MediaQuery.of(context).size.width,
                    child: new TextField(
                      onChanged: (text) {
                        checkPassword(text);
                      },
                      obscureText: true,
                      controller: _passCtrler,
                      decoration: new InputDecoration(
                        labelText: "Enter Password",
                        fillColor: Colors.black,
                      ),
                      keyboardType: TextInputType.emailAddress,
                      style: new TextStyle(
                          fontFamily: "Quicksand", color: Colors.black),
                    ),
                  ),
                ),

                RePasswordInput(
                  controller: _repassCtrler,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 40, right: 50, left: 50),
                  child: Container(
                    alignment: Alignment.bottomRight,
                    height: 50,
                    width: MediaQuery.of(context).size.width,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(0),
                    ),
                    child: FlatButton(
                      onPressed: () {
                        _signup(_nameCtrler.text, _emailCtrler.text,
                            _passCtrler.text, _repassCtrler.text);
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'SIGNUP',
                            style: TextStyle(
                                color: Colors.white, fontFamily: "Quicksand"),
                          ),
                          Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                Container(
                  padding: EdgeInsets.only(left: 100.0, top: 10.0),
                  alignment: Alignment.center,
                  child: new LinearPercentIndicator(
                    width: 200.0,
                    lineHeight: 14.0,
                    percent: passwordpercent,
                    backgroundColor: Colors.grey,
                    progressColor: Colors.blue,
                  ),
                ),
                Container(
                  child: Center(
                    child: Text(passwordtype),
                  ),
                ),
                FlatButton(
                  padding: EdgeInsets.all(0),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Login()));
                  },
                  child: Text(
                    'back to login',
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.right,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
