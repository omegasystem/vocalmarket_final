import 'package:flutter/material.dart';
// import 'package:settings_ui/settings_ui.dart';
import 'package:vocalmarket/application.dart';
import 'package:vocalmarket/base.dart';
// import 'package:vocalmarket/odoo/data/pojo/language.dart';
// import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
// import 'package:vocalmarket/src/theme/lightcolor.dart';

class LanguageList extends StatefulWidget {
  final void Function(String) onChangeLang;
  LanguageList({this.onChangeLang});

  @override
  _LanguageListState createState() => _LanguageListState();
}

class _LanguageListState extends Base<LanguageList> {
  static final List<String> languagesList = application.supportedLanguages;
  // String firstLang = languagesList[0];
  static final List<String> languageCodesList =
      application.supportedLanguagesCodes;

  final Map<dynamic, dynamic> languagesMap = {
    languagesList[0]: languageCodesList[0],
    languagesList[1]: languageCodesList[1],
  };

  // _getLanguageData() async {
  //   isConnected().then((isInternet) {
  //     if (isInternet) {
  //       odoo.searchRead("res.lang", [
  //         ["active", "=", true],
  //       ], [
  //         'id',
  //         'name',
  //       ]).then(
  //         (OdooResponse res) {
  //           if (!res.hasError()) {
  //             setState(() {
  //               for (var i in res.getRecords()) {
  //                 _language.add(
  //                   new OdooLanguage(
  //                     id: i["id"].toString(),
  //                     name: i["name"],
  //                   ),
  //                 );
  //               }
  //             });
  //           }
  //         },
  //       );
  //     }
  //   });
  // }

  @override
  void initState() {
    super.initState();

    // getOdooInstance().then((odoo) {
    //   _getLanguageData();
    // });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          iconTheme: IconThemeData(
            color: Colors.black, //change your color here
          ),
          title: Text(
            "Language",
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.white,
        ),
        body: ListView.separated(
          itemCount: languagesList.length,
          itemBuilder: (context, index) {
            return ListTile(
              title: Text(languagesList[index]),
              onTap: () {
                widget.onChangeLang(languagesList[index]);
                Navigator.pop(context);
              },
            );
          },
          separatorBuilder: (context, index) {
            return Divider();
          },
        ));
  }
}
