import 'package:flutter/material.dart';
import 'dart:convert';
import 'package:settings_ui/settings_ui.dart';
import 'package:vocalmarket/app_translations.dart';
import 'package:vocalmarket/application.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/pojo/partners.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/utility/constant.dart';
import 'package:vocalmarket/screens/login.dart';
import 'package:vocalmarket/screens/myaddress.dart';
import 'package:vocalmarket/screens/profile.dart';
import 'package:vocalmarket/screens/settings/languagelist.dart';
import 'package:vocalmarket/widget/product/currency.dart';

class SettingsScreen extends StatefulWidget {
  @override
  _SettingsScreenState createState() => _SettingsScreenState();
}

class _SettingsScreenState extends Base<SettingsScreen> {
  bool lockInBackground = true;

  String language = "";
  String currency = "";
  final Map<String, dynamic> someMap = {};

  static final List<String> languagesList = application.supportedLanguages;
  String firstLang = languagesList[0];
  static final List<String> languageCodesList =
      application.supportedLanguagesCodes;

  final Map<dynamic, dynamic> languagesMap = {
    languagesList[0]: languageCodesList[0],
    languagesList[1]: languageCodesList[1],
  };

  getCurrency() async {
    isConnected().then((isInternet) {
      if (isInternet) {
        odoo.searchRead("res.partner", [
          ["id", "=", user.result.partnerId]
        ], [
          'id',
          'app_currency_id'
        ]).then(
          (OdooResponse res) {
            if (!res.hasError()) {
              if (this.mounted) {
                final result = res.getResult()['records'][0];
                someMap['result'] = res.getResult()['records'][0];
                Partner partner = Partner.fromJson(someMap);
                setState(() {
                  currency = result['app_currency_id'] is! bool
                      ? result['app_currency_id']
                      : "INR";
                });
                savePartner(json.encode(partner));
              }
            }
          },
        );
      }
    });
  }

  @override
  void initState() {
    super.initState();
    getOdooInstance().then((odoo) {
      getCurrency();
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  _clearPrefs() async {
    odoo.destroy();
    preferences.remove(Constants.USER_PREF);
    preferences.remove(Constants.PARTNER_PREF);
    preferences.remove(Constants.SESSION);
    pushAndRemoveUntil(Login());
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      // appBar: AppBar(title: Text('Settings UI')),
      body: SettingsList(
        sections: [
          // SettingsSection(
          //   title: AppTranslations.of(context).text("Common"),
          //   tiles: [
          //     SettingsTile(
          //       title: AppTranslations.of(context).text("Language"),
          //       subtitle: firstLang,
          //       leading:
          //           IconButton(icon: Icon(Icons.language), onPressed: () {}),
          //       onTap: () {
          //         Navigator.of(context).push(MaterialPageRoute(
          //             builder: (BuildContext context) =>
          //                 LanguageList(onChangeLang: (String val) {
          //                   setState(() {
          //                     firstLang = val;
          //                   });
          //                   application
          //                       .onLocaleChanged(Locale(languagesMap[val]));
          //                 })));
          //       },
          //     ),
          //     SettingsTile(
          //       title: AppTranslations.of(context).text("Currency"),
          //       subtitle: currency,
          //       leading: IconButton(
          //           icon: Icon(Icons.monetization_on), onPressed: () {}),
          //       onTap: () {
          //         Navigator.of(context).push(MaterialPageRoute(
          //             builder: (BuildContext context) =>
          //                 ProductCurrency(onChangeCurrency: (String val) {
          //                   print("vals--------------$val");
          //                   setState(() {
          //                     currency = val;
          //                   });
          //                   // application
          //                   //     .onLocaleChanged(Locale(languagesMap[val]));
          //                 })));
          //       },
          //     ),
          //   ],
          // ),
          SettingsSection(
            title: AppTranslations.of(context).text("Account"),
            tiles: [
              SettingsTile(
                title: AppTranslations.of(context).text("Profile"),
                leading: IconButton(
                    icon: Icon(Icons.people),
                    onPressed: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) => ProfilePage()));
                    }),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => ProfilePage()));
                },
              ),
              SettingsTile(
                title: "Address",
                leading: IconButton(
                    icon: Icon(Icons.local_post_office),
                    onPressed: () {
                      Navigator.push(context,
                          MaterialPageRoute(builder: (context) => MyAddress()));
                    }),
                onTap: () {
                  Navigator.push(context,
                      MaterialPageRoute(builder: (context) => MyAddress()));
                },
              ),
              // SettingsTile(
              //     title: 'Email',
              //     leading:
              //         IconButton(icon: Icon(Icons.email), onPressed: () {})),
              SettingsTile(
                  onTap: () {
                    _clearPrefs();
                  },
                  title: AppTranslations.of(context).text("Sign_out"),
                  leading: IconButton(
                      icon: Icon(Icons.exit_to_app), onPressed: () {})),
            ],
          ),
          // SettingsSection(
          //   title: 'Secutiry',
          //   tiles: [
          //     SettingsTile.switchTile(
          //       title: 'Lock app in background',
          //       leading: Icon(Icons.phonelink_lock),
          //       switchValue: lockInBackground,
          //       onToggle: (bool value) {
          //         setState(() {
          //           lockInBackground = value;
          //         });
          //       },
          //     ),
          //     SettingsTile.switchTile(
          //         title: 'Use fingerprint',
          //         leading: Icon(Icons.fingerprint),
          //         onToggle: (bool value) {},
          //         switchValue: false),
          //     SettingsTile.switchTile(
          //       title: 'Change password',
          //       leading: Icon(Icons.lock),
          //       switchValue: true,
          //       onToggle: (bool value) {},
          //     ),
          //   ],
          // ),
          SettingsSection(
            title: AppTranslations.of(context).text("Misc"),
            tiles: [
              SettingsTile(
                  title: AppTranslations.of(context).text("Terms_of_Service"),
                  leading: IconButton(
                      icon: Icon(Icons.description), onPressed: () {})),
              SettingsTile(
                  title:
                      AppTranslations.of(context).text("Open_source_licenses"),
                  leading: IconButton(
                      icon: Icon(Icons.collections_bookmark),
                      onPressed: () {})),
            ],
          )
        ],
      ),
    );
  }
}
