import 'dart:async';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'package:flutter/material.dart';
import 'package:vocalmarket/base.dart';
import 'package:vocalmarket/odoo/data/services/odoo_response.dart';
import 'package:vocalmarket/odoo/utility/strings.dart';
import 'package:vocalmarket/screens/login.dart';
import 'package:vocalmarket/widget/text/newEmail.dart';
import 'package:vocalmarket/widget/text/password.dart';
import 'package:vocalmarket/widget/text/repassword.dart';
import 'package:vocalmarket/widget/text/textLogin.dart';
import 'package:mailer/mailer.dart';
import 'package:mailer/smtp_server.dart';
import 'package:shared_preferences/shared_preferences.dart';

class ResetPass extends StatefulWidget {
  @override
  _ResetPassState createState() => _ResetPassState();
}

class _ResetPassState extends Base<ResetPass> {
  TextEditingController _emailCtrler = new TextEditingController();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<void> sendemail() async {
    final prefs = await SharedPreferences.getInstance();
    final smtpServer = gmail("omega.corpo@gmail.com", "9979175490*Oc");
    var rnd = new Random();
    var next = rnd.nextDouble() * 1000000;
    while (next < 100000) {
      next *= 10;
    }

    prefs.setInt("code", next.toInt());
    final message = Message()
      ..from = Address("omega.corpo@gmail.com")
      ..recipients.add(_emailCtrler.text)
      ..subject = 'Password Reset Code!'
      ..text = 'Wearme Code: ${next.toInt()}';
    try {
      _scaffoldKey.currentState
          .showSnackBar(new SnackBar(content: Text("Code Sending!")));
      final sendReport = await send(message, smtpServer);
      prefs.setInt("code", next.toInt());
      print('Message sent: ' + sendReport.toString());
      Navigator.push(
          context, (MaterialPageRoute(builder: (context) => ResetPassPage())));
    } on MailerException catch (e) {
      print('Message not sent. \n' + e.toString());
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.white, Colors.white]),
        ),
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 40, right: 50, left: 50),
                  child: Container(
                      height: 100,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: 100.0,
                              child: Image.asset(
                                "assets/logo.png",
                                fit: BoxFit.contain,
                              ),
                            ),
                          ])),
                ),
                // NewNome(),
                NewEmail(
                  controller: _emailCtrler,
                ),
                // PasswordInput(),
                Padding(
                  padding: const EdgeInsets.only(top: 30, left: 50, right: 50),
                  child: Container(
                    alignment: Alignment.topRight,
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(0),
                    ),
                    child: FlatButton(
                      onPressed: () {
                        sendemail();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Forgot Password',
                            style: TextStyle(
                                color: Colors.white, fontFamily: "Quicksand"),
                          ),
                          Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                FlatButton(
                  padding: EdgeInsets.all(0),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Login()));
                  },
                  child: Text(
                    'back to login',
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.right,
                  ),
                ),
                // UserOld(),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class ResetPassPage extends StatefulWidget {
  @override
  _ResetPassPageState createState() => _ResetPassPageState();
}

class _ResetPassPageState extends Base<ResetPassPage> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  TextEditingController _codeCtrler = new TextEditingController();
  TextEditingController _emailCtrler = new TextEditingController();
  TextEditingController _passCtrler = new TextEditingController();
  TextEditingController _repassCtrler = new TextEditingController();
  bool code = false;

  _resetpass() async {
    final prefs = await SharedPreferences.getInstance();
    if (prefs.getInt("code") == int.parse(_codeCtrler.text)) {
      getOdooInstance().then((odoo) {
        odoo
            .authenticate(
                Strings.admin_email, Strings.admin_pass, Strings.odoo_db)
            .then((http.Response auth) {
          if (auth.body != null) {
            odoo.searchRead(Strings.res_users, [
              ['login', '=', _emailCtrler.text]
            ], [
              'id',
              'login'
            ]).then((OdooResponse res) {
              if (!res.hasError()) {
                final result = res.getResult()['records'][0];
                odoo.write(Strings.res_users, [
                  result['id']
                ], {
                  'password': _passCtrler.text
                }).then((OdooResponse writeres) async {
                  if (!writeres.hasError()) {
                    if (writeres.getStatusCode() == 200) {
                      _scaffoldKey.currentState.showSnackBar(new SnackBar(
                          content: Text("Password Changed successfully!")));
                      await Future.delayed(const Duration(seconds: 2), () {});
                      pushReplacement(Login());
                    }
                  }
                });
              }
            });
          }
        });
      });
    } else {
      _scaffoldKey.currentState
          .showSnackBar(new SnackBar(content: Text("Wrong Code!")));
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        decoration: BoxDecoration(
          gradient: LinearGradient(
              begin: Alignment.topRight,
              end: Alignment.bottomLeft,
              colors: [Colors.white, Colors.white]),
        ),
        child: ListView(
          children: <Widget>[
            Column(
              children: <Widget>[
                Padding(
                  padding: const EdgeInsets.only(top: 40, right: 50, left: 50),
                  child: Container(
                      height: 100,
                      width: MediaQuery.of(context).size.width,
                      child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            SizedBox(
                              height: 100.0,
                              child: Image.asset(
                                "assets/logo.png",
                                fit: BoxFit.contain,
                              ),
                            ),
                          ])),
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 20, left: 50, right: 50),
                  child: Container(
                    height: 60,
                    width: MediaQuery.of(context).size.width,
                    child: new TextField(
                      controller: _codeCtrler,
                      decoration: new InputDecoration(
                        labelText: "Enter Code",
                        fillColor: Colors.black,
                      ),
                      keyboardType: TextInputType.emailAddress,
                      style: new TextStyle(
                          fontFamily: "Quicksand", color: Colors.black),
                    ),
                  ),
                ),
                NewEmail(
                  controller: _emailCtrler,
                ),
                PasswordInput(
                  controller: _passCtrler,
                ),
                RePasswordInput(
                  controller: _repassCtrler,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 30, left: 50, right: 50),
                  child: Container(
                    alignment: Alignment.topRight,
                    height: 50,
                    decoration: BoxDecoration(
                      color: Colors.black,
                      borderRadius: BorderRadius.circular(0),
                    ),
                    child: FlatButton(
                      onPressed: () {
                        _resetpass();
                      },
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Text(
                            'Reset Password',
                            style: TextStyle(
                                color: Colors.white, fontFamily: "Quicksand"),
                          ),
                          Icon(
                            Icons.arrow_forward,
                            color: Colors.white,
                          ),
                        ],
                      ),
                    ),
                  ),
                ),
                FlatButton(
                  padding: EdgeInsets.all(0),
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) => Login()));
                  },
                  child: Text(
                    'back to login',
                    style: TextStyle(
                      fontSize: 12,
                      color: Colors.black,
                    ),
                    textAlign: TextAlign.right,
                  ),
                ),
                // UserOld(),
                // codenew Center(child: new CircularProgressIndicator())
              ],
            ),
          ],
        ),
      ),
    );
  }
}
